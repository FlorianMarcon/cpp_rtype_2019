/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Client.cpp
*/

#include "Client.hpp"
#include "../Scenes/MainMenu.hpp"

rtype::Client *rtype::Client::_instance = nullptr;


rtype::Client::Client(std::string address, unsigned int port, int _case)
{
	//Create TCPClient
	try {
		this->_serv = new rtype::TCPClient(address, port);
	}

	catch(std::exception &e) {
		std::cout << "Impossible de se connecter au serveur" << std::endl;
		exit(1);
	}

	//Create Window
	Vector2<int>size(800, 600);
	this->_displayManager = new rtype::DisplayManager();
	this->_displayManager->setWindow(&this->_window);
	this->_displayManager->createWindow(size, _case);
	this->_displayManager->setFrameLimit();

	//Music
	this->_soundVolume = 50;
	this->musicMenu = new rtype::Client::Music();
	this->musicMenu->setSoundBuffer("../client/src/Font/menu.ogg");
	this->musicMenu->getSound()->setLoop(true);
	this->musicMenu->getSound()->play();
	this->_address = address;
	this->_port = port;

	this->_clock = new sf::Clock;


	setPos_par((sf::Vector2i){0,0});
	setPos_parr((sf::Vector2i){-400,0});
	setPos_lune((sf::Vector2i){-900,350});
	setPos_pp((sf::Vector2i){-200, 450});
	setPos_red((sf::Vector2i){-900, 800});
	setPos_earth((sf::Vector2i){-200,450});


	rtype::Client::_instance = this;
}

rtype::Client::~Client()
{
}

void rtype::Client::setOldScene()
{
    this->_oldScene = rtype::scene::Scene::getActiveScene();
}

rtype::scene::Scene *rtype::Client::getOldScene()
{
    return this->_oldScene;
}

rtype::DisplayManager *rtype::Client::getDisplayManager()
{
	return(this->_displayManager);
}

rtype::Client *rtype::Client::getInstance()
{
	return _instance;
}

float rtype::Client::getSoundVolume()
{
    return this->_soundVolume;
}

void rtype::Client::setSoundVolume(float soundVolume)
{
    this->_soundVolume = soundVolume;
}

rtype::Client::Music::Music()
{
}

void rtype::Client::Music::setSoundBuffer(std::string filepath)
{
    _sound = new sf::Sound;
    if (!_soundBuffer.loadFromFile(filepath))
        return;
    _sound->setBuffer(_soundBuffer);
}

sf::Sound *rtype::Client::Music::getSound()
{
    return this->_sound;
}

rtype::Client::Music *rtype::Client::getMusicMenu()
{
    return this->musicMenu;
}

void rtype::Client::loop()
{
	rtype::scene::Scene::setActiveScene<rtype::scene::MainMenu>();
	while (this->getDisplayManager()->isWindowOpen()) {
		this->musicMenu->getSound()->setVolume(this->getSoundVolume());
		while (this->_window.pollEvent(this->_event))
		{
			if (this->_event.type == sf::Event::Closed)
				this->getDisplayManager()->closeWindow();
			else if (this->_event.type == sf::Event::Resized)
                glViewport(0, 0, this->_event.size.width, this->_event.size.height);
			rtype::scene::Scene::onEvent();
		}
		rtype::scene::Scene::drawActiveScene();
	}
}

void rtype::Client::setEvent(sf::Event event)
{
	this->_event = event;
}

sf::Event rtype::Client::getEvent(void)
{
	return this->_event;
}

rtype::TCPClient *rtype::Client::getServer(void)
{
	return this->_serv;
}

rtype::TCPClient::ReceiveData rtype::Client::getReceiveData(void)
{
	return this->_rcvData;
}

sf::Clock *rtype::Client::getClock(void)
{
	return this->_clock;
}

sf::Vector2i rtype::Client::getPos_par(void)
{
	return this->_Pos_par;
}

sf::Vector2i rtype::Client::getPos_parr(void)
{
	return this->_Pos_parr;
}

sf::Vector2i rtype::Client::getPos_earth(void)
{
	return this->_Pos_earth;
}

sf::Vector2i rtype::Client::getPos_lune(void)
{
	return this->_Pos_lune;
}

sf::Vector2i rtype::Client::getPos_red(void)
{
	return this->_Pos_red;
}

sf::Vector2i rtype::Client::getPos_pp(void)
{
	return this->_Pos_pp;
}







void rtype::Client::setPos_par(sf::Vector2i pos)
{
	this->_Pos_par = pos;
}

void rtype::Client::setPos_parr(sf::Vector2i pos)
{
	this->_Pos_parr = pos;
}

void rtype::Client::setPos_earth(sf::Vector2i pos)
{
	this->_Pos_earth = pos;
}

void rtype::Client::setPos_lune(sf::Vector2i pos)
{
	this->_Pos_lune = pos;
}

void rtype::Client::setPos_red(sf::Vector2i pos)
{
	this->_Pos_red = pos;
}

void rtype::Client::setPos_pp(sf::Vector2i pos)
{
	this->_Pos_pp = pos;
}

