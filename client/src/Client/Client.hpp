/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Client.hpp
*/

#ifndef client
#define client

#include "../DisplayManager/DisplayManager.hpp"
#include <SFML/Audio.hpp>
#include "Network/TCPClient.hpp"


namespace rtype
{
	namespace scene {
		class Scene;
	}
	class Client {
		class Music;
		public:
			Client(std::string address = "127.0.0.1", unsigned int port = 8080, int _case = 1);
			~Client();

			/**
			 * @brief Get display manager
			 * 
			 * @return DisplayManager
			 */
			DisplayManager *getDisplayManager();

			/**
			 * @brief Get instance
			 * 
			 * @return Client
			 */
			static Client *getInstance();

			/**
			 * @brief Get sound volume
			 * 
			 * @return float
			 */
			float getSoundVolume();

			/**
			 * @brief Set soud volume
			 * 
			 * @return void
			 */
			void setSoundVolume(float SoundVolume);

			/**
			 * @brief Set old scene
			 * 
			 * @return void
			 */
			void setOldScene();

			/**
			 * @brief Get old scene
			 * 
			 * @return rtype;;scene::Scene
			 */
			rtype::scene::Scene *getOldScene();

			/**
			 * @brief Get music menu
			 * 
			 * @return Music
			 */
			Music *getMusicMenu();

			/**
			 * @brief Loop
			 * 
			 * @return void
			 */
			void loop();

			/**
			 * @brief Set event
			 * 
			 * @return void
			 */
			void setEvent(sf::Event event);

			/**
			 * @brief Get event
			 * 
			 * @return sf::Event
			 */
			sf::Event getEvent(void);

			/**
			 * @brief Get server
			 *
			 * @return rtype::TCPClient
			 */
			rtype::TCPClient *getServer(void);

			/**
			 * @brief Get received data
			 * 
			 * @return rtype::TCPClient::ReceivedData
			 */
			rtype::TCPClient::ReceiveData getReceiveData(void);

			/**
			 * @brief Get clock
			 * 
			 * @return sf::Clock
			 */
			sf::Clock *getClock();
			sf::Vector2i getPos_par(void);
			sf::Vector2i getPos_parr(void);
			sf::Vector2i getPos_earth(void);
			sf::Vector2i getPos_lune(void);
			sf::Vector2i getPos_red(void);
			sf::Vector2i getPos_pp(void);

			void setPos_par(sf::Vector2i pos);
			void setPos_parr(sf::Vector2i pos);
			void setPos_earth(sf::Vector2i pos);
			void setPos_lune(sf::Vector2i pos);
			void setPos_red(sf::Vector2i pos);
			void setPos_pp(sf::Vector2i pos);

		protected:
		private:
			DisplayManager *_displayManager;
			static Client *_instance;
			rtype::scene::Scene *_oldScene;
			float _soundVolume;
			sf::RenderWindow _window;
			sf::Event _event;
			int _port;
			std::string _address;
			rtype::TCPClient *_serv;
			rtype::TCPClient::ReceiveData _rcvData;
			class Music
			{
			public:
				explicit Music();
				void setSoundBuffer(std::string filepath);
				sf::Sound *getSound();

			private:
				sf::Sound *_sound;
				sf::SoundBuffer _soundBuffer;
			};
			Music *musicMenu;
			sf::Clock *_clock;
			sf::Vector2i _Pos_par;
			sf::Vector2i _Pos_parr;
			sf::Vector2i _Pos_earth;
			sf::Vector2i _Pos_lune;
			sf::Vector2i _Pos_red;
			sf::Vector2i _Pos_pp;
	};
}


#endif /* !client */