/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** thread.hpp
*/

#ifndef thread
#define thread

#include <SFML/System.hpp>
#include "Client.hpp"

class LoadingThread : public sf::Thread
{
   rtype::Client * m_cli;  // Un pointeur pour savoir où mettre ce qui va être chargé
   sf::String m_info;                 // Une chaine qui va servir à donner des informations sur la progression du chargement
   int  m_status;                     // Le pourcentage accompli
   bool m_running;                    // true si le chargement est en cours
   bool m_stop;                       // pour interrompre le chargement


   /**
    * @brief Do the job
    * 
    * @return void
    */
   static void doTheJob(void * data) // <- Tres important le static, sans lui ça ne marchera pas
   {
        LoadingThread * me(reinterpret_cast<LoadingThread*>(data));
      // les opérations de chargement viennent ici

      // Pendant ces opérations tu testes périodiquement si m_stop == true;
      // ce booléen va te permettre par exemple de gérer un bouton d'annulation du chargement

      // Tu remets aussi periodiquement à jour m_info et m_status qui vont servir à mettre la
      // barre de progression à jour (m_status) et éventuellement à indiquer de manière plus explicite
      // ou tu en es (m_info) par exemple "Chargement des textures"

     // juste avant de sortir de la fonction
    // while ( me->m_status != 1000) {
    //     me->m_status += 5;
	    me->m_cli->getInstance()->getDisplayManager()->drawText(Vector2<int>(100, 100), "RTYPE", Color(255, 255, 255, 255), 50);
        // std::cout << me->m_status << std::endl;
    // }
        me->m_running = false;
   }

public:

   LoadingThread(rtype::Client * env):sf::Thread(doTheJob,this),m_cli(env),m_status(0),m_running(false),m_stop(false){}
   virtual ~LoadingThread(){} // Pas de destruction de m_whereDoIPutTheResult, cette destruction ne relève de la responsabilité de la classe.

   /**
    * @brief Stop
    * 
    * @return void
    */
   void stop(){m_stop = true; wait();}


   /**
    * @brief Active
    * 
    * @return bool
    */
   bool active() const {return m_running;}


   /**
    * @brief Start
    * 
    * @return void
    */
   void start(){m_stop = false; m_status = 0; m_info = "Debut du chargement"; m_running = true; launch();}


   /**
    * @brief Get info
    * 
    * @return std::string
    */
   std::string getInfo() const {return m_info;}

   /**
    * @brief Get status
    * 
    * @return int
    */
   int getStatus() const {return m_status;}

};

#endif /* !thread */
