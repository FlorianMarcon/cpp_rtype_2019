/*
** EPITECH PROJECT, 2018
** OOP_indie_studio_2018
** File description:
** MainMenu.cpp
*/

#include <iostream>
#include "MainMenu.hpp"
#include "GameSelection.hpp"
#include "../Client/Client.hpp"

rtype::scene::MainMenu::MainMenu() : Scene(
	"Main Menu")
{
	this->_player = new Player();
	this->_player->setName("_");
	sf::IntRect rect = this->getDisplayManager().setRectangle(0, 0, _width, _height);
	this->_name = new Button("../client/src/Font/name2.png", (sf::Vector2i){_width/2,_posD + 150}, rect, (sf::Vector2f){1000, 506 * 0.4}, Color(255, 255, 255, 255));
	this->_name->setScale((sf::Vector2f){1, 0.4});
	this->_to_write = 0;
	_posD = 0;
	_endScene = false;
    _intensity = 255;
}

void rtype::scene::MainMenu::toDo()
{
	if (this->_name->isHover(sf::Mouse::getPosition()) && _to_write != 1) {
		_hover = true;
	} else
		_hover = false;
	if (this->_player->getName().length() == 0 && this->_to_write == 0)
		this->_player->setSizeName(0);
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && this->_name->isHover(sf::Mouse::getPosition())) {
		this->_to_write = 1;
		this->_player->setSizeName(1);
		_hover = false;
	}
	else if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !this->_name->isHover(sf::Mouse::getPosition()))
		this->_to_write = 0;
	if (rtype::Client::getInstance()->getEvent().type == sf::Event::TextEntered && this->_to_write == 1)
	{
		this->_player->setSizeName(1);
		if (rtype::Client::getInstance()->getEvent().text.unicode == '\b' && this->_player->getName().length() > 1) {
			this->_player->setName(this->_player->getName().erase(this->_player->getName().size() - 1, 1));
			this->_player->setName(this->_player->getName().erase(this->_player->getName().size() - 1, 1));
			this->_player->setName(this->_player->getName() += "_");
		}
		else if (this->_player->getName().length() <= 9 && rtype::Client::getInstance()->getEvent().text.unicode < 128) {
			this->_player->setName(this->_player->getName().erase(this->_player->getName().size() - 1, 1));
			this->_player->setName(this->_player->getName() += static_cast<char>(rtype::Client::getInstance()->getEvent().text.unicode));
			this->_player->setName(this->_player->getName() += "_");
		}
	}
	if (rtype::Client::getInstance()->getEvent().type == sf::Event::KeyPressed) {
		if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Return) {
			if (this->_player->getName().length() <= 1)
				this->_player->setName("Player1");
			this->_player->setInstance(this->_player);
			_request = rtype::Protocol::generateSetNameRequest(this->_player->getName());
			_serializedObject = _request->serialize();
			_serv->sendData(_serializedObject.first, _serializedObject.second);
			delete _request;

			_rcvData = _serv->receiveData();
			_response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
			if (_response.getIdMessage() == Protocol::IDMessage::RESPONSE) {
				_responseMeta.unserialize(_response.getBodyUnserialized());
				std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
				if (status ==  std::to_string(rtype::OK))
					_endScene = true;
			}
		}
		else if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Escape)
				this->getDisplayManager().closeWindow();
	}
}

void rtype::scene::MainMenu::displayAll()
{
  	_elapsedT = getClock()->getElapsedTime();
    if (_elapsedT.asSeconds() > 0.01 && _endScene == true) {
            _posD -= 20;
        this->getClock()->restart();
    }

	if (_posD == -860)
		rtype::scene::Scene::setActiveScene<rtype::scene::GameSelection>();

    if (this->_par->getPos().x <= -1840)
        this->_par->setPos((sf::Vector2i){0, 0});
    if (this->_parr->getPos().x <= -1840)
        this->_parr->setPos((sf::Vector2i){0, 0});
    if (this->_lune->getPos().x <= -1000)
        this->_lune->setPos((sf::Vector2i){2000, 0});
    if (this->_earth->getPos().x <= -1500)
        this->_earth->setPos((sf::Vector2i){2134, 0});
    if (this->_red->getPos().x <= -670)
        this->_red->setPos((sf::Vector2i){4896, 0});
    if (this->_pp->getPos().x <= -760)
        this->_pp->setPos((sf::Vector2i){2345, 0});
    this->getDisplayManager().drawButton(this->_par);
    // this->getDisplayManager().drawButton(this->_parr);
    this->getDisplayManager().drawButton(this->_lune);
    this->getDisplayManager().drawButton(this->_pp);
    this->getDisplayManager().drawButton(this->_red);
    this->getDisplayManager().drawButton(this->_earth);
    if (_elapsedT.asSeconds() < 0.5) {
        this->_par->setPos((sf::Vector2i){this->_par->getPos().x - 1, 0});
        this->_parr->setPos((sf::Vector2i){this->_parr->getPos().x - 2, 10});
        this->_lune->setPos((sf::Vector2i){this->_lune->getPos().x - 2, -100});
        this->_earth->setPos((sf::Vector2i){this->_earth->getPos().x - 4, 450});
        this->_red->setPos((sf::Vector2i){this->_red->getPos().x - 3, 800});
        this->_pp->setPos((sf::Vector2i){this->_pp->getPos().x - 3, 450});
        this->getDisplayManager().drawText(Vector2<int>(_width/2, _posD + 820), "Press ENTER to continue", Color(0, 0, 0, 0), 0);
        this->getDisplayManager().drawText(Vector2<int>(_width/2 - 240, _posD + 820), "Press ENTER to continue", Color(255, 255, 255, _intensity), 30);
        _intensity = (_intensity - 1 < _intensity) ? _intensity -1 : _intensity + 1;
        this->getClock()->restart();
    }
	this->getDisplayManager().drawText(Vector2<int>(_width/2,_posD + 30), "RTYPE", Color(0, 0, 0, 0), 0);
	this->getDisplayManager().drawText(Vector2<int>(_width/2 - 100,_posD + 35), "RTYPE", Color(255, 0, 0, 255), 50);
	// if (this->_to_write == 1)
		// this->getDisplayManager().drawSquare(Vector2<int>(_width/2, 135), Vector2<int>(400, 80), "", Color(0, 0, 255, 255));
	if (_hover == true || this->_to_write == 1)
		this->getDisplayManager().drawSquare(Vector2<int>(_width/2,_posD + 245), Vector2<int>(380, 60), "", Color(255, 255, 255, 255));
	this->_name->setPosition((sf::Vector2i){_width/2,_posD + 42});
	this->getDisplayManager().drawButton(_name);
	if (this->_player->getSizeName() == 0) {
		this->getDisplayManager().drawText(Vector2<int>(_width/2 + 217,_posD + 300), "Enter your name here", Color(105, 105, 105, 100), 23);
		if (_hover == true || this->_to_write == 1)
			this->getDisplayManager().drawText(Vector2<int>(_width/2,_posD + 280), "Enter your name here", Color(105, 105, 105, 255), 23);
	}
	else if (this->_player->getSizeName() == 1)
		this->getDisplayManager().drawText(Vector2<int>(_width/2 + 35,_posD + 285), this->_player->getName(), Color(0, 0, 0, 255), 35);

	_mouse->setPos((sf::Vector2i){sf::Mouse::getPosition().x - 20, sf::Mouse::getPosition().y - 85});
    this->getDisplayManager().drawSprite(this->_mouse);

	this->getDisplayManager().displayUpdate();
}

rtype::scene::MainMenu::~MainMenu()
{
}