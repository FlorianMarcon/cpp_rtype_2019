/*
** EPITECH PROJECT, 2018
** OOP_indie_studio_2018
** File description:
** SelectPlayer.cpp
*/

#include <iostream>
#include "SelectPlayer.hpp"
#include "Room.hpp"
#include "../Client/Client.hpp"
#include "Room.hpp"
#include "Game.hpp"

rtype::scene::SelectPlayer::SelectPlayer() : Scene(
                                                   "RTYPE")
{
	sf::IntRect rect = this->getDisplayManager().setRectangle(0, 0, _width, _height);
    this->_player =  rtype::scene::Scene::getSceneWithName("Room Selection").getPlayer()->getInstance();
	this->_next = new Button("", (sf::Vector2i){_width - _width/6, 800}, rect, (sf::Vector2f){260, 60}, Color(255, 255, 255, 150));
	this->_back = new Button("", (sf::Vector2i){_width - _width/6, 900}, rect, (sf::Vector2f){260, 60}, Color(255, 255, 255, 150));
    _request = rtype::Protocol::generateListPlayerRequest();
    _serializedObject = _request->serialize();
	_background = new Sprite("../client/src/Font/2.jpg", (sf::Vector2i){0, 0}, rect, 0);

    _serv->sendData(_serializedObject.first, _serializedObject.second);
	delete _request;

	_rcvData = _serv->receiveData();
	_response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
    if (_response.getIdMessage() == Protocol::IDMessage::RESPONSE) {
		_responseMeta.unserialize(_response.getBodyUnserialized());
        std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
		if (status ==  std::to_string(rtype::OK)) {
        	_players = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_PLAYERS));
            size_t pos = 0;
            while ((pos = _players.find(",")) != std::string::npos) {
                _token.push_back(_players.substr(0, pos));
                _players.erase(0, pos + 1);
            }
            _token.push_back(_players.substr(0, pos));
        }

    }
    _posD = 0;
	_endScene = false;
    _choiceY = 250;
}

void rtype::scene::SelectPlayer::toDo(){
	if (rtype::Client::getInstance()->getEvent().type == sf::Event::KeyPressed) {
		if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Escape)
			this->getDisplayManager().closeWindow();
        if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Return)
            _endScene = true;
    }
    if (this->_next->isHover(sf::Mouse::getPosition()))
        this->_hover = true;
    else
        this->_hover = false;

    if (this->_back->isHover(sf::Mouse::getPosition()))
        this->_hover2 = true;
    else
        this->_hover2 = false;

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && this->_next->isHover(sf::Mouse::getPosition()))
        _endScene = true;
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && _hover2 == true)
        rtype::scene::Scene::setActiveScene<rtype::scene::Room>();
}

void rtype::scene::SelectPlayer::displayAll()
{
	_elapsedT = getClock()->getElapsedTime();
    if (_elapsedT.asSeconds() > 0.01 && _endScene == true) {
            _posD -= 20;
        this->getClock()->restart();
    }

	if (_posD <= -1060)
		rtype::scene::Scene::setActiveScene<rtype::scene::Game>();


   if (this->_par->getPos().x <= -1840)
        this->_par->setPos((sf::Vector2i){0, 0});
    if (this->_parr->getPos().x <= -1840)
        this->_parr->setPos((sf::Vector2i){0, 0});
    if (this->_lune->getPos().x <= -1000)
        this->_lune->setPos((sf::Vector2i){2000, 0});
    if (this->_earth->getPos().x <= -1500)
        this->_earth->setPos((sf::Vector2i){2134, 0});
    if (this->_red->getPos().x <= -670)
        this->_red->setPos((sf::Vector2i){4896, 0});
    if (this->_pp->getPos().x <= -760)
        this->_pp->setPos((sf::Vector2i){2345, 0});
    this->getDisplayManager().drawButton(this->_par);
    // this->getDisplayManager().drawButton(this->_parr);
    this->getDisplayManager().drawButton(this->_lune);
    this->getDisplayManager().drawButton(this->_pp);
    this->getDisplayManager().drawButton(this->_red);
    this->getDisplayManager().drawButton(this->_earth);
    if (_elapsedT.asSeconds() < 0.5) {
        this->_par->setPos((sf::Vector2i){this->_par->getPos().x - 1, 0});
        this->_parr->setPos((sf::Vector2i){this->_parr->getPos().x - 2, 10});
        this->_lune->setPos((sf::Vector2i){this->_lune->getPos().x - 2, -100});
        this->_earth->setPos((sf::Vector2i){this->_earth->getPos().x - 4, 450});
        this->_red->setPos((sf::Vector2i){this->_red->getPos().x - 3, 800});
        this->_pp->setPos((sf::Vector2i){this->_pp->getPos().x - 3, 450});
        this->getClock()->restart();
    }
    this->getDisplayManager().drawText(Vector2<int>(_width/2 - 30, _posD + 55), this->_player->getNameRoom(), Color(0, 255, 255, 155), 50);
    this->getDisplayManager().drawText(Vector2<int>(_width - _width/6 + 50, _posD + 130), "Players Online:", Color(0, 255, 255, 155), 35);
    this->getDisplayManager().drawSquare(Vector2<int>(_width - _width/6, _posD + 160), Vector2<int>(280, 600), "", Color(255, 255, 255, 230));
    this->getDisplayManager().drawText(Vector2<int>(_width - _width/6 + 14, _posD + 195), this->_player->getName(), Color(0, 255, 0), 30);
   _choiceY = 195;
    for (unsigned int i = 0; i < _token.size(); i++) {
        this->getDisplayManager().drawText(Vector2<int>(_width - _width/6 + 8, _posD + _choiceY), _token.at(i), Color(0, 255, 0), 30);
        _choiceY += 55;
    }
    if(_hover)
        this->getDisplayManager().drawSquare(Vector2<int>(_width - _width/6, _posD + 800), Vector2<int>(260, 60), "", Color(255, 255, 255, 255));
    if(_hover2)
        this->getDisplayManager().drawSquare(Vector2<int>(_width - _width/6, _posD + 900), Vector2<int>(260, 60), "", Color(255, 255, 255, 255));
    this->_back->setPos((sf::Vector2i){_width - _width/6, _posD + 900});
    this->_next->setPos((sf::Vector2i){_width - _width/6,  _posD + 800});
    this->getDisplayManager().drawButton(_next);
    this->getDisplayManager().drawButton(_back);
    this->getDisplayManager().drawText(Vector2<int>(_width - _width/6,_posD +  830), "PLAY", Color(0, 0, 0), 30);
    this->getDisplayManager().drawText(Vector2<int>(_width - _width/6,_posD +  930), "BACK", Color(0, 0, 0), 30);
    // this->getDisplayManager().drawSquare(Vector2<int>(_width/2, _posD + 160), Vector2<int>(900, 700), "", Color(0, 0, 0, 150));
   	_mouse->setPos((sf::Vector2i){sf::Mouse::getPosition().x - 20, sf::Mouse::getPosition().y - 85});
    this->getDisplayManager().drawSprite(this->_mouse);
    this->getDisplayManager().displayUpdate();
}

rtype::scene::SelectPlayer::~SelectPlayer()
{
}
