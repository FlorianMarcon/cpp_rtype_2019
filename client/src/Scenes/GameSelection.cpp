/*
** EPITECH PROJECT, 2018
** OOP_indie_studio_2018
** File description:
** GameSelection.cpp
*/

#include <iostream>
#include "GameSelection.hpp"
#include "Room.hpp"
#include "MainMenu.hpp"
#include "../Client/Client.hpp"

rtype::scene::GameSelection::GameSelection() : Scene(
                                         "Game Selection")
{
    this->_player =  rtype::scene::Scene::getSceneWithName("Main Menu").getPlayer()->getInstance();
	this->_player->setName(this->_player->getName().erase(this->_player->getName().size() - 1, 1));

    pos_name = -850;
    _side = true;
    sf::IntRect rect = this->getDisplayManager().setRectangle(0, 0, _width, _height);
	// _par = new Sprite("../client/src/Font/night.png", (sf::Vector2i){0, 0}, rect, 0);
    // _par = rtype::scene::Scene::getPar();
    // _earth = rtype::scene::Scene::getEarth();
    // _lune = rtype::scene::Scene::getLune();
    // _red = rtype::scene::Scene::getRed();
    // _pp = rtype::scene::Scene::getPP();

    _request = rtype::Protocol::generateListGamesRequest();
    _serializedObject = _request->serialize();

    _serv->sendData(_serializedObject.first, _serializedObject.second);
	delete _request;

	_rcvData = _serv->receiveData();
	_response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
    if (_response.getIdMessage() == rtype::Protocol::IDMessage::RESPONSE) {
		_responseMeta.unserialize(_response.getBodyUnserialized());
        std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
		if (status ==  std::to_string(rtype::OK)) {
        	_games = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_GAMES));
            size_t pos = 0;
            while ((pos = _games.find(",")) != std::string::npos) {
                if (_games.substr(0, pos).size() != 0)
                    _token.push_back(_games.substr(0, pos));
                _games.erase(0, pos + 1);
            }
            if (_games.substr(0, pos).size() != 0)
                _token.push_back(_games.substr(0, pos));
        }
    }

    _request = rtype::Protocol::generateListPlayerRequest();
    _serializedObject = _request->serialize();

    _serv->sendData(_serializedObject.first, _serializedObject.second);
	delete _request;

	_rcvData = _serv->receiveData();
	_response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
    if (_response.getIdMessage() == rtype::Protocol::IDMessage::RESPONSE) {
		_responseMeta.unserialize(_response.getBodyUnserialized());
        std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
		if (status ==  std::to_string(rtype::OK)) {
        	_players = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_PLAYERS));
            size_t pos = 0;
            while ((pos = _players.find(",")) != std::string::npos) {
                _token2.push_back(_players.substr(0, pos));
                _players.erase(0, pos + 1);
            }
            _token2.push_back(_players.substr(0, pos));
        }

    }


    _choiceY = 400;
    _choiceM = 400;
    _caseM = 0;
    for (unsigned int i = 0; i < _token.size(); i++) {
        _buttons.push_back(new Button("", (sf::Vector2i){_width/2, 110 + _choiceY}, rect, (sf::Vector2f){(float)300, 50}, Color(255, 255, 255, 0)));
        _choiceY += 80;
    }
    _isMoving = true;
    _intensity = 255;
	_intensity2 = 0;
}

void rtype::scene::GameSelection::toDo()
{
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
        for (unsigned int i = 0; i < _buttons.size(); i++) {
	        if (_buttons.at(i)->isHover(sf::Mouse::getPosition())) {
                 _side = false;
                _isMoving = true;
            }
        }
    }

    _choiceY = 400;
    for (unsigned int i = 0; i < _buttons.size(); i++) {
	    if (_buttons.at(i)->isHover(sf::Mouse::getPosition())) {
            _choiceM = _choiceY;
            _caseM = i;
        }
        _choiceY += 80;
    }


    if (rtype::Client::getInstance()->getEvent().type == sf::Event::KeyPressed)
    {
        if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Return) {
            _side = false;
            _isMoving = true;
        }
        if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Down && _caseM + 1 < _token.size()) {
           _caseM += 1;
           _choiceM += 80;
        }
        if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Up && _choiceM - 80 > 380) {
            _caseM -= 1;
            _choiceM -= 80;
        }
        if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Escape)
            this->getDisplayManager().closeWindow();
    }
}

void rtype::scene::GameSelection::displayAll()
{
    _elapsedT = getClock()->getElapsedTime();
    if (_elapsedT.asSeconds() > 0.01 && _isMoving == true) {
        if (pos_name < 100 && _side == true)
            pos_name += 20;
        else if (pos_name > -860 && _side == false)
            pos_name -= 20;
        this->getClock()->restart();
    }

    if (pos_name == 110)
        _isMoving = false;
    else if (pos_name == -870) {
        this->_player->setNameGame(_token.at(_caseM));
        rtype::scene::Scene::setActiveScene<rtype::scene::Room>();
    }

    if (this->_par->getPos().x <= -1840)
        this->_par->setPos((sf::Vector2i){0, 0});
    if (this->_parr->getPos().x <= -1840)
        this->_parr->setPos((sf::Vector2i){0, 0});
    if (this->_lune->getPos().x <= -1000)
        this->_lune->setPos((sf::Vector2i){2000, 0});
    if (this->_earth->getPos().x <= -1500)
        this->_earth->setPos((sf::Vector2i){2134, 0});
    if (this->_red->getPos().x <= -670)
        this->_red->setPos((sf::Vector2i){4896, 0});
    if (this->_pp->getPos().x <= -760)
        this->_pp->setPos((sf::Vector2i){2345, 0});
    this->getDisplayManager().drawButton(this->_par);
    // this->getDisplayManager().drawButton(this->_parr);
    this->getDisplayManager().drawButton(this->_lune);
    this->getDisplayManager().drawButton(this->_pp);
    this->getDisplayManager().drawButton(this->_red);
    this->getDisplayManager().drawButton(this->_earth);
    if (_elapsedT.asSeconds() < 0.5) {
        this->_par->setPos((sf::Vector2i){this->_par->getPos().x - 1, 0});
        this->_parr->setPos((sf::Vector2i){this->_parr->getPos().x - 2, 10});
        this->_lune->setPos((sf::Vector2i){this->_lune->getPos().x - 2, -100});
        this->_earth->setPos((sf::Vector2i){this->_earth->getPos().x - 4, 450});
        this->_red->setPos((sf::Vector2i){this->_red->getPos().x - 3, 800});
        this->_pp->setPos((sf::Vector2i){this->_pp->getPos().x - 3, 450});
        _intensity = (_intensity - 1 < _intensity) ? _intensity -1 : _intensity + 1;
        if (_intensity2 > 255)
			_intensity2 = 0;
		_intensity2 += 1;
        this->getClock()->restart();
    }
    this->getDisplayManager().drawText(Vector2<int>(_width/6, pos_name + 400), "<Use arrows keys to naviguate>", Color(0, 0, 0, 0), 20);
    this->getDisplayManager().drawText(Vector2<int>(_width/6 - 40, pos_name + 400), "<Use arrows keys to naviguate or", Color(255, 255, 255, _intensity2), 25);
    this->getDisplayManager().drawText(Vector2<int>(_width/6 + 30, pos_name + 430), "use your mouse to select any game>", Color(255, 255, 255, _intensity2), 25);
    this->getDisplayManager().drawText(Vector2<int>(_width/2 - 70, pos_name + 820), "<Choose a game to continue>", Color(255, 255, 255, 255), 35);

    this->getDisplayManager().drawText(Vector2<int>(_width - _width/6 + 50, pos_name + 130), "Players Online:", Color(0, 255, 255, 155), 25);
    this->getDisplayManager().drawSquare(Vector2<int>(_width - _width/6, pos_name + 160), Vector2<int>(280, 600), "", Color(255, 255, 255, 230));
    this->getDisplayManager().drawText(Vector2<int>(_width - _width/6 + 14, pos_name + 195), this->_player->getName(), Color(0, 255, 0), 20);
   _choiceY = 195;
    for (unsigned int i = 0; i < _token2.size(); i++) {
        this->getDisplayManager().drawText(Vector2<int>(_width - _width/6 + 8, pos_name + _choiceY), _token2.at(i), Color(0, 255, 0), 30);
        _choiceY += 55;
    }


    // this->getDisplayManager().drawText(Vector2<int>(_width/2 - _width/8, pos_name + _choiceM + 10), "~>", Color(255, 0, 0, 255), 40);
    _choiceY = 480;
    for (unsigned int i = 0; i < _token.size(); i++) {
        if (i != _caseM)
            this->getDisplayManager().drawText(Vector2<int>(_width/2,pos_name +  _choiceY), _token.at(i), Color(255, 255, 255, 255), 30);
        _choiceY += 80;
    }
    this->getDisplayManager().drawText(Vector2<int>(_width/2 - 35, pos_name + _choiceM), _token.at(_caseM), Color(255, 0, 0, 255), 65);

    this->getDisplayManager().drawText(Vector2<int>(_width/2, 20), "Welcome " + this->_player->getName(), Color(0, 0, 0, 0), 0);
    this->getDisplayManager().drawText(Vector2<int>(_width/3 + 120, pos_name - 20), "Welcome " + this->_player->getName() + " !", Color(0, 255, 255, 155), 40);
	_mouse->setPos((sf::Vector2i){sf::Mouse::getPosition().x - 20, sf::Mouse::getPosition().y - 85});
    this->getDisplayManager().drawSprite(this->_mouse);
    this->getDisplayManager().displayUpdate();
}

rtype::scene::GameSelection::~GameSelection()
{
}
