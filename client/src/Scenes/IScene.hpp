/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** IScene
*/

#ifndef ISCENE_HPP_
#define ISCENE_HPP_

//#include "../Items/IItems.hpp"
#include <list>
#include <iostream>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

namespace rtype
{
    namespace scene {

        class IScene {
        public:
                IScene(std::string name);
                ~IScene();
                std::string getName() const;
                void displayAll();
                static void setActiveScene(IScene &scene);
                // template <typename T>
                // static void setActiveScene() {
                //     if (_activeScene)
                //         _activeScene->onQuit();
                //     Client::getInstance()->getDisplayManager()->displayUpdate();
                //     _activeScene = new T();
                //     std::cout << "Switching to scene \"" << _activeScene->getName() << "\"" << std::endl;
                // }
                static void drawActiveScene();
                static IScene getSceneWithName(std::string scene);
                static IScene *getActiveScene();
                virtual void drawGUI();
                virtual void onQuit();
            protected:
            private:
                static IScene *_activeScene;
                static std::map<std::string, IScene *> _sceneList;
                std::string _name;
                //std::list<IItems> _items;
        };

    }
}

#endif /* !ISCENE_HPP_ */
