/*
** EPITECH PROJECT, 2018
** OOP_indie_studio_2018
** File description:
** Room.cpp
*/

#include <iostream>
#include "Room.hpp"
#include "Protocol/Protocol.hpp"
#include "SelectPlayer.hpp"
#include "GameSelection.hpp"
#include "../Client/Client.hpp"

rtype::scene::Room::Room() : Scene(
										 "Room Selection")
{
	pos_name = -850;
	_side = true;
	this->_player =  rtype::scene::Scene::getSceneWithName("Game Selection").getPlayer()->getInstance();
	sf::IntRect rect = this->getDisplayManager().setRectangle(0, 0, _width, this->getDisplayManager().getHeight());

	_request = rtype::Protocol::generateListRoomRequest(this->_player->getNameGame());
	_serializedObject = _request->serialize();

	_serv->sendData(_serializedObject.first, _serializedObject.second);
	delete _request;

	_rcvData = _serv->receiveData();
	_response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
	if (_response.getIdMessage() == rtype::Protocol::IDMessage::RESPONSE) {
		_responseMeta.unserialize(_response.getBodyUnserialized());
		std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
		if (status ==  std::to_string(rtype::OK)) {
			_games = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_ROOMS));
			// _games += "ok,mathieu, non,erezr,";
			size_t pos = 0;
			while ((pos = _games.find(",")) != std::string::npos) {
				if (_games.substr(0, pos).size() != 0)
					_token.push_back(_games.substr(0, pos));
				_games.erase(0, pos + 1);
			}
			if (_games.substr(0, pos).size() != 0)
				_token.push_back(_games.substr(0, pos));
			if (_token.size() > 10) {
				while (_token.size() > 10)
					_token.pop_back();
			}
		}
	}

	_choiceY = 100;
	_choiceM = 100;
	_caseM = 0;
	if (_token.size() != 0) {
		for (unsigned int i = 0; i < _token.size(); i++) {
			_buttons.push_back(new Button("", (sf::Vector2i){_width/2, 110 + _choiceY}, rect, (sf::Vector2f){(float)300, 50}, Color(255, 255, 255, 255)));
			_choiceY += 80;
		}
	}
	_isMoving = true;
	_intensity = 255;
	_intensity2 = 0;
	this->_next = new Button("", (sf::Vector2i){_width/2, 800}, rect, (sf::Vector2f){400, 60}, Color(255, 255, 255, 150));
	this->_back = new Button("", (sf::Vector2i){_width - _width/6, 900}, rect, (sf::Vector2f){260, 60}, Color(255, 255, 255, 150));
	_setName = false;
	_bar = 0;
	sf::IntRect rectN = this->getDisplayManager().setRectangle(0, 0, _width, _height);
	this->_name = new Button("../client/src/Font/name2.png", (sf::Vector2i){_width/2,pos_name - 800 + 245}, rectN, (sf::Vector2f){1000, 506 * 0.4}, Color(255, 255, 255, 255));
	this->_name->setScale((sf::Vector2f){1, 0.4});
	// this->_name = new Button("", (sf::Vector2i){_width/2,pos_name - 800 + 245}, rectN, (sf::Vector2f){390, 60}, Color(255, 255, 255, 210));
	this->_scrollbar = new Button("", (sf::Vector2i){_width/2 + 335, pos_name + 105 + _bar * 80}, rectN, (sf::Vector2f){15, 438 - 6 * 80}, Color(0, 0, 255, 150));
	this->_to_write = 0;
	_scroll = 0;

    // _par = rtype::scene::Scene::getPar();
    // _earth = rtype::scene::Scene::getEarth();
    // _lune = rtype::scene::Scene::getLune();
    // _red = rtype::scene::Scene::getRed();
    // _pp = rtype::scene::Scene::getPP();
}

void rtype::scene::Room::toDo()
{
	if (this->_next->isHover(sf::Mouse::getPosition()))
		this->_hover = true;
	else
		this->_hover = false;


	if (this->_back->isHover(sf::Mouse::getPosition()))
		this->_hover2 = true;
	else
		this->_hover2 = false;

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && (_hover == false || _hover2 == false) && ( _token.size() != 0 && _setName == false)) {
		for (unsigned int i = 0; i < _buttons.size(); i++) {
			if (_buttons.at(i)->isHover(sf::Mouse::getPosition())) {
				this->_player->setNameRoom(_token.at(_caseM));
				this->_player->setInstance(this->_player);
				_side = false;
				_isMoving = true;
			}
		}
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && _hover2 == true)
		rtype::scene::Scene::setActiveScene<rtype::scene::GameSelection>();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && _hover == true) {
		this->_player->setNameRoom("");
		_isMoving = true;
		_setName = true;
		_side = false;
	}
	_choiceY = 100;
	if (_token.size() != 0 && _setName == false) {
		for (unsigned int i = 0; i < _buttons.size(); i++) {
			if (_buttons.at(i)->isHover(sf::Mouse::getPosition()) && (sf::Mouse::getPosition().y >= 100 && sf::Mouse::getPosition().y <= 700)) {
				_choiceM = _choiceY;
				_caseM = i;
			}
			_choiceY += 80;
		}
	}

	if (this->_name->isHover(sf::Mouse::getPosition()) && _to_write != 1)
		_hover3 = true;
	else
		_hover3 = false;
	if (this->_player->getNameRoom().length() == 0 && this->_to_write == 0)
		this->_player->setSizeName(0);
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && this->_name->isHover(sf::Mouse::getPosition())) {
		this->_to_write = 1;
		this->_player->setSizeName(1);
		_hover3 = false;
	}
	else if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !this->_name->isHover(sf::Mouse::getPosition()))
		this->_to_write = 0;
	if (rtype::Client::getInstance()->getEvent().type == sf::Event::TextEntered && this->_to_write == 1)
	{
		this->_player->setSizeName(1);
		if (rtype::Client::getInstance()->getEvent().text.unicode == '\b' && this->_player->getNameRoom().length())
			this->_player->setNameRoom(this->_player->getNameRoom().erase(this->_player->getNameRoom().size() - 1, 1));
		else if (this->_player->getNameRoom().length() <= 7 && rtype::Client::getInstance()->getEvent().text.unicode < 128)
			this->_player->setNameRoom(this->_player->getNameRoom() += static_cast<char>(rtype::Client::getInstance()->getEvent().text.unicode));
	}

	if (rtype::Client::getInstance()->getEvent().type == sf::Event::KeyPressed)
	{
		if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Return && _setName == true) {
			if (this->_player->getNameRoom().length() <= 1)
				this->_player->setNameRoom("Room");
			this->_player->setInstance(this->_player);

			_request = rtype::Protocol::generateCreateRoomRequest(this->_player->getNameGame(), this->_player->getNameRoom());
			_serializedObject = _request->serialize();
			_serv->sendData(_serializedObject.first, _serializedObject.second);
			delete _request;

			_rcvData = _serv->receiveData();
			_response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
			if (_response.getIdMessage() == Protocol::IDMessage::RESPONSE) {
				_responseMeta.unserialize(_response.getBodyUnserialized());
				std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
				if (status ==  std::to_string(rtype::OK)) {
					rtype::scene::Scene::setActiveScene<rtype::scene::SelectPlayer>();
				}
			}
		}

		if (_token.size() != 0) {
			if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Return && _setName == false) {
				 this->_player->setNameRoom(_token.at(_caseM));
				_side = false;
				_isMoving = true;
			}
			if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Down && _caseM + 1 < _token.size()) {
				if (_choiceM + _scroll < 500) {
				 	_caseM += 1;
					_choiceM += 80;
				} else if (_caseM >= 5 && _choiceM + _scroll >= 500) {
					_scroll -= 80;
					_caseM += 1;
					_choiceM += 80;
					_bar += 1;
				}
			}
			if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Up && _choiceM - 80 > 80) {
				if (_choiceM + _scroll <= 100) {
					_caseM -= 1;
					_choiceM -= 80;
					_scroll += 80;
					_bar -= 1;
				} else if (_choiceM + _scroll > 100) {
					_caseM -= 1;
					_choiceM -= 80;
				}
			}
		}
		if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Escape)
			this->getDisplayManager().closeWindow();
	}
}

void rtype::scene::Room::displayAll()
{
	_elapsedT = getClock()->getElapsedTime();
	if (_elapsedT.asSeconds() > 0.01 && _isMoving == true) {
		if (pos_name < 100 && _side == true && _setName == false)
			pos_name += 20;
		else if (pos_name > -860 && _side == false && _setName == false)
			pos_name -= 20;
		else if (pos_name < 1200 && _side == false && _setName == true)
			pos_name += 20;
		this->getClock()->restart();
	}

	if (pos_name - 1400 == 110)
		_isMoving = false;
	else if (pos_name == -870)
		rtype::scene::Scene::setActiveScene<rtype::scene::SelectPlayer>();

    if (this->_par->getPos().x <= -1840)
    	this->_par->setPos((sf::Vector2i){0, 0});
    if (this->_parr->getPos().x <= -1840)
        this->_parr->setPos((sf::Vector2i){0, 0});
    if (this->_lune->getPos().x <= -1000)
        this->_lune->setPos((sf::Vector2i){2000, 0});
    if (this->_earth->getPos().x <= -1500)
    	this->_earth->setPos((sf::Vector2i){2134, 0});
    if (this->_red->getPos().x <= -670)
    	this->_red->setPos((sf::Vector2i){4896, 0});
    if (this->_pp->getPos().x <= -760)
		this->_pp->setPos((sf::Vector2i){2345, 0});

    this->getDisplayManager().drawButton(this->_par);
    // this->getDisplayManager().drawButton(this->_parr);
    this->getDisplayManager().drawButton(this->_lune);
    this->getDisplayManager().drawButton(this->_pp);
    this->getDisplayManager().drawButton(this->_red);
    this->getDisplayManager().drawButton(this->_earth);
	if (_elapsedT.asSeconds() < 0.5 && _setName == false) {
		this->_par->setPos((sf::Vector2i){this->_par->getPos().x - 1, 0});
		this->_parr->setPos((sf::Vector2i){this->_parr->getPos().x - 2, 10});
		this->_lune->setPos((sf::Vector2i){this->_lune->getPos().x - 2, -100});
		this->_earth->setPos((sf::Vector2i){this->_earth->getPos().x - 4, 450});
		this->_red->setPos((sf::Vector2i){this->_red->getPos().x - 3, 800});
		this->_pp->setPos((sf::Vector2i){this->_pp->getPos().x - 3, 450});
		_intensity = (_intensity - 1 < _intensity) ? _intensity -1 : _intensity + 1;
		if (_intensity2 > 255)
			_intensity2 = 0;
		_intensity2 += 1;
		this->getClock()->restart();
	} else if (_elapsedT.asSeconds() < 0.5 && _setName == true) {
		this->_par->setPos((sf::Vector2i){this->_par->getPos().x - 1, 0});
		this->_parr->setPos((sf::Vector2i){this->_parr->getPos().x - 2, 10});
		this->_lune->setPos((sf::Vector2i){this->_lune->getPos().x - 4, -100});
		this->_earth->setPos((sf::Vector2i){this->_earth->getPos().x - 1, 450});
		this->_red->setPos((sf::Vector2i){this->_red->getPos().x - 8, 800});
		this->_pp->setPos((sf::Vector2i){this->_pp->getPos().x - 6, 450});
	}
	this->getDisplayManager().drawText(Vector2<int>(_width/6, pos_name), "Use arrows keys to naviguate", Color(0, 0, 0, 0), 20);
	this->getDisplayManager().drawText(Vector2<int>(_width/6 - 40, pos_name +  100), "Use arrows keys to naviguate", Color(255, 255, 255, _intensity), 20);
	this->getDisplayManager().drawText(Vector2<int>(_width/6 - 40, pos_name + 130), "Use your mouse to select any game", Color(255, 255, 255, _intensity), 20);
	this->getDisplayManager().drawText(Vector2<int>(_width/6 - 40, pos_name + 160), "or press enter to continue", Color(255, 255, 255, _intensity), 20);
	this->getDisplayManager().drawText(Vector2<int>(_width/2 - 50, pos_name + 820), "Select a room or create a new one", Color(255, 255, 255, 255), 25);

	// this->getDisplayManager().drawText(Vector2<int>(_width/2 - _width/6,pos_name - 800 - 220), "Name:", Color(255, 255, 255, 255), 35);
	// if (_hover3 == true || this->_to_write == 1)
		// this->getDisplayManager().drawSquare(Vector2<int>(_width/2,pos_name - 800 - 245), Vector2<int>(390, 60), "", Color(255, 255, 255, 255));
	this->_name->setPosition((sf::Vector2i){_width/2,pos_name - 800 - 245});
	this->getDisplayManager().drawButton(_name);
	if (this->_player->getSizeName() == 0) {
		this->getDisplayManager().drawText(Vector2<int>(_width/2 - 32,pos_name - 820), "Enter room name here", Color(105, 105, 105, 100), 30);
		if (_hover3 == true || this->_to_write == 1)
			this->getDisplayManager().drawText(Vector2<int>(_width/2,pos_name - 817), "Enter room name here", Color(105, 105, 105, 255), 30);
			// this->getDisplayManager().drawText(Vector2<int>(_width/2 + 55,pos_name - 820 - 210), "Enter room name here", Color(105, 105, 105, 100), 15);
	}
	else if (this->_player->getSizeName() == 1)
		this->getDisplayManager().drawText(Vector2<int>(_width/2 - 20,pos_name - 820), this->_player->getNameRoom(), Color(0, 0, 0, 255), 30);


	if (_token.size() > 6) {
		this->getDisplayManager().drawSquare(Vector2<int>(_width/2, pos_name + 90), Vector2<int>(700, 460), "", Color(0, 0, 0, 110));
		this->getDisplayManager().drawSquare(Vector2<int>(_width/2 + 335, pos_name + 105), Vector2<int>(15, 438), "", Color(255, 255, 255, 200));
		this->_scrollbar->setPos((sf::Vector2i){_width/2 + 335, pos_name + 105 + _bar * 80});
		this->_scrollbar->setSize((sf::Vector2f){15, 438 - ((float)_token.size()-6) * 80});
		this->getDisplayManager().drawButton(this->_scrollbar);
	}

	_choiceY = 100 + _scroll;
	if (_token.size() != 0	&&	(_side == true && _isMoving == true)) {
		for (unsigned int i = 0; i < _token.size() ; i++) {
			if (i != _caseM && (pos_name +  _choiceY >= 180 && pos_name +  _choiceY <= 660))
				this->getDisplayManager().drawText(Vector2<int>(_width/2 - 20,pos_name +  _choiceY), _token.at(i), Color(255, 255, 255, 255), 30);
			_buttons.at(i)->setPos((sf::Vector2i){_width/2,pos_name +  _choiceY});
 			_choiceY += 80;
		}
		if (pos_name +  _choiceM + _scroll >= 180 && pos_name +  _choiceM + _scroll <= 660) {
			// this->getDisplayManager().drawText(Vector2<int>(_width/2 - _width/8, pos_name + (_choiceM + _scroll) + 10), "~>", Color(255, 0, 0, 255), 40);
			this->getDisplayManager().drawText(Vector2<int>(_width/2 - 50, pos_name + _choiceM + _scroll), _token.at(_caseM), Color(255, 0, 0, 255), 55);
		}
	} else if (_token.size() == 0)
		this->getDisplayManager().drawText(Vector2<int>(_width/2, pos_name + _choiceM), "No rooms available", Color(0, 255, 255, 155), 35);


	if(_hover)
		this->getDisplayManager().drawSquare(Vector2<int>(_width/2, pos_name + 600), Vector2<int>(400, 60), "", Color(255, 255, 255, 255));

	if(_hover2)
		this->getDisplayManager().drawSquare(Vector2<int>(_width - _width/6, pos_name + 800), Vector2<int>(260, 60), "", Color(255, 255, 255, 255));
	this->_next->setPos((sf::Vector2i){_width/2,  pos_name + 600});
	this->_back->setPos((sf::Vector2i){_width - _width/6, pos_name + 800});
	this->getDisplayManager().drawButton(_next);
	this->getDisplayManager().drawText(Vector2<int>(_width/2 + 160 ,pos_name +  650), "Create a new Room", Color(0, 0, 0, 0), 0);
	this->getDisplayManager().drawText(Vector2<int>(_width/2 - 160,pos_name +  610), "Create a new Room", Color(0, 0, 0), 25);
	this->getDisplayManager().drawButton(_back);
	this->getDisplayManager().drawText(Vector2<int>(_width - _width/6,pos_name +  830), "BACK", Color(0, 0, 0), 30);

	_mouse->setPos((sf::Vector2i){sf::Mouse::getPosition().x - 20, sf::Mouse::getPosition().y - 85});
    this->getDisplayManager().drawSprite(this->_mouse);
	this->getDisplayManager().displayUpdate();

}

rtype::scene::Room::~Room()
{
}
