/*
** EPITECH PROJECT, 2018
** RTYPE_2019
** File description:
** Room.hpp
*/

#ifndef RTYPE_2019_Room_HPP
#define RTYPE_2019_Room_HPP

#include "Scene.hpp"
#include <vector>
#include <algorithm>
#include "../Items/Player.hpp"
#include "../Items/Vector2.hpp"
#include "../Items/Items.hpp"

namespace rtype
{
    namespace scene
    {
        class Room : public rtype::scene::Scene
        {
        public:
            explicit Room();
            ~Room();
            void displayAll() override;
            void toDo() override;
        private:
            Player *_player;
            rtype::Button *_next;
            rtype::Button *_back;
            rtype::Button *_name;
            rtype::Button *_scrollbar;
            int _to_write;
            sf::Time _elapsedT;
            int pos_name;
            bool _side;
            std::string _games;
            std::vector<std::string> _token;
            std::vector<rtype::Button*> _buttons;
            int _choiceY;
            int _choiceM;
            unsigned int _caseM;
            bool _isMoving;
            int _intensity;
            int _intensity2;
            bool _hover;
            bool _hover2;
            bool _hover3;
            bool _setName;
            unsigned int _scroll;
            int _bar;
        };
    } // namespace scene
} // namespace rtype

#endif //RTYPE_2019_Room_HPP
