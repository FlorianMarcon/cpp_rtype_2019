/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2019
** File description:
** Scene.cpp
*/

#include "Scene.hpp"
#include "SceneNotFoundException.hpp"

std::map<std::string, rtype::scene::Scene *> rtype::scene::Scene::_sceneList = {};
rtype::scene::Scene *rtype::scene::Scene::_activeScene = nullptr;

rtype::scene::Scene::Scene(std::string name)
{
	_sceneList[name] = this;
	_name = name;
	_displayManager = rtype::Client::getInstance()->getDisplayManager();
	_width = rtype::Client::getInstance()->getDisplayManager()->getWidth();
	_height = rtype::Client::getInstance()->getDisplayManager()->getHeight();
	_serv = rtype::Client::getInstance()->getServer();
	_rcvData = rtype::Client::getInstance()->getReceiveData();
	_clock = rtype::Client::getInstance()->getClock();
	_clock->restart();
	sf::IntRect rect = this->getDisplayManager().setRectangle(0, 0, this->getDisplayManager().getWidth(), this->getDisplayManager().getHeight());
	// _background = new Sprite("../client/src/Font/planete.jpg", (sf::Vector2i){0, 0}, rect, 0);
	_mouse = new Sprite("../client/src/Font/cur.png", (sf::Vector2i){sf::Mouse::getPosition().x, sf::Mouse::getPosition().y}, rect, 0);
	_mouse->setScale((sf::Vector2f){0.1, 0.1});
	this->_par = new Button("../client/src/Font/parrr.png", (sf::Vector2i){0,0}, rect, (sf::Vector2f){1980, 1020}, Color(255, 255, 255, 255));
	this->_parr = new Button("../client/src/Font/parr.png", (sf::Vector2i){-400,0}, rect, (sf::Vector2f){1980, 1020}, Color(255, 255, 255, 255));
	this->_lune = new Button("../client/src/Font/plan.png", (sf::Vector2i){-900,350}, rect, (sf::Vector2f){720, 720}, Color(255, 255, 255, 255));
	this->_pp = new Button("../client/src/Font/pp.png", (sf::Vector2i){-200, 450}, rect, (sf::Vector2f){720, 720}, Color(255, 255, 255, 255));
	this->_red = new Button("../client/src/Font/red.png", (sf::Vector2i){-900, 800}, rect, (sf::Vector2f){720, 720}, Color(255, 255, 255, 255));
	this->_earth = new Button("../client/src/Font/earth.png", (sf::Vector2i){-200,450}, rect, (sf::Vector2f){500, 500}, Color(255, 255, 255, 255));
    this->_par->setScale((sf::Vector2f){2.3, 2.3});
    this->_parr->setScale((sf::Vector2f){2.3, 2.3});
    this->_earth->setScale((sf::Vector2f){2, 2});
    this->_lune->setScale((sf::Vector2f){0.6, 0.6});
    this->_pp->setScale((sf::Vector2f){0.5, 0.5});
    this->_red->setScale((sf::Vector2f){0.3, 0.3});
	this->_par->setPos(rtype::Client::getInstance()->getPos_par());
	this->_parr->setPos(rtype::Client::getInstance()->getPos_parr());
	this->_lune->setPos(rtype::Client::getInstance()->getPos_lune());
	this->_pp->setPos(rtype::Client::getInstance()->getPos_pp());
	this->_red->setPos(rtype::Client::getInstance()->getPos_red());
	this->_earth->setPos(rtype::Client::getInstance()->getPos_earth());

}

rtype::DisplayManager &rtype::scene::Scene::getDisplayManager() const
{
	return *this->_displayManager;
}

rtype::scene::Scene::~Scene()
{
}

void rtype::scene::Scene::displayAll()
{
	this->getDisplayManager().displayUpdate();
}

std::string rtype::scene::Scene::getName() const
{
	return this->_name;
}

void rtype::scene::Scene::drawGUI() {}

void rtype::scene::Scene::setActiveScene(rtype::scene::Scene &scene)
{
	_activeScene = &scene;
	std::cout << "Switching to scene \"" << _activeScene->getName() << "\"" << std::endl;
	_activeScene->drawGUI();
}

void rtype::scene::Scene::drawActiveScene()
{
	if (_activeScene)
		_activeScene->displayAll();
}

rtype::scene::Scene rtype::scene::Scene::getSceneWithName(std::string scene)
{
	return *_sceneList[scene];
}

rtype::scene::Scene *rtype::scene::Scene::getActiveScene()
{
	return _activeScene;
}

void rtype::scene::Scene::onQuit()
{
	rtype::Client::getInstance()->setPos_par(this->_par->getPos());
	rtype::Client::getInstance()->setPos_parr(this->_parr->getPos());
	rtype::Client::getInstance()->setPos_lune(this->_lune->getPos());
	rtype::Client::getInstance()->setPos_pp(this->_pp->getPos());
	rtype::Client::getInstance()->setPos_red(this->_red->getPos());
	rtype::Client::getInstance()->setPos_earth(this->_earth->getPos());
}

void rtype::scene::Scene::toDo(){}

void rtype::scene::Scene::onEvent() {
	if (_activeScene)
		_activeScene->toDo();
}

rtype::Player *rtype::scene::Scene::getPlayer()
{
	return this->_player;
}

void rtype::scene::Scene::setPlayer(rtype::Player *player)
{
	this->_player = player;
}

int rtype::scene::Scene::getWidth()
{
	return this->_width;
}

int rtype::scene::Scene::getHeight()
{
	return this->_height;
}

rtype::Sprite *rtype::scene::Scene::getSprite()
{
	return this->_background;
}

sf::Clock *rtype::scene::Scene::getClock()
{
	return this->_clock;
}

rtype::TCPClient * rtype::scene::Scene::getSserv()
{
	return this->_serv;
}

rtype::TCPClient::ReceiveData rtype::scene::Scene::getRcvData()
{
	return this->_rcvData;
}

rtype::GenericMessage rtype::scene::Scene::getResponse()
{
	return this->_response;
}

rtype::MetadataMessage rtype::scene::Scene::getResponseMeta()
{
	return this->_responseMeta;
}

rtype::IMessage::SerializedObject rtype::scene::Scene::getSerializedObject()
{
	return this->_serializedObject;
}

rtype::GenericMessage *rtype::scene::Scene::getRequest()
{
	return this->_request;
}

rtype::Button *rtype::scene::Scene::getPar(void)
{
    return this->_par;
}

rtype::Button *rtype::scene::Scene::getEarth(void)
{
return this->_earth;
}

rtype::Button *rtype::scene::Scene::getLune(void)
{
    return this->_lune;
}

rtype::Button *rtype::scene::Scene::getRed(void)
{
    return this->_red;
}

rtype::Button *rtype::scene::Scene::getPP(void)
{
    return this->_pp;
}