/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Game
*/

#ifndef RTYPE_2019_Game_HPP
#define RTYPE_2019_Game_HPP

#include "Scene.hpp"
#include <vector>
#include "../Items/Player.hpp"
#include "../Items/Vector2.hpp"
#include "../Items/Sprite.hpp"
#include "Game.hpp"
#include "../Items/Ennemy01.hpp"
#include "../Items/EMissile.hpp"

namespace rtype
{
    namespace scene
    {
        class Game : public rtype::scene::Scene
        {
        public:
            explicit Game();
            virtual ~Game();
			void parallax();
			void initPlayer();
            void initEnnemy();
            void updateEnnemy();
            void displayAll() override;
            void toDo() override;
            void moveManager();
            void releaseManager();
            void movePlayer();
        private:
            Player *_player;
            std::vector<Player *> _ennemy;
            std::vector<Player *> _friends;
            std::vector<Items*>   _missiles;
            rtype::Sprite *_item;
            int _witdh;
            int _height;
            bool _play;
            bool _startgame;
            rtype::Button *_next;
            bool _hover;
            std::string _seconds;
            std::string _step;
        };
    } // namespace scene
} // namespace rtype

#endif //RTYPE_2019_Game_HPP

