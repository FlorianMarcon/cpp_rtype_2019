/*
** EPITECH PROJECT, 2018
** RTYPE_2019
** File description:
** MainMenu.hpp
*/

#ifndef RTYPE_2019_MAINMENU_HPP
#define RTYPE_2019_MAINMENU_HPP

#include "Scene.hpp"
#include <vector>
#include "../Items/Player.hpp"

namespace rtype
{
    namespace scene
    {
        class MainMenu : public rtype::scene::Scene
        {
        public:
            explicit MainMenu();
            ~MainMenu();
            void displayAll() override;
            void toDo() override;
        private:
            Player *_player;
            rtype::Button *_name;
            int _to_write;
            bool _hover;
            sf::Time _elapsedT;
            int _posD;
            bool _endScene;
            int _intensity;
        };
    }
}

#endif //RTYPE_2019_MAINMENU_HPP
