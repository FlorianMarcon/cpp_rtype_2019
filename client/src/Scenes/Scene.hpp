/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Scene
*/

#ifndef ISCENE_HPP_
#define ISCENE_HPP_

#include <list>
#include <iostream>
#include "../Client/Client.hpp"
#include "../Items/Player.hpp"
#include "../Items/Sprite.hpp"
#include "../Items/Button.hpp"
#include "Network/TCPClient.hpp"
#include "Protocol/Protocol.hpp"
#include "Messages/GenericMessage.hpp"
#include "Messages/MetadataMessage.hpp"
#include "Protocol/ProtocolRtype.hpp"


namespace rtype
{
	namespace scene {

		class Scene {
		public:
				explicit Scene(std::string name);
				virtual ~Scene();


				/**
				 * @brief Get name 
				 * 
				 * @return void
				 */
				std::string getName() const;
				/**
				 * @brief display all
				 * 
				 * @return void
				 */
				virtual void displayAll();

				/**
				 * @brief Set active scene
				 * 
				 * @return void
				 */
				static void setActiveScene(Scene &scene);
				template <typename T>
				static void setActiveScene() {
					if (_activeScene)
						_activeScene->onQuit();
					/*Uncomment to have a flash between scenes
					rtype::Client::getInstance()->getDisplayManager()->displayUpdate();*/
					_activeScene = new T();
					std::cout << "Switching to scene \"" << _activeScene->getName() << "\"" << std::endl;
				}

				/**
				 * @brief Draw active scene
				 * 
				 * @return void
				 */
				static void drawActiveScene();

				/**
				 * @brief Get scene with name 
				 * 
				 * @return Scene
				 */
				static Scene getSceneWithName(std::string scene);

				/**
				 * @brief Get active scene
				 * 
				 * @return Scene
				 */
				static Scene *getActiveScene();

				/**
				 * @brief Draw GUI
				 * 
				 * @return void
				 */
				virtual void drawGUI();

				/**
				 * @brief On quit
				 * 
				 * @return void
				 */
				virtual void onQuit();

				/**
				 * @brief get display manager
				 * 
				 * @return rtype::DisplayManager
				 */
				rtype::DisplayManager &getDisplayManager() const;

				/**
				 * @brief On event
				 * 
				 * @return void
				 */
				static void onEvent();

				/**
				 * @brief to do 
				 * 
				 * @return void
				 */
				virtual void toDo();

				/**
				 * @brief Get player
				 * 
				 * @return Player
				 */
				virtual Player *getPlayer();

				/**
				 * @brief Set player
				 * 
				 * @return void
				 */
				virtual void setPlayer(Player *player);

				/**
				 * @brief Get width
				 * 
				 * @return int
				 */
				virtual int getWidth();

				/**
				 * @brief Get height
				 * 
				 * @return int
				 */
				virtual int getHeight();

				/**
				 * @brief Get sprite
				 * 
				 * @return rtype::Sprite
				 */
				virtual rtype::Sprite *getSprite();

				/**
				 * @brief Get clock
				 * 
				 * @return sf::Clock
				 */
				virtual sf::Clock *getClock();

				/**
				 * @brief Get server
				 * 
				 * @return rtyupe::TCPClient
				 */
				virtual rtype::TCPClient * getSserv();

				/**
				 * @brief Get received data
				 * 
				 * @return rtype::TCPClient::ReceiveData
				 */
				virtual rtype::TCPClient::ReceiveData getRcvData();

				/**
				 * @brief Get response
				 * 
				 * @return rtype::GenericMessage
				 */
				virtual rtype::GenericMessage getResponse();

				/**
				 * @brief Get response meta
				 * 
				 * @return rtype::MetadataMessage
				 */
				virtual rtype::MetadataMessage getResponseMeta();

				/**
				 * @brief Get serialized object
				 * 
				 * @return rtype::IMessage::SerializedObject
				 */
				virtual rtype::IMessage::SerializedObject getSerializedObject();

				/**
				 * @brief Get request
				 * 
				 * @return rtype::GenericMessage
				 */
        virtual rtype::GenericMessage *getRequest();

				/**
				 * @brief Get par
				 * 
				 * @return rtype::Button
				 */
				virtual rtype::Button *getPar(void);

				/**
				 * @brief Get earth
				 * 
				 * @return rtype::Button
				 */
				virtual rtype::Button *getEarth(void);

				/**
				 * @brief Get Lune
				 * 
				 * @return rtype::Button
				 */
				virtual rtype::Button *getLune(void);

				/**
				 * @brief Get red
				 * 
				 * @return rtype::Button
				 */
				virtual rtype::Button *getRed(void);

				/**
				 * @brief Get PP
				 * 
				 * @return rtype::Button
				 */
				virtual rtype::Button *getPP(void);

		protected:
				int _width;
				int _height;
				rtype::TCPClient *_serv;
				rtype::TCPClient::ReceiveData _rcvData;
        	    rtype::GenericMessage _response;
        	    rtype::GenericMessage *_request;
	    	    rtype::MetadataMessage _responseMeta;
    		    rtype::IMessage::SerializedObject _serializedObject;
	            rtype::Sprite *_mouse;
				sf::Clock *_clock;
				rtype::Button *_par;
				rtype::Button *_parr;
            	rtype::Button *_earth;
         		rtype::Button *_lune;
         	 	rtype::Button *_red;
         	    rtype::Button *_pp;
		private:
				rtype::DisplayManager *_displayManager;
				static Scene *_activeScene;
				static std::map<std::string, Scene *> _sceneList;
				std::string _name;
				sf::Event _event;
				Player *_player;
				bool _hover;
				sf::Time _elapsed;
	            rtype::Sprite *_background;
		};

	}
}

#endif /* !ISCENE_HPP_ */
