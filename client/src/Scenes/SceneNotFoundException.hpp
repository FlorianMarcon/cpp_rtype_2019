#include <utility>

/*
** EPITECH PROJECT, 2018
** RTYPE_2019
** File description:
** SceneNotFoundException.hpp
*/

#ifndef RTYPE_2019_SCENENOTFOUNDEXCEPTION_HPP
#define RTYPE_2019_SCENENOTFOUNDEXCEPTION_HPP

#include <exception>
#include <string>

struct SceneNotFoundException : public std::exception
{
    explicit SceneNotFoundException(std::string sceneName) : _sceneName(std::move(sceneName)) {}
    const char *what() const noexcept override {
        return std::string("Scene " + _sceneName + " not found ...").c_str();
    }
private:
    std::string _sceneName;
};

#endif //RTYPE_2019_SCENENOTFOUNDEXCEPTION_HPP
