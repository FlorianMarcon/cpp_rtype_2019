/*
** EPITECH PROJECT, 2018
** OOP_indie_studio_2018
** File description:
** Game.cpp
*/

#include <iostream>
#include "Game.hpp"
#include "../Client/Client.hpp"
#include <mutex>
#include "../Scenes/SelectPlayer.hpp"

std::mutex  infos;

#define LOGG std::cout << "File: " << __FILE__  << " Line: " << __LINE__ << " function: " << __FUNCTION__ << std::endl;

rtype::scene::Game::Game() : Scene(
                                                   "Select Game")
{
    sf::IntRect rect = this->getDisplayManager().setRectangle(0, 0, 1800, 720);
    // this->_item = new Items("../client/src/Font/sprites/background.jpg", (sf::Vector2f){0, 0}, rect, 0);
    this->_item = new Sprite("../client/src/Font/sprites/Background/Nebula Aqua-Pink.png", (sf::Vector2i){0, 0}, rect, 0);
	this->_next = new Button("", (sf::Vector2i){_width - _width/6, 800}, rect, (sf::Vector2f){260, 60}, Color(255, 255, 255, 150));
    this->_player =  rtype::scene::Scene::getSceneWithName("RTYPE").getPlayer()->getInstance();
    this->_witdh = this->getDisplayManager().getWidth();
	this->_height = this->getDisplayManager().getHeight();
    //std::cout << "Window width: " << _width << std::endl;
    //std::cout << "Window height: " << _height << std::endl;
    initPlayer();
    _play = false;
    _startgame = false;


    // std::cout << "_height = " << _height << "_width = " << _width << std::endl;
    // _par = rtype::scene::Scene::getPar();
    // _earth = rtype::scene::Scene::getEarth();
    // _lune = rtype::scene::Scene::getLune();
    // _red = rtype::scene::Scene::getRed();
    // _pp = rtype::scene::Scene::getPP();


    _step += "1";
    _friends.resize(0);
    _ennemy.resize(0);
    this->_player->setUdp(new rtype::UDP());
    _request = rtype::Protocol::generateGoInRoomRequest(this->_player->getNameGame(), this->_player->getNameRoom(), this->_player->getUdp()->getEndPoint().address().to_string(), this->_player->getUdp()->getEndPoint().port());
    _serializedObject = _request->serialize();

    _serv->sendData(_serializedObject.first, _serializedObject.second);
    delete _request;
    _rcvData = _serv->receiveData();
    _response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
    if (_response.getIdMessage() == Protocol::IDMessage::RESPONSE) {
	    _responseMeta.unserialize(_response.getBodyUnserialized());
        std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
	    if (status ==  std::to_string(rtype::OK)) {
    	    this->_player->setAddress(_responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::ADDRESS)));
            this->_player->setPort(std::stoi(_responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::PORT))));

            this->_player->getUdp()->addReceiver(rtype::BUDP::endpoint(boost::asio::ip::address::from_string(this->_player->getAddress()), (unsigned int)this->_player->getPort()));
            this->_player->getUdp()->setReceiveCallback([this](UDP &udp, const char *buffer, unsigned int size, rtype::UDPEndPoint &endpoint){

                GenericMessage message;
                MetadataMessage metadata;
                //LOGG;
                if (!_play)
                    _play = true;
                // LOGG;

                try
                {
                    std::cout << "Try unserialize generic" << std::endl;
                    message.unserialize(buffer, size);
                    // LOGG;
                    std::cout << "Try unserialize Metadata" << std::endl;
                    metadata.unserialize(message.getBodyUnserialized());
                    std::cout << "End unserialize Metadata" << std::endl;
                    // LOGG;
                
                }
                catch(const std::exception& e)
                {
                    std::cerr << e.what() << '\n';
                    return;
                }

                // infos.lock();
                std::cout << "ID Message is " << (int)message.getIdMessage() << std::endl;
                switch(message.getIdMessage()) {
                    case rtype::ProtocolRtype::IDPacket::STARTING_IN:
                        // this->metadata.unserialize(message.getBodyUnserialized());
                        _seconds = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::SECONDS));
                        // _step = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::STEP));
                        if (_seconds == "0")
                            _startgame = true;
                    break;

                    case rtype::ProtocolRtype::IDPacket::POSITIONS:
                    //std::cout << "POSITIONS" << std::endl;
                        // this->metadata.unserialize(message.getBodyUnserialized());
                        try
                        {
                            std::string name = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::ID));
                            int x = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::X)));
                            int y = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::Y)));
                            // //std::cout << name << std::endl;
                            // //std::cout << x << std::endl;
                            // //std::cout << y << std::endl;
                            if (this->_player->getName() == name) {
                                // sdt::cout << "ME" << std::endl;
                                std::cout << "setPosition for me" << std::endl;
                                _player->setPosition((sf::Vector2f){(float)x, (float)y});
                            } else {
                                std::vector<Player*>::iterator it = std::find_if(this->_friends.begin(), this->_friends.end(), [name](Player *player){
                                    return player->getName() == name;
                                });
                                if (it == this->_friends.end()) {
                                    Player *p = new Player();
                                    p->setName(name);
                                    p->setPosition((sf::Vector2f){(float) x, (float) y});
                                    infos.lock();
                                    this->_friends.push_back(p);
                                    infos.unlock();
                                } else {
                                    Player *p = *it;
                                    infos.lock();
                                    p->setPosition((sf::Vector2f){(float)x, (float)y});
                                    infos.unlock();
                                }
                                
                            }
                        }
                        catch(const std::exception& e)
                        {
                            std::cerr << e.what() << '\n';
                        }
                        
                    break;
                    
                    case rtype::ProtocolRtype::IDPacket::MISSILE:
                    //std::cout << "MISSILES" << std::endl;
                    //std::cout << "missiles nb = " << _missiles.size() << std::endl;
                        // this->metadata.unserialize(message.getBodyUnserialized());
                        try
                        {
                            std::string name = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::ID));
                            int x = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::X)));
                            int y = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::Y)));
                            // int vx = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::VX)));
                            // int vy = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::VY)));
                            // // int power = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::POWER)));
                            //std::cout << "name = " << name << std::endl;
                            //std::cout << "x = " << x << std::endl;
                            //std::cout << "y = " << y << std::endl;
                            // Missile *m = new Missile(name, (sf::Vector2i){x, y}, 2, sf::Vector2i{vx, vy});
                            // this->_player->addMissile(m);
                            // //std::cout << _player->getMissiles().size() << std::endl;
                            infos.lock();
                            std::vector<Items*>::iterator it = std::find_if(this->_missiles.begin(), this->_missiles.end(), [name](Items *missile){
                                return missile->getName() == name;
                            });
                            infos.unlock();

                            std::cout << "1" << std::endl;
                            if (it == this->_missiles.end()) {
                                std::cout << "it == this->_missiles.end()" << std::endl;

                                // if (vx < 0) {
                                    // EMissile *e = new EMissile(name, {x, y});
                                //     _missiles.push_back(new EMissile(name, {x, y}));
                                // } else {
                                    // Missile *e = new Missile(name, {x, y});
                                    if ( x > 33 && x <= 1700){
                                        Missile *m = new Missile(name, {x, y});
                                        if (m != nullptr) {
                                            infos.lock();
                                            _missiles.push_back(m);
                                            infos.unlock();
                                        }
                                    }
                                // }
                                
                                // this->_ennemy.push_back(e);
                            } else {
                                std::cout << "else" << std::endl;

                                Items *e = (Items *)*it;
                                if ( x <= 33 || x >= 1700){
                                    infos.lock();
                                    this->_missiles.erase(it);
                                    infos.unlock();
                                    delete e;
                                } else {
                                    infos.lock();
                                    e->setPosition((sf::Vector2i){x, y});
                                    infos.unlock();

                                }
                            }
                        }
                        catch(const std::exception& e)
                        {
                            std::cerr << e.what() << '\n';
                        }
                        
                    break; 

                    case rtype::ProtocolRtype::IDPacket::ENEMY_POSITIONS:
                    //std::cout << "ENNEMY POSITIONS" << std::endl;
                        // this->metadata.unserialize(message.getBodyUnserialized());
                        try
                        {
                            std::string name = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::ID));
                            std::string type = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::TYPE));
                            int x = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::X)));
                            int y = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::Y)));
                            
                            // //std::cout << name << std::endl;
                            // //std::cout << x << std::endl;
                            // //std::cout << y << std::endl;
        
                            std::vector<Player*>::iterator it = std::find_if(this->_ennemy.begin(), this->_ennemy.end(), [name](Player *player){
                                return player->getName() == name;
                            });
                            if (it == this->_ennemy.end()) {
                                std::cout << "create ennemy" << std::endl;
                                Ennemy01 *e = new Ennemy01(name, {x, y});
                                infos.lock();
                                _ennemy.push_back(e);
                                infos.unlock();
                                // this->_ennemy.push_back(e);
                            } else {
                                std::cout << "set missile pos" << std::endl;
                                Ennemy01 *e = (Ennemy01 *)*it;
                                //std::cout << "recup ennemy" << std::endl;
                                infos.lock();
                                e->setPosition((sf::Vector2f){(float)x, (float)y});
                                infos.unlock();
                                //std::cout << "set position" << std::endl;
                            }
                                
                        
                        }
                        catch(const std::exception& e)
                        {
                            std::cerr << e.what() << '\n';
                        }
                        
                    break;

                    case rtype::ProtocolRtype::IDPacket::DEATH:
                    //std::cout << "DEATH" << std::endl;
                        // this->metadata.unserialize(message.getBodyUnserialized());
                        try
                        {
                            std::string name = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::ID));
                            
                            //std::cout << name << std::endl;
                            // //std::cout << x << std::endl;
                            // //std::cout << y << std::endl;

                            std::vector<Player*>::iterator it2 = std::find_if(this->_ennemy.begin(), this->_ennemy.end(), [name](Player *player){
                                return player->getName() == name;
                            });
                            if (it2 == this->_ennemy.end()) {
                            } else {
                                //std::cout << "ennemy is dying" << std::endl;
                                Ennemy01 *e = (Ennemy01 *)*it2;
                                e->die();
                                infos.lock();
                                // e->~Ennemy01();
                                _ennemy.erase(it2);
                                infos.unlock();
                                delete e;
                            }
                            std::vector<Player*>::iterator it = std::find_if(this->_friends.begin(), this->_friends.end(), [name](Player *player){
                                return player->getName() == name;
                            });
                            if (it == this->_friends.end()) {
                            } else {
                                //std::cout << "friend is dying" << std::endl;
                                Player *e = (Player *)*it;
                                infos.lock();
                                e->die();
                                // e->~Player();
                                _friends.erase(it);
                                infos.unlock();
                                delete e;
                            }
                            std::vector<Items*>::iterator it3 = std::find_if(this->_missiles.begin(), this->_missiles.end(), [name](Items *missile){
                                return missile->getName() == name;
                            });
                            if (it3 == this->_missiles.end()) {
                            } else {
                                //std::cout << "missile is dying" << std::endl;
                                Items *e = (Items *)*it3;
                                infos.lock();
                                // e->~Items();
                                _missiles.erase(it3);
                                infos.unlock();
                                delete e;
                            }
                            if (name == _player->getName()) {
                                //std::cout << "player is dying" << std::endl;
                                infos.lock();
                                _player->die();
                                infos.unlock();
                                //_player->~Player();
                            
                            }
                                
                        
                        }
                        catch(const std::exception& e)
                        {
                            std::cerr << e.what() << '\n';
                        }
                        
                    break;
                    case rtype::ProtocolRtype::IDPacket::DISCONNECTION:
                        rtype::scene::Scene::setActiveScene<rtype::scene::SelectPlayer>();
                    break;
                    default:
                    //std::cout << "UNKNOWN" << std::endl;
                    break;

                }
                (void)udp;
                (void)endpoint;
                //LOGG;
                // infos.unlock();
                //std::cout << "End handle udp message" << std::endl;
            });
            this->_player->getUdp()->startReceive();
        }
    }
}

void    rtype::scene::Game::moveManager()
{
    //LOGG;
    // if (this->getClock() >= 100) {}
    static long int prev = 0;

    if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Up) {
        this->_player->setMove({this->_player->getMove().x, -1});
    } if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Down) {
        this->_player->setMove({this->_player->getMove().x, 1});
    } if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Right) {
        this->_player->setMove({1, this->_player->getMove().y});
    } if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Left) {
        this->_player->setMove({-1, this->_player->getMove().y});
    }

    if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Space) {
        if (this->_clock->getElapsedTime().asMilliseconds() > prev + 1000) {
            prev = this->_clock->getElapsedTime().asMilliseconds();
            if (this->_player->getPower() < 5)
                this->_player->setPower(this->_player->getPower() + 1);
        }
        this->_player->loadShot();
    }
}

void  rtype::scene::Game::releaseManager()
{
    //LOGG;
    if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Up) {
        this->_player->setMove({this->_player->getMove().x, 0});
    } if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Down) {
        this->_player->setMove({this->_player->getMove().x, 0});
    } if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Right) {
        this->_player->setMove({0, this->_player->getMove().y});
    } if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Left) {
        this->_player->setMove({0, this->_player->getMove().y});
    }
    //SHOT
    if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Space) {
		this->_player->shoot();
        // Items *m = this->_player->getMissiles()[_player->getMissiles().size() - 1];
        int x = _player->getPosition().x + 33;
        int y = _player->getPosition().y;
        std::cout << "power = " << _player->getPower() << std::endl;
        rtype::GenericMessage *message = rtype::ProtocolRtype::generatePacketMissile(0, this->_player->getPower(), x, y);
        if (message != nullptr) {
            rtype::IMessage::SerializedObject serialize = message->serialize();
            this->_player->getUdp()->asyncSend(serialize.first, serialize.second);
            delete message;
        }
        this->_player->setPower(1);
    }
}

void rtype::scene::Game::toDo(){
    //LOGG;
    if (this->_next->isHover(sf::Mouse::getPosition()))
        this->_hover = true;
    else
        this->_hover = false;

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && this->_next->isHover(sf::Mouse::getPosition())) {
        _request = rtype::Protocol::generateStartGameRequest(this->_player->getNameGame(), this->_player->getNameRoom());
        _serializedObject = _request->serialize();

        _serv->sendData(_serializedObject.first, _serializedObject.second);
        delete _request;
        _rcvData = _serv->receiveData();
        _response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
        if (_response.getIdMessage() == Protocol::IDMessage::RESPONSE) {
            _responseMeta.unserialize(_response.getBodyUnserialized());
            std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
            if (status ==  std::to_string(rtype::OK))
                _play = true;
        }
    }

	if (rtype::Client::getInstance()->getEvent().type == sf::Event::KeyPressed) {
		if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Escape)
			this->getDisplayManager().closeWindow();
        if (rtype::Client::getInstance()->getEvent().key.code == sf::Keyboard::Return && !_play) {
            _request = rtype::Protocol::generateStartGameRequest(this->_player->getNameGame(), this->_player->getNameRoom());
            _serializedObject = _request->serialize();

            _serv->sendData(_serializedObject.first, _serializedObject.second);
            delete _request;
            _rcvData = _serv->receiveData();
            _response.unserialize(std::get<0>(_rcvData), std::get<1>(_rcvData));
            if (_response.getIdMessage() == Protocol::IDMessage::RESPONSE) {
                _responseMeta.unserialize(_response.getBodyUnserialized());
                std::string status = _responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE));
                if (status ==  std::to_string(rtype::OK))
                    _play = true;
            }
        }
        this->moveManager();

    }

    if (rtype::Client::getInstance()->getEvent().type == sf::Event::KeyReleased) {
        this->releaseManager();
    }
    //LOGG;

}

void    rtype::scene::Game::initEnnemy()
{
}

void rtype::scene::Game::initPlayer()
{
    //LOGG;
    this->_player->setPlayerShip(rtype::Player::Ship::GREEN);
    this->_player->setPosition({100, 100});
    this->_player->setVitesse(5);
}

void rtype::scene::Game::updateEnnemy()
{
    // this->_ennemy->update();
    // this->_ennemy->getItem()->animate();
}

void rtype::scene::Game::parallax()
{
    // updateEnnemy();
    //this->_item->setVitesse(1);
    //this->getDisplayManager().drawSprite(this->_item);
    this->getDisplayManager().drawSprite(this->_player->getItem());
    //this->getDisplayManager().drawSprite(this->_ennemy->getItem());
    //this->getDisplayManager().drawSprite(this->_ennemy2->getItem());
}

void rtype::scene::Game::movePlayer()
{
    //LOGG;
    //ANIMATE
    static long int previous = 0;
    static long int prev2 = 0;
    static long int animE = 0;
    
    if (this->_clock->getElapsedTime().asMilliseconds() > previous + 10) {
        previous = this->_clock->getElapsedTime().asMilliseconds();
        this->_player->updatePos();
        //send player pos
        if (_player->getMove().x != 0 || _player->getMove().y != 0) {
            int x = this->_player->getPosition().x;
            int y = this->_player->getPosition().y;

            rtype::GenericMessage *message = rtype::ProtocolRtype::generatePacketPosition(this->_player->getName(), x, y);
            if (message != nullptr) {
                rtype::IMessage::SerializedObject serialize = message->serialize();
                this->_player->getUdp()->asyncSend(serialize.first, serialize.second);
                delete message;
            }
        }

        //SEND MISSILES POS
        // for (unsigned int i = 0; i < _player->getMissiles().size(); i++) {
        //     if (_player->getMissiles()[i]->getMove().x != 0 || _player->getMissiles()[i]->getMove().y != 0) {
        //         int x = this->_player->getMissiles()[i]->getPosition().x;
        //         int y = this->_player->getMissiles()[i]->getPosition().y;
        //         int power = 0;

        //         rtype::GenericMessage *message = rtype::ProtocolRtype::generateMissile(power, x, y, 1, 0);
        //         if (message != nullptr) {
        //             rtype::IMessage::SerializedObject serialize = message->serialize();
        //             this->_player->getUdp()->asyncSend(serialize.first, serialize.second);
        //             delete message;
        //         }
        //     }
        // }
    }

    //ANIM LOAD MISSILE
    if (this->_clock->getElapsedTime().asMilliseconds() > prev2 + 200) {
        prev2 = this->_clock->getElapsedTime().asMilliseconds();
        this->_player->animate();
    }

    //ANIM ENNEMY
    if (this->_clock->getElapsedTime().asMilliseconds() > animE + 200) { 
        animE = this->_clock->getElapsedTime().asMilliseconds();
        for (unsigned int i = 0; i < this->_ennemy.size(); i++) {
            //std::cout << "animatre ennemy" << std::endl;
            this->_ennemy[i]->animate();
        }
    }
    //LOGG;


}

void rtype::scene::Game::displayAll()
{
    if (this->_par->getPos().x <= -1840)
        this->_par->setPos((sf::Vector2i){0, 0});
    if (this->_parr->getPos().x <= -1840)
        this->_parr->setPos((sf::Vector2i){0, 0});
    // if (this->_lune->getPos().x <= -1000)
    //     this->_lune->setPos((sf::Vector2i){2000, 0});
    // if (this->_earth->getPos().x <= -1500)
    //     this->_earth->setPos((sf::Vector2i){2134, 0});
    // if (this->_red->getPos().x <= -670)
            // this->_red->setPos((sf::Vector2i){4896, 0});
    // if (this->_pp->getPos().x <= -760)
    //     this->_pp->setPos((sf::Vector2i){2345, 0});
    this->_par->setPos((sf::Vector2i){this->_par->getPos().x - 1, 0});
    this->_parr->setPos((sf::Vector2i){this->_parr->getPos().x - 2, 10});
    // this->_lune->setPos((sf::Vector2i){this->_lune->getPos().x - 4, -100});
    // this->_earth->setPos((sf::Vector2i){this->_earth->getPos().x - 1, 450});
    // this->_red->setPos((sf::Vector2i){this->_red->getPos().x - 8, 800});
    // this->_pp->setPos((sf::Vector2i){this->_pp->getPos().x - 6, 450});
    this->getDisplayManager().drawButton(this->_par);
    // this->getDisplayManager().drawButton(this->_parr);

    // this->getDisplayManager().drawButton(this->_lune);
    // this->getDisplayManager().drawButton(this->_pp);
    // this->getDisplayManager().drawButton(this->_red);
    // this->getDisplayManager().drawButton(this->_earth);
    if (_startgame) {
        //std::cout << 1 << std::endl;
        infos.lock();
        this->movePlayer();
        //DISPLAY MISSILE
        if (_missiles.size() > 0) {
            for (unsigned int i = 0; i < _missiles.size(); i++) {
                // //std::cout << "x = " << this->_player->getMissiles()[i]->getPos().x << "y = " << this->_player->getMissiles()[i]->getPos().y << std::endl;
                // infos.lock();    
                this->getDisplayManager().drawSprite(_missiles[i]);
                // infos.unlock();
            }
        }
        //DISPLAY FRIENDS
        if (this->_friends.size() > 0) {
            for (Player* p: this->_friends)
                // if (!p->isDead())
                    this->getDisplayManager().drawSprite(p->getItem());
                // else {
                //     this->getDisplayManager().drawSprite(p->getDie());
                // }
        }
        //DISPLAY ENNEMY
        if (this->_ennemy.size() > 0) {
            for (Player* p: this->_ennemy) {
                // if (!p->isDead())
                    this->getDisplayManager().drawSprite(p->getItem());
                // else {
                //     this->getDisplayManager().drawSprite(p->getDie());
                // }
            }
        }

        // std::vector<Player*>::iterator it = _friends.begin();
        // for (; it != _friends.end(); *it++) {
        //     Player *p = (Player *)*it;
        //     if (p->getDie()->getRect().left <= 470) {
        //         delete p;
        //         _friends.erase(it);
        //     }
        // }

        // std::vector<Player*>::iterator it2 = _ennemy.begin();
        // for (; it2 != _ennemy.end(); *it2++) {
        //     Player *p = (Player *)*it2;
        //     if (p->getDie()->getRect().left <= 470) {
        //         delete p;
        //         _ennemy.erase(it2);
        //     }
        // }
        

        //DISPLAY MY PLAYER
        if (!this->_player->isDead()) {
            this->getDisplayManager().drawSprite(this->_player->getItem());
            if (this->_player->isLoad() == true) {
                this->getDisplayManager().drawSprite(this->_player->getLoad());
            }
        } else {
            this->getDisplayManager().drawSprite(this->_player->getDie());
        }
        infos.unlock();
    }


    else if (!_play) {

        this->getDisplayManager().drawText(Vector2<int>(_width/2 - 150, 330), "Waiting for other players", Color(0, 255, 255, 255), 60);
        if(_hover)
            this->getDisplayManager().drawSquare(Vector2<int>(_width - _width/6,  800), Vector2<int>(260, 60), "", Color(255, 255, 255, 255));
        this->getDisplayManager().drawButton(_next);
        this->getDisplayManager().drawText(Vector2<int>(_width - _width/6 + 40, 850), "PLAY", Color(0, 0, 0), 35);
    	_mouse->setPos((sf::Vector2i){sf::Mouse::getPosition().x - 20, sf::Mouse::getPosition().y - 85});
        this->getDisplayManager().drawSprite(this->_mouse);
    } else if (_play) {
        this->getDisplayManager().drawText(Vector2<int>(_width /2 - 50, 330), "Round " + _step, Color(0, 255, 255, 255), 80);
        this->getDisplayManager().drawText(Vector2<int>(_width/2 + 60, 480), "Starting in " + _seconds, Color(0, 255, 255, 255), 60);
    }

    this->getDisplayManager().displayUpdate();
    //LOGG;
    infos.unlock();
}

rtype::scene::Game::~Game()
{
    delete _item;
    delete _next;
    delete _player;
}

