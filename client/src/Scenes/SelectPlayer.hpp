/*
** EPITECH PROJECT, 2018
** RTYPE_2019
** File description:
** SelectPlayer.hpp
*/

#ifndef RTYPE_2019_SelectPlayer_HPP
#define RTYPE_2019_SelectPlayer_HPP

#include "Scene.hpp"
#include <vector>
#include "../Items/Player.hpp"
#include "../Items/Vector2.hpp"
#include "Network/UDP.hpp"
#include "Game.hpp"

namespace rtype
{
    namespace scene
    {
        class SelectPlayer : public rtype::scene::Scene
        {
        public:
            explicit SelectPlayer();
            ~SelectPlayer();
            void displayAll() override;
            void toDo() override;
        private:
            Player *_player;
            rtype::Button *_next;
            rtype::Button *_back;
            bool _hover;
            bool _hover2;
            std::string _players;
            std::vector<std::string> _token;
            sf::Time _elapsedT;
            int _posD;
            bool _endScene;
            int _choiceY;
            rtype::Sprite *_background;
        };
    } // namespace scene
} // namespace rtype

#endif //RTYPE_2019_SelectPlayer_HPP
