/*
** EPITECH PROJECT, 2018
** RTYPE_2019
** File description:
** GameSelection.hpp
*/

#ifndef RTYPE_2019_GameSelection_HPP
#define RTYPE_2019_GameSelection_HPP

#include "Scene.hpp"
#include <vector>
#include "../Items/Player.hpp"
#include "../Items/Vector2.hpp"
#include "../Items/Items.hpp"

namespace rtype
{
    namespace scene
    {
        class GameSelection : public rtype::scene::Scene
        {
        public:
            explicit GameSelection();
            ~GameSelection();
            void displayAll() override;
            void toDo() override;
        private:
            Player *_player;
            sf::Time _elapsedT;
            int pos_name;
            bool _side;
            std::string _games;
            std::vector<std::string> _token;
            std::vector<rtype::Button*> _buttons;
            std::string _players;
            std::vector<std::string> _token2;
            int _choiceY;
            int _choiceM;
            unsigned int _caseM;
            bool _isMoving;
            int _intensity;
            int _intensity2;
        };
    } // namespace scene
} // namespace rtype

#endif //RTYPE_2019_GameSelection_HPP
