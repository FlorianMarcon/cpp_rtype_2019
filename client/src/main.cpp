/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** main.cpp
*/

#include <iostream>
#include <boost/program_options.hpp>
#include "Network/TCPClient.hpp"
#include "Client/Client.hpp"
#include "Client/Thread.hpp"


int	startCommunication(std::string address, unsigned int port)
{
	std::cout << "address: " << address << std::endl;
	std::cout << "port: " << port << std::endl;
	rtype::TCPClient serv(address, port);
	rtype::TCPClient::ReceiveData rcvData;


	// LoadingThread lt(cli);
	// lt.start();
	// std::cout << lt.getStatus() << std::endl;
	// std::cout << lt.getInfo() << std::endl;
	// std::cout << lt.active() << std::endl;

	// char a = 178;
	// std::string b;
	// int c = 1;
	// double d = 0;
	// do {
	// 	// std::cout << b;
	// 	if ((int)d % 2 == 0)
	// 		std::cout << "["<<d<<"%]" << std::endl;
	// 	std::this_thread::sleep_for(std::chrono::milliseconds((int)d));
	// 	// std::system("clear");
	// 	b+=a;
	// 	d+=7.69;
	// 	c++;
	// } while (c != 13);


	// serv.sendData("Hello world in serv", 22);
	// rcvData = serv.receiveData();

	// std::cout << "Size: " << std::get<1>(rcvData) << std::endl;
	// std::cout << "message: " << std::get<0>(rcvData) << std::endl;
	try
	{
		auto *cli = new rtype::Client(address, port);

		cli->loop();

		delete cli;
	}
	catch(const std::exception& e)
	{
		std::cerr << "Try --help or server is not activated" << '\n';
		return (84);
	}
	

	return (0);
}

int main(int ac, char **av)
{

	boost::program_options::options_description desc("Allowed options");
	boost::program_options::variables_map vm;

	desc.add_options()
    	("help", "Produce help message")
    	("address", boost::program_options::value<std::string>()->required(), "Server address")
    	("port", boost::program_options::value<unsigned int>()->required(),"Server port");

	boost::program_options::store(boost::program_options::parse_command_line(ac, av, desc), vm);

	if (vm.count("help")) {
		cout << desc << "\n";
		return 0;
	}
	boost::program_options::notify(vm);

	try
	{
		vm.notify();
		return startCommunication(vm["address"].as<std::string>(), vm["port"].as<unsigned int>());
	}
	catch(const std::exception& e)
	{
		std::cerr << "Use --help" << std::endl;
		return (84);
	}

	return (0);

}

