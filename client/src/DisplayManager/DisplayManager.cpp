/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** DisplayManager.cpp
*/

#include "DisplayManager.hpp"
#include <time.h>
#include <stdlib.h>
#include "../Items/Vector2.hpp"
#include "../Items/Color.hpp"

rtype::DisplayManager::DisplayManager(void)
{
	if (!_font.loadFromFile("../client/src/Font/ka1.ttf")) {
		closeWindow();
		exit(84);
	}
}

rtype::DisplayManager::~DisplayManager()
{
}

void rtype::DisplayManager::closeWindow(void)
{
	this->_window->close();
}

void rtype::DisplayManager::setFrameLimit(void)
{
	this->_window->setFramerateLimit(60);
	this->_window->setKeyRepeatEnabled(true);
	this->_window->setMouseCursorVisible(false);
}

bool rtype::DisplayManager::isWindowOpen(void)
{
	return (this->_window->isOpen());
}

void rtype::DisplayManager::displayUpdate(void)
{
		_window->display();
		_window->clear();
}

void rtype::DisplayManager::createWindow(Vector2<int>size, int _case)
{
	if (_case == 0) {
		this->_window->create(sf::VideoMode(size.getX(), size.getY()), "RTYPE");
		this->screen_width = size.getX();
		this->screen_height = size.getY();
	}
	else {
		this->_window->create(sf::VideoMode(sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height), "RTYPE");
		this->screen_width = sf::VideoMode::getDesktopMode().width;
		this->screen_height = sf::VideoMode::getDesktopMode().height;
	}
	glEnable(GL_TEXTURE_2D);
}

void rtype::DisplayManager::drawText(Vector2<int> position, std::string in_text, Color in_color, int size)
{
	_text.setString(in_text);
	sf::FloatRect textRect = _text.getLocalBounds();
	_text.setOrigin(textRect.left + textRect.width/2.0f,
               textRect.top  + textRect.height/2.0f);
	_text.setPosition(position.getX(), position.getY());
	_text.setFont(_font);
	_text.setCharacterSize(size);
	sf::Color color(in_color.r, in_color.g, in_color.b, in_color.a);
	_text.setFillColor(color);
	this->_window->draw(_text);
}

void rtype::DisplayManager::drawPixel(Vector2<int> position, Color color)
{
	(void)position;
	(void)color;
	return;
}

bool rtype::DisplayManager::loadTexture(std::string texName, std::string location)
{
	sf::Texture texture;

	if (!texture.loadFromFile(location))
		return (false);
	sf::Sprite sprite(texture);
	(void)texName;
	return (true);
}

void rtype::DisplayManager::drawSquare(Vector2<int> pos, Vector2<int> size, std::string texture, Color in_color)
{
	(void)texture;
	_rectangle.setPosition(sf::Vector2f(pos.getX(), pos.getY()));
	_rectangle.setPosition(sf::Vector2f(pos.getX() - size.getX()/2, pos.getY()));
	_rectangle.setSize(sf::Vector2f(size.getX(), size.getY()));
	sf::Color color(in_color.r, in_color.g, in_color.b, in_color.a);
	_rectangle.setFillColor(color);
	_window->draw(_rectangle);
}

void rtype::DisplayManager::updateInputs()
{
	return;
}

std::vector<int> rtype::DisplayManager::getInputs()
{
	std::vector<int> inputs;
	return (inputs);
}

void rtype::DisplayManager::setWindow(sf::RenderWindow *window)
{
	this->_window = window;
}

void rtype::DisplayManager::drawSprite(rtype::Sprite *item)
{
	item->getSprite().setPosition((sf::Vector2f)item->getPos());
	item->getSprite().setPosition(sf::Vector2f(item->getPos().x - item->getSize().x/2, item->getPos().y));
	item->getSprite().setTexture(item->getTexture());
	item->getSprite().setTextureRect(item->getRect());
	this->_window->draw(item->getSprite());
}

void rtype::DisplayManager::drawSprite(rtype::Button *item)
{
	item->getSprite().setPosition((sf::Vector2f)item->getPos());
	item->getSprite().setPosition(sf::Vector2f(item->getPos().x - item->getSize().x/2, item->getPos().y + item->getSize().y));
	item->getSprite().setTexture(item->getTexture());
	item->getSprite().setTextureRect(item->getRect());
	this->_window->draw(item->getSprite());
}

void rtype::DisplayManager::drawSprite(rtype::Items *item)
{
	// std::cout << item->getSprite().getPosition().y << std::endl;	
	item->getSprite().setPosition((sf::Vector2f)item->getPos());
	// std::cout << item->getSprite().getPosition().y << std::endl;	
	item->getSprite().setPosition(sf::Vector2f(item->getPos().x - item->getSize().x/2, item->getPos().y));
	item->getSprite().setTexture(item->getTexture());
	item->getSprite().setTextureRect(item->getRect());
	this->_window->draw(item->getSprite());
}

void rtype::DisplayManager::drawButton(rtype::Button *item)
{
	if (item->getPath().size() != 0)
		this->drawSprite(item);
	else
		this->drawSquare(Vector2<int>(item->getPos().x, item->getPos().y), Vector2<int>(item->getSize().x, item->getSize().y), item->getPath(), item->getColor());
}


sf::IntRect rtype::DisplayManager::setRectangle(int top, int left, int width, int height)
{
	sf::IntRect rect;

	rect.top = top;
	rect.left = left;
	rect.width = width;
	rect.height = height;
	return (rect);
}

int rtype::DisplayManager::getWidth(void)
{
	return this->screen_width;
}

int rtype::DisplayManager::getHeight(void)
{
	return this->screen_height;
}

