/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** DisplayManager
*/

#ifndef DISPLAYMANAGER_HPP_
#define DISPLAYMANAGER_HPP_

#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include "./IDisplayManager.hpp"
#include "../Items/Color.hpp"


namespace rtype
{
	class DisplayManager : public IDisplayManager {
		public:
			DisplayManager();
			~DisplayManager();
			/**
			 * @brief Create a Window object
			 * @param size
			 */
			void createWindow(Vector2<int> size, int _case) override;
			/**
			 * @brief Set the Frame Limit object
			 *
			 */
			void setFrameLimit(void) override;
			/**
			 * @brief Close a Window object
			 *
			 */
			void closeWindow(void) override;
			/**
			 * @brief Draw a Square object
			 *
			 * @param pos
			 * @param size
			 * @param texture
			 * @param color
			 */
			void drawSquare(Vector2<int> pos, Vector2<int> size, std::string texture = "", Color color = Color()) override;
			/**
			 * @brief Load a Texture
			 *
			 * @param texName
			 * @param location
			 * @return true
			 * @return false
			 */
			bool loadTexture(std::string texName, std::string location) override;
			/**
			 * @brief Draw a Text
			 *
			 * @param position
			 * @param text
			 * @param color
			 * @param size
			 */
			void drawText(Vector2<int> position, std::string text, Color color = Color(), int size = 10) override; //10 = PIXEL_HEIGHT
			/**
			 * @brief Draw a single Pixel
			 *
			 * @param position
			 * @param color
			 */
			void drawPixel(Vector2<int> position, Color color = Color()) override;
			/**
			 * @brief Refresh the Window
			 *
			 */
			void displayUpdate(void) override;
			/**
			 * @brief Get the Inputs object
			 *
			 * @return std::vector<int>
			 */
			std::vector<int> getInputs(void) override;

			/**
			 * @brief Update the list inputs
			 *
			 */
			void updateInputs(void) override;

			/**
			 * @brief Check if the Window is running
			 *
			 */
			bool isWindowOpen(void) override;

			/**
			 * @brief Set the Window object
			 *
			 * @param window
			 */
			void setWindow(sf::RenderWindow *window) override;
			/**
			 * @brief Draw sprite
			 *
			 * @param item
			 */
			void drawSprite(rtype::Sprite *item) override;
			/**
			 * @brief Draw Sprite button
			 *
			 * @param item
			 */
			void drawSprite(rtype::Button *item) override;
			/**
			 * @brief Draw Button
			 *
			 * @param item
			 */
			void drawSprite(rtype::Items *item) override;
			void drawButton(rtype::Button *item) override;
			/**
			 * @brief Set the Rectangle object
			 *
			 * @param top
			 * @param left
			 * @param width
			 * @param height
			 * @return sf::IntRect
			 */
			sf::IntRect setRectangle(int top, int left, int width, int height) override;
			/**
			 * @brief Get the Width object
			 *
			 * @return int
			 */
			int getWidth(void) override;
			/**
			 * @brief Get the Height object
			 *
			 * @return int
			 */
			int getHeight(void) override;
		protected:
		private:
			sf::RenderWindow *_window;
			sf::Event _event;
			sf::Font _font;
			sf::Text _text;
			int screen_width;
			int screen_height;
			sf::RectangleShape _rectangle;
	};
}

#endif /* !DISPLAYMANAGER_HPP_ */
