/*
** EPITECH PROJECT, 2019
** OOP_arcade_2018
** File description:
** IDisplayManager
*/

#ifndef IDISPLAYMANAGER_HPP_
#define IDISPLAYMANAGER_HPP_

#include <string>
#include <vector>
#include "../Items/Vector2.hpp"
#include "../Items/Color.hpp"
#include "../Items/Sprite.hpp"
#include "../Items/Button.hpp"


class IDisplayManager
{
  public:
	virtual void createWindow(Vector2<int> size, int _case) = 0;
	virtual void closeWindow(void) = 0;
	virtual void drawSquare(Vector2<int> pos, Vector2<int> size,
							std::string texture = "", Color color = Color()) = 0;
	virtual bool loadTexture(std::string texName, std::string location) = 0;
	virtual void drawText(Vector2<int> position, std::string text,
						  Color color = Color(), int size = 10) = 0; // PixelHeight = 10
	virtual void drawPixel(Vector2<int> position, Color color = Color()) = 0;
	virtual void displayUpdate(void) = 0;
	virtual bool isWindowOpen(void) = 0;
	virtual std::vector<int> getInputs(void) = 0;
	virtual void updateInputs(void) = 0;
	virtual void setFrameLimit(void) = 0;
	virtual void setWindow(sf::RenderWindow *window) = 0;
	virtual void drawSprite(rtype::Sprite *item) = 0;
	virtual void drawSprite(rtype::Items *item) = 0;
	virtual void drawSprite(rtype::Button *item) = 0;
	virtual sf::IntRect setRectangle(int top, int left, int width, int height) = 0;
	virtual int getWidth(void) = 0;
	virtual int getHeight(void) = 0;
	virtual void drawButton(rtype::Button *item) = 0;

};

#endif /* !IDISPLAYMANAGER_HPP_ */
