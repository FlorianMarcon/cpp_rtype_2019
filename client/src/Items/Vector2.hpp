/*
** EPITECH PROJECT, 2018
** CPP_rtype_2019
** File description:
** Vector2.hpp
*/

#ifndef CPP_RTYPE_2019_VECTOR2_HPP
#define CPP_RTYPE_2019_VECTOR2_HPP

template <typename T>
class Vector2
{
  public:
    Vector2()
    {
        this->x = 0;
        this->y = 0;
    }

    Vector2(T x, T y)
    {
        this->x = x;
        this->y = y;
    }

    T getX() const
    {
        return this->x;
    }

    T getY() const
    {
        return this->y;
    }

    void setX(T x)
    {
        this->x = x;
    }

    void setY(T y)
    {
        this->y = y;
    }

    bool operator==(const Vector2<T> &a)
    {
        return this->x == a.getX() && this->y == a.getY();
    }

    bool operator!=(const Vector2<T> &vec)
    {
        return !(*(this) == vec);
    }

    Vector2<T> operator+(const Vector2 &b)
    {
        return Vector2<T>(this->x + b.getX(), this->y, b.getY());
    }

    Vector2<T> operator-(const Vector2 &b)
    {
        return Vector2<T>(this->x - b.getX(), this->y - b.getY());
    }

  private:
    T x;
    T y;
};

#endif //CPP_RTYPE_2019_VECTOR2_HPP