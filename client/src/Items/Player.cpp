/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Player
*/

#include "Player.hpp"

rtype::Player *rtype::Player::_instance = nullptr;


rtype::Player::Player()
{
    this->_missiles.resize(0);
    this->_vitesse = 2;
    //this->_load = new Sprite()
    setPlayerShip(GREEN);
    _power = 1;
    this->_load = new Load((sf::Vector2i){0, 0}, 0);
    _explosion = new Explosion((sf::Vector2i){0, 0}, 0);
    this->_loadShot = false;
    _dead = false;
}

rtype::Player::Player(std::string name, sf::Vector2i pos)
{
    this->_missiles.resize(0);
    _name = name;
    _pos.x = pos.x;
    _pos.y = pos.y;
    this->_vitesse = 2;
    //this->_load = new Sprite()
    setPlayerShip(GREEN);

    this->_load = new Load((sf::Vector2i){0, 0}, 0);
    this->_loadShot = false;
    _explosion = new Explosion((sf::Vector2i)pos, 0);
    _dead = false;
}

rtype::Player::~Player()
{

}

void rtype::Player::setName(std::string name)
{
    this->_name = name;
}

std::string rtype::Player::getName()
{
    return this->_name;
}


void rtype::Player::setNameGame(std::string nameGame)
{
    this->_nameGame = nameGame;
}

std::string rtype::Player::getNameGame()
{
    return this->_nameGame;
}


void rtype::Player::setNameRoom(std::string nameRoom)
{
    this->_nameRoom = nameRoom;
}

std::string rtype::Player::getNameRoom()
{
    return this->_nameRoom;
}


void rtype::Player::setAddress(std::string address)
{
    this->_address = address;
}


std::string rtype::Player::getAddress(void)
{
    return this->_address;
}

void rtype::Player::setPort(int port)
{
    this->_port = port;
}

int rtype::Player::getPort(void) {
    return this->_port;
}



void rtype::Player::setUdp(rtype::UDP *udp)
{
    this->_udp = udp;
}

rtype::UDP * rtype::Player::getUdp(void)
{
    return this->_udp;
}


void rtype::Player::setSizeName(int sizeName)
{
    this->_sizeName = sizeName;
}

int rtype::Player::getSizeName(void)
{
    return this->_sizeName;
}

rtype::Player *rtype::Player::getInstance()
{
	return _instance;
}

void rtype::Player::setInstance(rtype::Player *instance)
{
	_instance = instance;
}

void    rtype::Player::setItem(rtype::Sprite *item)
{
    this->_item = item;
}

rtype::Items *rtype::Player::getItem()
{
    return this->_item;
}

void rtype::Player::setPlayerShip(int s)
{
    int width = 33;
    int height = 18;
    sf::IntRect rect;
	rect.top = s * height;
	rect.left = width * 2;
	rect.width = width;
	rect.height = height;

    auto item = new Sprite("../client/src/Font/sprites/r-typesheet42.gif", (sf::Vector2i){100, 100}, rect, 0);
    this->_item = item;
}


void rtype::Player::updatePos()
{
    //UPDATE SHIP POS
	this->_item->updatePosition();
    this->_pos.x = this->_item->getPos().x;
    this->_pos.y = this->_item->getPos().y;

    //UPDATE MISSILES POS
    // std::vector<Player*>::iterator it = this->_missiles.begin();
    // for (; it != this->_missiles.end(); it++) {
    //     if (!*it->updatePosition()) {
    //         this->_missiles[i]->~Items();
    //         _missiles.erase(it);
    //         //delete missile from vector
    //     }
    // }


    // for (unsigned int i = 0; i < this->_missiles.size(); i++) {
    //     if (!this->_missiles[i]->updatePosition()) {
    //         this->_missiles[i]->~Items();
    //         //delete missile from vector
    //     }
    // }

    sf::Vector2i pos;
    pos.x = this->_pos.x + 33;
    pos.y = this->_pos.y - 5;
    this->_load->setPosition(pos);
    // if (this->_loadShot == true)

}

void    rtype::Player::setMove(sf::Vector2f move)
{
    this->_move.x = move.x;
    this->_move.y = move.y;
    this->_item->setMove((sf::Vector2i)this->_move);
}

void    rtype::Player::setPosition(sf::Vector2f pos)
{
    // std::cout << "Player setPosition" << std::endl;
    // std::cout << "pos.x: " << pos.x << std::endl;
    // std::cout << "pos.y: " << pos.y << std::endl;
    // std::cout << "this->_pos.x: " << this->_pos.x <<std::endl;
    // std::cout << "this->_pos.y: " << this->_pos.y <<std::endl;
    this->_pos.x = pos.x;
    this->_pos.y = pos.y;
    if (this->_item != nullptr) {
        this->_item->setPosition((sf::Vector2i)this->_pos);
    }
    // std::cout << "Player End setPosition" << std::endl;
}

sf::Vector2f rtype::Player::getPosition()
{
    return this->_pos;
}

void rtype::Player::shoot()
{
    this->_loadShot = false;
    // float vitesse = 3;
    sf::Vector2i pos;
    pos.x = this->getPosition().x + 33;
    pos.y = this->getPosition().y;
    //this->_missiles.push_back(new Missile(pos, 3));
    //Sprite("../client/src/Font/sprites/r-typesheet42.gif", (sf::Vector2i)pos, {0, 0, 33, 18}, {1, 0}, vitesse));
    
}

void    rtype::Player::loadShot()
{
    this->_loadShot = true;
}

std::vector<rtype::Items*> rtype::Player::getMissiles()
{
    return this->_missiles;
}

void rtype::Player::setVitesse(float vitesse)
{
    this->_item->setVitesse(vitesse);
    this->_vitesse = vitesse;
}

float rtype::Player::getVitesse()
{
    return this->_vitesse;
}

sf::Vector2f rtype::Player::getMove(void)
{
    return this->_move;
}


void rtype::Player::animate()
{
    if (this->_loadShot == true && _dead == false)
        this->_load->animate();
    if (_dead)
        this->_explosion->animate();
    //this->_item->animate();
}

bool rtype::Player::isLoad()
{
    return this->_loadShot;
}

rtype::Items *rtype::Player::getLoad()
{
    return this->_load;
}

void 	rtype::Player::addMissile(Items *m)
{
    this->_missiles.push_back(m);
}

void    rtype::Player::die()
{
    _dead = true;
    _explosion->setPosition((sf::Vector2i)_pos);
}

bool    rtype::Player::isDead()
{
    return _dead;
}

rtype::Items    *rtype::Player::getDie()
{
    return _explosion;
}

int rtype::Player::getPower()
{
    return this->_power;
}

void    rtype::Player::setPower(int power)
{
    this->_power = power;
}
