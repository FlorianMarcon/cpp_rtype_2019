/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Items
*/

#ifndef Items_HPP_
#define Items_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "./Color.hpp"

namespace rtype
{
	class Items {
		public:
			explicit Items();
			virtual ~Items();

			/**
			 * @brief Get vitesse
			 * 
			 * @return float 
			 */
			virtual float getVitesse(void);

			/**
			 * @brief set vitesse
			 * 
			 * @return void 
			 */
			virtual void setVitesse(float vitesse);

			/**
			 * @brief Get sprite
			 * 
			 * @return sf::Sprite 
			 */
			virtual sf::Sprite getSprite(void);

			/**
			 * @brief Get texture
			 * 
			 * @return sf::Texture 
			 */
			virtual sf::Texture getTexture(void);

			/**
			 * @brief Get position
			 * 
			 * @return sf::Vector2i
			 */
			virtual sf::Vector2i getPos(void);

			/**
			 * @brief Set position
			 * 
			 * @return void 
			 */
			virtual void setPos(sf::Vector2i);

			/**
			 * @brief Get size
			 * 
			 * @return sf::Vector2i 
			 */
			virtual sf::Vector2f getSize(void);

			/**
			 * @brief Set size
			 * 
			 * @return void 
			 */
			virtual void setSize(sf::Vector2f size);

			/**
			 * @brief Get rect
			 * 
			 * @return sf::IntRect 
			 */
			virtual sf::IntRect getRect(void);

			/**
			 * @brief Set rect
			 * 
			 * @return void
			 */
			virtual void setRect(sf::IntRect);

			/**
			 * @brief Button is hover ?
			 * 
			 * @return bool 
			 */
			virtual bool isHover(sf::Vector2i pos);

			/**
			 * @brief Get path
			 * 
			 * @return std::string 
			 */
			virtual std::string getPath(void);

			/**
			 * @brief Get color
			 * 
			 * @return Color 
			 */
			virtual Color getColor(void);

			/**
			 * @brief anim current sprite
			 * 
			 * @return void
			 */
			virtual void animate();

			/**
			 * @brief SetMove
			 * 
			 * @return void 
			 */
			virtual void setMove(sf::Vector2i);

			/**
			 * @brief set position
			 * 
			 * @return void
			 */
			virtual void setPosition(sf::Vector2i);

			/**
			 * @brief set scale
			 * 
			 * @return void 
			 */
			virtual void setScale(sf::Vector2f scale);

			/**
			 * @brief Update position
			 * 
			 * @return bool
			 */
			virtual bool updatePosition();

			/**
			 * @brief Set name
			 * 
			 * @return void
			 */
			virtual void setName(std::string);

			/**
			 * @brief Get name
			 * 
			 * @return std::string 
			 */
			virtual std::string getName();
		protected:
		private:
			std::string _name;
			sf::Sprite _sprite;
			sf::Texture _texture;
			sf::Vector2i _pos;
			sf::Vector2i _move;
			sf::IntRect _rect;
			sf::Image _image;
			sf::Vector2f _size;
			Color _color;
			std::string _path;
			float _vitesse;
	};
}
#endif /* !Items_HPP_ */


