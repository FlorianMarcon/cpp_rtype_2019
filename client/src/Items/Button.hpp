/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Button
*/

#ifndef Button_HPP_
#define Button_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "../Items/Items.hpp"


namespace rtype
{
	class Button : public rtype::Items {
		public:
			explicit Button(std::string path, sf::Vector2i pos, sf::IntRect rect, sf::Vector2f size, Color color);
			~Button();
			sf::Sprite getSprite(void) override;
			sf::Texture getTexture(void) override;
			sf::Vector2i getPos(void) override;
			void setPos(sf::Vector2i) override;
			sf::Vector2f getSize(void) override;
			void setSize(sf::Vector2f size) override;
			sf::IntRect getRect() override;
            std::string getPath(void) override;
            Color getColor(void) override;
            bool isHover(sf::Vector2i pos) override;
			void animate() override;
			void setScale(sf::Vector2f scale) override;
			void setPosition(sf::Vector2i) override;
		protected:
		private:
			sf::Sprite _sprite;
			sf::Texture _texture;
			sf::Vector2i _pos;
			sf::Vector2f _size;
			sf::IntRect _rect;
			sf::Image _image;
            Color _color;
			std::string _path;
	};
}
#endif /* !Button_HPP_ */
