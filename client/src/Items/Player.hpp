/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Player
*/

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <iostream>
#include "Items.hpp"
#include "Sprite.hpp"
#include "Network/UDP.hpp"
#include <vector>
#include "Missile.hpp"
#include "Load.hpp"
#include "Explosion.hpp"

namespace rtype {
	class Player {

		public:

			enum Ship{
				BLUE_SKY = 0,
				PURPLE = 1,
				GREEN = 2,
				RED = 3,
				BLUE = 4
			};

		
			Player();
			Player(std::string, sf::Vector2i);
			virtual ~Player();
			//Player(color);

			/**
			 * @brief Set name 
			 * 
			 * @return void
			 */
			void setName(std::string name);

			/**
			 * @brief Set name game
			 * 
			 * @return void
			 */
			void setNameGame(std::string nameGame);

			/**
			 * @brief Set name room
			 * 
			 * @return void
			 */
			void setNameRoom(std::string nameRoom);

			/**
			 * @brief Set address
			 * 
			 * @return void
			 */
			void setAddress(std::string address);

			/**
			 * @brief Set port 
			 * 
			 * @return void
			 */
			void setPort(int port);


			/**
			 * @brief Set udp 
			 * 
			 * @return void
			 */
			void setUdp(rtype::UDP *udp);


			/**
			 * @brief Get name 
			 * 
			 * @return std::string
			 */
			std::string getName(void);

			/**
			 * @brief Get name game
			 * 
			 * @return std::string
			 */
			std::string getNameGame(void);

			/**
			 * @brief Get name room
			 * 
			 * @return std::string
			 */
			std::string getNameRoom(void);


			/**
			 * @brief Get address
			 * 
			 * @return std::string
			 */
			std::string getAddress(void);

			/**
			 * @brief Get port 
			 * 
			 * @return int
			 */
			int getPort(void);

			/**
			 * @brief Get udp
			 * 
			 * @return rtype::UDP
			 */
			rtype::UDP * getUdp(void);

			/**
			 * @brief Set size name
			 * 
			 * @return void
			 */
			void setSizeName(int sizeName);

			/**
			 * @brief Get size name 
			 * 
			 * @return int 
			 */
			int getSizeName(void);

			/**
			 * @brief Get instance
			 * 
			 * @return Player
			 */
			static Player *getInstance();

			/**
			 * @brief Set instance 
			 * 
			 * @return void 
			 */
			static void setInstance(Player *instance);
			Player(const Player &P)
			{
				_nameRoom = P._nameRoom;
				_nameGame = P._nameGame;
				_name = P._name;
				_sizeName = P._sizeName;
				_instance = P._instance;
			}

			/**
			 * @brief Set item 
			 * 
			 * @return void
			 */
			void setItem(rtype::Sprite*);

			/**
			 * @brief Get item
			 * 
			 * @return rtype::Items
			 */
			rtype::Items *getItem();

			/**
			 * @brief Set mplayer ship 
			 * 
			 * @return void
			 */
			void setPlayerShip(int);


			/**
			 * @brief add missile
			 * 
			 * @return void
			 */		
			void 	addMissile(Items *m);

			/**
			 * @brief Update position
			 * 
			 * @return void
			 */
			void updatePos();

			/**
			 * @brief Animate current sprite  
			 * 
			 * @return void
			 */
			virtual void animate();

			/**
			 * @brief Shoot 
			 * 
			 * @return void
			 */
			void shoot();

			/**
			 * @brief Load shoot 
			 * 
			 * @return void
			 */
			void loadShot();

			/**
			 * @brief Die 
			 * 
			 * @return void
			 */
			void die();

			/**
			 * @brief Set move 
			 * 
			 * @return void
			 */
			void setMove(sf::Vector2f);

			/**
			 * @brief Get move 
			 * 
			 * @return sf::Vector2f
			 */
			sf::Vector2f getMove(void);

			/**
			 * @brief Set position  
			 * 
			 * @return void
			 */
			void setPosition(sf::Vector2f);

			/**
			 * @brief Get position 
			 * 
			 * @return sf::vector2f
			 */
			sf::Vector2f getPosition();

			/**
			 * @brief Get missiles
			 * 
			 * @return std::vector<rtype::Items*>
			 */
			std::vector<rtype::Items*> getMissiles();

			/**
			 * @brief Set vitesse
			 * 
			 * @return void
			 */
			void setVitesse(float);

			/**
			 * @brief Get vitesse 
			 * 
			 * @return float
			 */
			float getVitesse();

			/**
			 * @brief Is load ?  
			 * 
			 * @return bool
			 */
			bool isLoad();

			/**
			 * @brief Get load 
			 * 
			 * @return rtype::Items
			 */
			rtype::Items *getLoad();

			/**
			 * @brief isDead ? 
			 * 
			 * @return bool
			 */
			bool isDead();

			/**
			 * @brief get die 
			 * 
			 * @return rtype::Items
			 */
			rtype::Items *getDie();

			/**
			 * @brief Get power 
			 * 
			 * @return int
			 */
			int getPower();

			/**
			 * @brief Set power 
			 * 
			 * @return void
			 */
			void setPower(int);

			/**
			 * @brief Get point
			 * 
			 * @return int
			 */
			int getPoint();

			/**
			 * @brief Set point 
			 * 
			 * @return void
			 */
			void setPoint(int);



		protected:
            rtype::UDP *	_udp;
			float			_vitesse; 
			sf::Vector2f	_move;
			sf::Vector2f	_pos;
			std::string 	_name;
			std::string 	_nameGame;
			std::string 	_nameRoom;
			std::string 	_address;
			int 			_port;
			int 			_sizeName;
			static Player	*_instance;
			rtype::Items	*_item;
			sf::IntRect		_rect;
			std::vector<Items*> _missiles;
			rtype::Items   *_load;
			bool			_loadShot;
			rtype::Items	*_explosion;
			bool 			_dead;
			int				_power;
	};
}

#endif /* !PLAYER_HPP_ */