/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Explosion
*/

#include "Explosion.hpp"
#include <iostream>


rtype::Explosion::Explosion(sf::Vector2i pos, float vitesse)
{
	this->_vitesse = vitesse;
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;

	this->_rect_s.top = 295;
	this->_rect_s.left = 265;
	this->_rect_s.width = 22;
	this->_rect_s.height = 33;

    // this->_move = {1, 0};

	this->_sprite.setPosition((sf::Vector2f)pos);
	if (!(_image.loadFromFile("../client/src/Font/sprites/r-typesheet1.gif")))
		std::cout << "Cannot Explosion image" << std::endl;
	else {
		this->_texture.loadFromImage(this->_image);
		this->_sprite.setTexture(this->_texture);
		this->_sprite.setTextureRect(this->_rect_s);
	}
}


rtype::Explosion::~Explosion()
{
	this->_texture.~Texture();
}


float rtype::Explosion::getVitesse(void)
{
	return this->_vitesse;
}

void rtype::Explosion::setVitesse(float vitesse)
{
	this->_vitesse = vitesse;
}

sf::Sprite rtype::Explosion::getSprite(void)
{
 return this->_sprite;
}

sf::Texture rtype::Explosion::getTexture(void)
{
 return this->_texture;
}

sf::Vector2i rtype::Explosion::getPos(void)
{
 return this->_pos;
}

sf::IntRect rtype::Explosion::getRect()
{
    // std::cout << "left = " << this->_rect.left << " top = " << _rect.top << " h = " << _rect.height << " w " << _rect.width << std::endl;
	return this->_rect_s;
}

void 	rtype::Explosion::animate()
{
    static int i = 0;

	if (this->_rect_s.left < 470) {
        if (i == 0) {
            this->_rect_s.left += 22;
            this->_rect_s.width = 28;
        } else if (i == 1) {
            this->_rect_s.left += 30;
            this->_rect_s.width = 33;
        } else if (i == 2) {
            this->_rect_s.left += 33;
            this->_rect_s.width = 38;
        } else
            this->_rect_s.left += 38;
        i += 1;
        this->_sprite.setTextureRect(this->_rect_s);
    }
}


void rtype::Explosion::setMove(sf::Vector2i move)
{
	this->_move.x = move.x;
	this->_move.y = move.y;
}


void rtype::Explosion::setPosition(sf::Vector2i pos)
{
	this->_sprite.setPosition((sf::Vector2f)pos);
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
}

// void rtype::Explosion::updatePosition()
// {
// 	if (this->_pos.x + this->_move.x > 0 && this->_pos.x + this->_move.x < 1600) {
// 		this->_pos.x += this->_move.x * this->_vitesse;
// 	}
// 	if (this->_pos.y + this->_move.y > 0 && this->_pos.y + this->_move.y < 900) {
// 		this->_pos.y += this->_move.y * this->_vitesse;
// 	}
// 	this->_sprite.setPosition((sf::Vector2f)this->_pos);
// }