/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Ennemy01
*/

#include "Ennemy01.hpp"

rtype::Ennemy01 *rtype::Ennemy01::_instance = nullptr;


rtype::Ennemy01::Ennemy01()
{
    this->_missiles.resize(0);
    this->_vitesse = 2;
	this->_rect.top = 0;
	this->_rect.left = 33 * 8;
	this->_rect.width = 33;
	this->_rect.height = 33;

    // this->_item = new Sprite("../client/src/Font/sprites/r-typesheet5.gif", (sf::Vector2i){100, 100}, _rect, 0);
    this->_item = new RedShip((sf::Vector2i){100, 100}, 0);
    this->_load = new Load((sf::Vector2i){0, 0}, 0);
    this->_explosion = new Explosion((sf::Vector2i){0, 0}, 0);
    this->_loadShot = false;
}

rtype::Ennemy01::Ennemy01(std::string name, sf::Vector2i pos)
{
    this->_missiles.resize(0);
    _name = name;
    _pos.x = pos.x;
	_pos.y = pos.y;
    this->_vitesse = 2;
	this->_rect.top = 0;
	this->_rect.left = 33 * 8;
	this->_rect.width = 33;
	this->_rect.height = 33;

	// this->_item = new Sprite("../client/src/Font/sprites/r-typesheet5.gif", pos, _rect, 0);
    this->_item = new RedShip((sf::Vector2i){100, 100}, 0);
    this->_load = new Load((sf::Vector2i){0, 0}, 0);
    this->_explosion = new Explosion((sf::Vector2i)pos, 0);
    this->_loadShot = false;
}

rtype::Ennemy01::~Ennemy01()
{
	this->_load->~Items();
	this->_item->~Items();
}


void rtype::Ennemy01::animate()
{
    
    if (_dead)
        this->_explosion->animate();
    else {
        if (this->_loadShot) {
            this->_load->animate();
        }
        this->_item->animate();
    }
}



// void    rtype::Ennemy01::setPosition(sf::Vector2f pos)
// {
//     std::cout << "hello" << std::endl;
//     this->_pos.x = pos.x;
//     this->_pos.y = pos.y;
//     std::cout << "1" << std::endl;
//     if (this->_item != nullptr) {
//         std::cout << "2" << std::endl;
//         this->_item->setPosition((sf::Vector2i)this->_pos);
//         std::cout << "hell3o" << std::endl;
//     }
// }