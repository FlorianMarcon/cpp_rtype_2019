/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Iplayer
*/

#ifndef IPLAYER_HPP_
#define IPLAYER_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include "Vector2.hpp"

class Iplayer {

    enum Type {
        PLAYER,
        ENNEMY,
    };

    enum Move {
        UP,
        DOWN,
        RIGHT,
        LEFT
    };

	public:
		Iplayer();
		~Iplayer();

        virtual void shoot() = 0;
        virtual void movePlayer(Move) = 0;


	protected:
	private:
        int             _life;
        int             _power;
        int             _defence;
        Vector2<int>    _pos;
        Vector2<int>    _move;
        Type            _type;
        sf::Sprite      _sprite;


};

#endif /* !IPLAYER_HPP_ */
