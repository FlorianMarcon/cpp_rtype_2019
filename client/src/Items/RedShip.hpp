/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** RedShip
*/

#ifndef RedShip_HPP_
#define RedShip_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "../Items/Items.hpp"


namespace rtype
{
	class RedShip : public rtype::Items {
		public:
			RedShip(sf::Vector2i pos, float vitesse = 2);
			// explicit RedShip(std::string path, sf::Vector2i pos, sf::IntRect rect, float vitesse = 0);
			// explicit RedShip(std::string path, sf::Vector2i pos, sf::IntRect rect, sf::Vector2f move, float vitesse = 0);
			RedShip();
			~RedShip();
			float getVitesse(void) override;
			void setVitesse(float vitesse) override;
			sf::Sprite getSprite(void) override;
			sf::Texture getTexture(void) override;
			sf::Vector2i getPos(void) override;
			sf::IntRect getRect() override;
			void animate() override;
			void setPosition(sf::Vector2i) override;
			void setMove(sf::Vector2i) override;
			// void updatePosition() override;
			
		protected:
		private:
			int 		_life;

			sf::Sprite _sprite;
			sf::Texture _texture;
			sf::Vector2i _pos;
			sf::Vector2f _move;
			sf::IntRect _rect;
			sf::IntRect _rect_s;
			sf::Image _image;
			float _vitesse;
	};
}
#endif /* !RedShip_HPP_ */
