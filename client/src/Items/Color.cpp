/*
** EPITECH PROJECT, 2018
** CPP_rtype_2019
** File description:
** Color.cpp
*/

#include "./Color.hpp"

Color::Color(ushort r, ushort g, ushort b, ushort a)
{
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
}