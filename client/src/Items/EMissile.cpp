/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** EMissile
*/

#include "EMissile.hpp"
#include <iostream>


rtype::EMissile::EMissile(std::string name, sf::Vector2i pos, float vitesse, sf::Vector2i move)
{

	this->_name = name;
	this->_vitesse = vitesse;
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;

	this->_rect.top = 120;
	this->_rect.left = 8 * 33;
	this->_rect.width = 33;
	this->_rect.height = 18;

    this->_move.x = move.x;
    this->_move.y = move.y;

	this->_sprite.setPosition((sf::Vector2f)pos);
	if (!(_image.loadFromFile("../client/src/Font/sprites/r-typesheet1.gif")))
		std::cout << "Cannot load image" << std::endl;
	else {
		this->_texture.loadFromImage(this->_image, this->_rect);
		this->_sprite.setTexture(this->_texture);
	}
}


rtype::EMissile::~EMissile()
{
	this->_texture.~Texture();
}


float rtype::EMissile::getVitesse(void)
{
	return this->_vitesse;
}

void rtype::EMissile::setVitesse(float vitesse)
{
	this->_vitesse = vitesse;
}

sf::Sprite rtype::EMissile::getSprite(void)
{
 return this->_sprite;
}

sf::Texture rtype::EMissile::getTexture(void)
{
 return this->_texture;
}

sf::Vector2i rtype::EMissile::getPos(void)
{
 return this->_pos;
}

sf::IntRect rtype::EMissile::getRect()
{
	return this->_rect_s;
}

void 	rtype::EMissile::animate()
{
	// (void)clock;
	// static int s = 0;
	
	// if (this->_pos.x + this->_move.x > 0 && this->_pos.x + this->_move.x < 1600) {
	// 	this->_pos.x += this->_move.x * this->_vitesse;
	// }
	// if (this->_pos.y + this->_move.y > 0 && this->_pos.y + this->_move.y < 900) {
	// 	this->_pos.y += this->_move.y * this->_vitesse;
	// }
	// this->_sprite.setPosition((sf::Vector2f)this->_pos);

}


void rtype::EMissile::setMove(sf::Vector2i move)
{
	this->_move.x = move.x;
	this->_move.y = move.y;
}


void rtype::EMissile::setPosition(sf::Vector2i pos)
{
	this->_sprite.setPosition((sf::Vector2f)pos);
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
}

// void rtype::EMissile::updatePosition()
// {
// 	if (this->_pos.x + this->_move.x > 0 && this->_pos.x + this->_move.x < 1900) {
// 		this->_pos.x += this->_move.x * this->_vitesse;
// 		this->_sprite.setPosition((sf::Vector2f)this->_pos);
// 	} else {
// 		this->~EMissile();
// 	}


// 	if (this->_pos.y + this->_move.y > 0 && this->_pos.y + this->_move.y < 1400) {
// 		this->_pos.y += this->_move.y * this->_vitesse;
// 		this->_sprite.setPosition((sf::Vector2f)this->_pos);
// 	} else {
// 		this->~EMissile();
// 	}
	
// }