/*
** EPITECH PROJECT, 2018
** CPP_rtype_2019
** File description:
** Color.hpp
*/

#ifndef CPP_RTYPE_2019_COLOR_HPP
#define CPP_RTYPE_2019_COLOR_HPP

using ushort = unsigned short;

class Color
{
  public:
    explicit Color(ushort r = 255, ushort g = 255, ushort b = 255, ushort a = 255);
    ushort r;
    ushort g;
    ushort b;
    ushort a;
};

#endif //CPP_RTYPE_2019_COLOR_HPP