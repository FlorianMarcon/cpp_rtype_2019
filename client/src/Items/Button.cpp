/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Button
*/

#include "Button.hpp"
#include <iostream>

rtype::Button::Button(std::string path, sf::Vector2i pos, sf::IntRect rect, sf::Vector2f size, Color color) : Items()
{
    (void)color;
	this->_rect.top = rect.top;
	this->_rect.left = rect.left;
	this->_rect.width = rect.width;
	this->_rect.height = rect.height;
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
	this->_sprite.setPosition((sf::Vector2f)pos);
    if (path.size() != 0) {
        if (!(_image.loadFromFile(path)))
            std::cout << "Cannot load image" << std::endl;
        else {
            this->_texture.loadFromImage(this->_image);
            this->_sprite.setTexture(this->_texture);
        }
    }
    this->_size.x = size.x;
    this->_size.y = size.y;
    this->_color = color;
    this->_path = path;
}

rtype::Button::~Button()
{
	this->_texture.~Texture();
}

void rtype::Button::setScale(sf::Vector2f scale)
{
    this->_sprite.setScale(scale);
}

sf::Sprite rtype::Button::getSprite(void)
{
 return this->_sprite;
}

sf::Texture rtype::Button::getTexture(void)
{
 return this->_texture;
}

sf::Vector2i rtype::Button::getPos(void)
{
 return this->_pos;
}

sf::IntRect rtype::Button::getRect()
{
	return this->_rect;
}

bool rtype::Button::isHover(sf::Vector2i pos)
{
	if (pos.x > _pos.x - _size.x/2 && pos.x < _pos.x + _size.x/2 && pos.y > this->_pos.y + _size.y && pos.y < this->_pos.y + 2 *this->_size.y)
        return (true);
    return (false);
}

void rtype::Button::setPos(sf::Vector2i pos)
{
    this->_pos.x = pos.x;
    this->_pos.y = pos.y;
	this->_sprite.setPosition((sf::Vector2f){(float)pos.x, (float)pos.y});
}

void rtype::Button::setPosition(sf::Vector2i pos)
{
    this->_pos.x = pos.x;
    this->_pos.y = pos.y;
	this->_sprite.setPosition((sf::Vector2f){(float)pos.x, (float)pos.y});
	this->_sprite.setPosition((sf::Vector2f){(float)pos.x/2, (float)pos.y + this->_size.y/2});
}

sf::Vector2f rtype::Button::getSize(void)
{
	return this->_size;
}

void rtype::Button::setSize(sf::Vector2f size)
{
	this->_size.x = size.x;
	this->_size.y = size.y;
}

std::string rtype::Button::getPath(void)
{
	return this->_path;
}

Color rtype::Button::getColor(void)
{
	return this->_color;
}

void rtype::Button::animate()
{

}