/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** EMissile
*/

#ifndef EMissile_HPP_
#define EMissile_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "../Items/Items.hpp"


namespace rtype
{
	class EMissile : public rtype::Items {
		public:
			explicit EMissile(std::string name, sf::Vector2i pos, float vitesse = 2, sf::Vector2i move = {-1, 0});
			// explicit EMissile(std::string path, sf::Vector2i pos, sf::IntRect rect, float vitesse = 0);
			// explicit EMissile(std::string path, sf::Vector2i pos, sf::IntRect rect, sf::Vector2f move, float vitesse = 0);
			EMissile();
			~EMissile();
			float getVitesse(void) override;
			void setVitesse(float vitesse) override;
			sf::Sprite getSprite(void) override;
			sf::Texture getTexture(void) override;
			sf::Vector2i getPos(void) override;
			sf::IntRect getRect() override;
			void animate() override;
			void setPosition(sf::Vector2i) override;
			void setMove(sf::Vector2i) override;
			
			// void updatePosition() override;
			
		protected:
		private:
			int 		_life;
			std::string _name;
			sf::Sprite _sprite;
			sf::Texture _texture;
			sf::Vector2i _pos;
			sf::Vector2f _move;
			sf::IntRect _rect;
			sf::IntRect _rect_s;
			sf::Image _image;
			float _vitesse;
	};
}
#endif /* !EMissile_HPP_ */
