/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Sprite
*/

#ifndef Sprite_HPP_
#define Sprite_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "../Items/Items.hpp"


namespace rtype
{
	class Sprite : public rtype::Items {
		public:
			explicit Sprite(std::string path, sf::Vector2i pos, sf::IntRect rect, sf::IntRect rect_w, float vitesse = 0);
			explicit Sprite(std::string path, sf::Vector2i pos, sf::IntRect rect, float vitesse = 0);
			explicit Sprite(std::string path, sf::Vector2i pos, sf::IntRect rect, sf::Vector2f move, float vitesse = 0);
			Sprite();
			~Sprite();
			float getVitesse(void) override;
			void setVitesse(float vitesse) override;
			sf::Sprite getSprite(void) override;
			sf::Texture getTexture(void) override;
			sf::Vector2i getPos(void) override;
			void setPos(sf::Vector2i pos) override;
			sf::IntRect getRect() override;
			void animate() override;
			void setPosition(sf::Vector2i) override;
			void setMove(sf::Vector2i) override;
			void setScale(sf::Vector2f scale) override;
			bool updatePosition() override;
			void setRect(sf::IntRect) override;

		protected:
		private:
			int 		_life;

			sf::Sprite _sprite;
			sf::Texture _texture;
			sf::Vector2i _pos;
			sf::Vector2f _move;
			sf::IntRect _rect;
			sf::IntRect _rect_s;
			sf::Image _image;
			float _vitesse;
	};
}
#endif /* !Sprite_HPP_ */
