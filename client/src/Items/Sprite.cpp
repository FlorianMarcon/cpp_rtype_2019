/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Sprite
*/

#include "Sprite.hpp"
#include <iostream>


rtype::Sprite::Sprite(std::string path, sf::Vector2i pos, sf::IntRect rect, sf::IntRect rect_w, float vitesse)
{
	this->_vitesse = vitesse;
	this->_rect.top = rect_w.top;
	this->_rect.left = rect_w.left;
	this->_rect.width = rect_w.width;
	this->_rect.height = rect_w.height;
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
	this->_sprite.setPosition((sf::Vector2f)pos);
	if (!(_image.loadFromFile(path)))
		std::cout << "Cannot load image" << std::endl;
	else {
		this->_texture.loadFromImage(this->_image, rect);
		this->_sprite.setTexture(this->_texture);
	}
}

rtype::Sprite::Sprite(std::string path, sf::Vector2i pos, sf::IntRect rect, float vitesse)
{
	this->_vitesse = vitesse;
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
	this->_rect_s.top = rect.top;
	this->_rect_s.left = rect.left;
	this->_rect_s.width = rect.width;
	this->_rect_s.height = rect.height;
	this->_sprite.setPosition((sf::Vector2f)pos);
	if (!(_image.loadFromFile(path)))
		std::cout << "Cannot load image" << std::endl;
	else {
		this->_texture.loadFromImage(this->_image, rect);
		this->_sprite.setTexture(this->_texture);
	}
}

rtype::Sprite::Sprite(std::string path, sf::Vector2i pos, sf::IntRect rect, sf::Vector2f move, float vitesse)
{
	this->_vitesse = vitesse;
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
	this->_move.x = move.x;
	this->_move.y = move.y;
	this->_sprite.setPosition((sf::Vector2f)pos);
	if (!(_image.loadFromFile(path)))
		std::cout << "Cannot load image" << std::endl;
	else {
		this->_texture.loadFromImage(this->_image, rect);
		this->_sprite.setTexture(this->_texture);
	}
}

rtype::Sprite::Sprite()
{

}

rtype::Sprite::~Sprite()
{
	this->_texture.~Texture();
}


float rtype::Sprite::getVitesse(void)
{
	return this->_vitesse;
}

void rtype::Sprite::setVitesse(float vitesse)
{
	this->_vitesse = vitesse;
}

sf::Sprite rtype::Sprite::getSprite(void)
{
 return this->_sprite;
}

sf::Texture rtype::Sprite::getTexture(void)
{
 return this->_texture;
}

sf::Vector2i rtype::Sprite::getPos(void)
{
 return this->_pos;
}

void rtype::Sprite::setPos(sf::Vector2i pos)
{
    this->_pos.x = pos.x;
    this->_pos.y = pos.y;
	this->_sprite.setPosition((sf::Vector2f){(float)pos.x, (float)pos.y});
}


sf::IntRect rtype::Sprite::getRect()
{
	return this->_rect_s;
}

void 	rtype::Sprite::animate()
{
	// (void)clock;
	// static int s = 0;
	
	if (this->_pos.x + this->_move.x > 0 && this->_pos.x + this->_move.x < 1600) {
		this->_pos.x += this->_move.x * this->_vitesse;
	}
	if (this->_pos.y + this->_move.y > 0 && this->_pos.y + this->_move.y < 900) {
		this->_pos.y += this->_move.y * this->_vitesse;
	}
	this->_sprite.setPosition((sf::Vector2f)this->_pos);

// FOR PLAYER MOVE ANIME
	// int one = 10;
	// int two = 30;
	// 	if (s == 0)
	// 		this->_rect_s.left = 33 * 2;		
	// 	if (s == one)
	// 		this->_rect_s.left = 33 * 3;
	// 	if (s == two)
	// 		this->_rect_s.left = 33 * 4;
	// 	if (this->_move.y > 0)
	// 		if (s < two)
	// 			s += 1;
	// 	if (s == -one)
	// 		this->_rect_s.left = 33;
	// 	if (s == -two)
	// 		this->_rect_s.left = 0;
	// 	if (this->_move.y < 0)
	// 		if (s > -two)
	// 			s -= 1;
	// 	if (this->_move.y == 0) {
	// 		if (s < 0)
	// 			s += 1;
	// 		if (s > 0)
	// 			s -= 1;
	// 	}
	// 	//this->_sprite.setTextureRect(this->_rect_s);
	// 	std::cout << "rect left = " << this->_rect_s.left << std::endl;

}


void rtype::Sprite::setMove(sf::Vector2i move)
{
	this->_move.x = move.x;
	this->_move.y = move.y;
}


void rtype::Sprite::setPosition(sf::Vector2i pos)
{
	// std::cout << "set posi sprite" << std::endl;
	// std::cout << this->_pos.y << std::endl;
	this->_sprite.setPosition((sf::Vector2f)pos);
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;

	// std::cout << this->_pos.y << std::endl;
}

// void rtype::Sprite::updatePosition()
// {
// 	if (this->_pos.x + this->_move.x > 0 && this->_pos.x + this->_move.x < 1600) {
// 		this->_pos.x += this->_move.x * this->_vitesse;
// 	}
// 	if (this->_pos.y + this->_move.y > 0 && this->_pos.y + this->_move.y < 900) {
// 		this->_pos.y += this->_move.y * this->_vitesse;
// 	}
// 	this->_sprite.setPosition((sf::Vector2f)this->_pos);
// }

bool rtype::Sprite::updatePosition()
{
	if (_pos.x + _move.x * _vitesse > 0 && this->_pos.x + _move.x  *_vitesse < 1920 - _rect_s.width) {
		this->_pos.x += this->_move.x * this->_vitesse;
	} else {
		return false;
	}
	if (this->_pos.y + this->_move.y * _vitesse > 0 && this->_pos.y + this->_move.y * _vitesse < 1080 -_rect_s.height) {
		this->_pos.y += this->_move.y * this->_vitesse;
	} else {
		return false;
	}
	this->_sprite.setPosition((sf::Vector2f)this->_pos);
	return true;
}

void rtype::Sprite::setScale(sf::Vector2f scale)
{
    this->_sprite.setScale(scale);
}

void rtype::Sprite::setRect(sf::IntRect rect)
{
	this->_rect_s.top = rect.top;
	this->_rect_s.left = rect.left;
	this->_rect_s.height = rect.height;
	this->_rect_s.width = rect.width;
	// this->_sprite.setTextureRect(this->_rect_s);
}