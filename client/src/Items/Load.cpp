/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Load
*/

#include "Load.hpp"
#include <iostream>


rtype::Load::Load(sf::Vector2i pos, float vitesse)
{
	this->_vitesse = vitesse;
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;

	this->_rect_s.top = 51;
	this->_rect_s.left = 0;
	this->_rect_s.width = 33;
	this->_rect_s.height = 33;

    // this->_move = {1, 0};

	this->_sprite.setPosition((sf::Vector2f)pos);
	if (!(_image.loadFromFile("../client/src/Font/sprites/r-typesheet1.gif")))
		std::cout << "Cannot load image" << std::endl;
	else {
		this->_texture.loadFromImage(this->_image);
		this->_sprite.setTexture(this->_texture);
		this->_sprite.setTextureRect(this->_rect_s);
	}
}


rtype::Load::~Load()
{
	this->_texture.~Texture();
}


float rtype::Load::getVitesse(void)
{
	return this->_vitesse;
}

void rtype::Load::setVitesse(float vitesse)
{
	this->_vitesse = vitesse;
}

sf::Sprite rtype::Load::getSprite(void)
{
 return this->_sprite;
}

sf::Texture rtype::Load::getTexture(void)
{
 return this->_texture;
}

sf::Vector2i rtype::Load::getPos(void)
{
 return this->_pos;
}

sf::IntRect rtype::Load::getRect()
{
    // std::cout << "left = " << this->_rect.left << " top = " << _rect.top << " h = " << _rect.height << " w " << _rect.width << std::endl;
	return this->_rect_s;
}

void 	rtype::Load::animate()
{
	if (this->_rect_s.left < 33 * 7)
        this->_rect_s.left += 33;
    else
        this->_rect_s.left = 0;

    this->_sprite.setTextureRect(this->_rect_s);
}


void rtype::Load::setMove(sf::Vector2i move)
{
	this->_move.x = move.x;
	this->_move.y = move.y;
}


void rtype::Load::setPosition(sf::Vector2i pos)
{
	this->_sprite.setPosition((sf::Vector2f)pos);
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
}

// void rtype::Load::updatePosition()
// {
// 	if (this->_pos.x + this->_move.x > 0 && this->_pos.x + this->_move.x < 1600) {
// 		this->_pos.x += this->_move.x * this->_vitesse;
// 	}
// 	if (this->_pos.y + this->_move.y > 0 && this->_pos.y + this->_move.y < 900) {
// 		this->_pos.y += this->_move.y * this->_vitesse;
// 	}
// 	this->_sprite.setPosition((sf::Vector2f)this->_pos);
// }