/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Ennemy01
*/

#ifndef Ennemy01_HPP_
#define Ennemy01_HPP_

#include <iostream>
#include "Items.hpp"
#include "Sprite.hpp"
#include "Network/UDP.hpp"
#include <vector>
#include "Missile.hpp"
#include "Load.hpp"
#include "Player.hpp"
#include "RedShip.hpp"

namespace rtype {
	class Ennemy01 : public rtype::Player {

		public:

			enum Ship{
				BLUE_SKY = 0,
				PURPLE = 1,
				GREEN = 2,
				RED = 3,
				BLUE = 4
			};

		
			Ennemy01();
			Ennemy01(std::string, sf::Vector2i);
			virtual ~Ennemy01();
			//Ennemy01(color);
			// void setName(std::string name);
			// void setNameGame(std::string nameGame);
			// void setNameRoom(std::string nameRoom);
			// void setAddress(std::string address);
			// void setPort(int port);
			// void setUdp(rtype::UDP *udp);
			// std::string getName(void);
			// std::string getNameGame(void);
			// std::string getNameRoom(void);
			// std::string getAddress(void);
			// int getPort(void);
			// rtype::UDP * getUdp(void);
			// void setSizeName(int sizeName);
			// int getSizeName(void);
			// static Ennemy01 *getInstance();
			// static void setInstance(Ennemy01 *instance);
			// // Ennemy01(const Ennemy01 &P)
			// // {
			// // 	_nameRoom = P._nameRoom;
			// // 	_nameGame = P._nameGame;
			// // 	_name = P._name;
			// // 	_sizeName = P._sizeName;
			// // 	_instance = P._instance;
			// // }
			// void setItem(rtype::Sprite*);
			// rtype::Sprite *getItem();

			// void 	addMissile(Items *m);

			// void updatePos();
			void animate() override;
			// void shoot();
			// void loadShot();

			// void setMove(sf::Vector2f);
			// sf::Vector2f getMove(void);
			// void setPosition(sf::Vector2f);
			// sf::Vector2f getPosition();
			// std::vector<rtype::Items*> getMissiles();
			// void setVitesse(float);
			// float getVitesse();
			// bool isLoad();
			// rtype::Items *getLoad();



		protected:
		private:
            // rtype::UDP *	_udp;
			// float			_vitesse; 
			// sf::Vector2f	_move;
			// sf::Vector2f	_pos;
			// std::string 	_name;
			// std::string 	_nameGame;
			// std::string 	_nameRoom;
			// std::string 	_address;
			// int 			_port;
			// int 			_sizeName;
			static Ennemy01	*_instance;
			// rtype::Sprite	*_item;
			// sf::IntRect		_rect;
			// std::vector<Items*> _missiles;
			// rtype::Items   *_load;
			// bool			_loadShot;
			


	};
}

#endif /* !Ennemy01_HPP_ */