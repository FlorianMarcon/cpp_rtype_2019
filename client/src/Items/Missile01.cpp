/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Missile01
*/

#include "Missile01.hpp"
#include <iostream>

rtype::Missile01::Missile01(sf::Vector2i pos, float vitesse, sf::Vector2i move)
{
	this->_vitesse = vitesse;
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;

	this->_rect.top = 120 + 15;
	this->_rect.left = 7 * 33;
	this->_rect.width = 33;
	this->_rect.height = 18;

    this->_move.x = move.x;
	this->_move.y = move.y;

	this->_sprite.setPosition((sf::Vector2f)pos);
	if (!(_image.loadFromFile("../client/src/Font/sprites/r-typesheet1.gif")))
		std::cout << "Cannot load image" << std::endl;
	else {
		this->_texture.loadFromImage(this->_image, this->_rect);
		this->_sprite.setTexture(this->_texture);
	}
}


rtype::Missile01::~Missile01()
{
	this->_texture.~Texture();
}


float rtype::Missile01::getVitesse(void)
{
	return this->_vitesse;
}

void rtype::Missile01::setVitesse(float vitesse)
{
	this->_vitesse = vitesse;
}

sf::Sprite rtype::Missile01::getSprite(void)
{
 return this->_sprite;
}

sf::Texture rtype::Missile01::getTexture(void)
{
 return this->_texture;
}

sf::Vector2i rtype::Missile01::getPos(void)
{
 return this->_pos;
}

sf::IntRect rtype::Missile01::getRect()
{
	return this->_rect_s;
}

void 	rtype::Missile01::animate()
{
	// (void)clock;
	// static int s = 0;
	
	// if (this->_pos.x + this->_move.x > 0 && this->_pos.x + this->_move.x < 1600) {
	// 	this->_pos.x += this->_move.x * this->_vitesse;
	// }
	// if (this->_pos.y + this->_move.y > 0 && this->_pos.y + this->_move.y < 900) {
	// 	this->_pos.y += this->_move.y * this->_vitesse;
	// }
	// this->_sprite.setPosition((sf::Vector2f)this->_pos);

}


void rtype::Missile01::setMove(sf::Vector2i move)
{
	this->_move.x = move.x;
	this->_move.y = move.y;
}


void rtype::Missile01::setPosition(sf::Vector2i pos)
{
	this->_sprite.setPosition((sf::Vector2f)pos);
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
}

// void rtype::Missile01::updatePosition()
// {
// 	if (this->_pos.x + this->_move.x > 0 && this->_pos.x + this->_move.x < 1900) {
// 		this->_pos.x += this->_move.x * this->_vitesse;
// 		this->_sprite.setPosition((sf::Vector2f)this->_pos);
// 	} else {
// 		this->~Missile01();
// 	}


// 	if (this->_pos.y + this->_move.y > 0 && this->_pos.y + this->_move.y < 1400) {
// 		this->_pos.y += this->_move.y * this->_vitesse;
// 		this->_sprite.setPosition((sf::Vector2f)this->_pos);
// 	} else {
// 		this->~Missile01();
// 	}
	
// }