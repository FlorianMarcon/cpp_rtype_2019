/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Items
*/

#include "Items.hpp"
#include <iostream>

rtype::Items::Items()
{
}

rtype::Items::~Items()
{
	this->_texture.~Texture();
}

float rtype::Items::getVitesse(void)
{
	return this->_vitesse;
}

void rtype::Items::setVitesse(float vitesse)
{
	this->_vitesse = vitesse;
}

sf::Sprite rtype::Items::getSprite(void)
{
 return this->_sprite;
}

sf::Texture rtype::Items::getTexture(void)
{
 return this->_texture;
}

sf::Vector2i rtype::Items::getPos(void)
{
 return this->_pos;
}

void rtype::Items::setPos(sf::Vector2i pos)
{
    this->_pos.x = pos.x;
    this->_pos.y = pos.y;
	this->_sprite.setPosition((sf::Vector2f){(float)pos.x, (float)pos.y});
}

sf::IntRect rtype::Items::getRect()
{
	return this->_rect;
}

bool rtype::Items::isHover(sf::Vector2i pos)
{
	(void)pos;
	return (false);
}

sf::Vector2f rtype::Items::getSize(void)
{
	return this->_size;
}

void rtype::Items::setSize(sf::Vector2f size)
{
	this->_size.x = size.x;
	this->_size.y = size.y;
}


std::string rtype::Items::getPath(void)
{
	return this->_path;
}

Color rtype::Items::getColor(void)
{
	return this->_color;
}

void rtype::Items::animate()
{

}

void rtype::Items::setMove(sf::Vector2i move)
{
	this->_move.x = move.x;
	this->_move.y = move.y;
}

void rtype::Items::setPosition(sf::Vector2i pos)
{
	this->_pos.x = pos.x;
	this->_pos.y = pos.y;
	this->_sprite.setPosition((sf::Vector2f){(float)pos.x, (float)pos.y});
}

void rtype::Items::setScale(sf::Vector2f scale)
{
    this->_sprite.setScale(scale);
	this->_sprite.setPosition((sf::Vector2f)this->_pos);
}

bool rtype::Items::updatePosition()
{
	std::cout << "update pos" << std::endl;
	if (_pos.x + _move.x * _vitesse > -100 && this->_pos.x + _move.x  *_vitesse < 1920 + 100) {
		this->_pos.x += this->_move.x * this->_vitesse;
	} else {
		std::cout << "up false" << std::endl;
		return false;
	}
	if (this->_pos.y + this->_move.y * _vitesse > -100 && this->_pos.y + this->_move.y * _vitesse < 1080 + 100) {
		this->_pos.y += this->_move.y * this->_vitesse;
	} else {
		std::cout << "up false" << std::endl;
		return false;
	}
	this->_sprite.setPosition((sf::Vector2f)this->_pos);
		std::cout << "up true" << std::endl;
	return true;
}

void rtype::Items::setRect(sf::IntRect rect)
{	
	this->_rect.top = rect.top;
	this->_rect.left = rect.left;
	this->_rect.height = rect.height;
	this->_rect.width = rect.width;
}

void 	rtype::Items::setName(std::string name)
{
	this->_name = name;
}

std::string rtype::Items::getName()
{
	return this->_name;
}