/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Item
*/

// #ifndef ITEM_HPP_
// #define ITEM_HPP_

// #include "IItems.hpp"


// class Item : public IItems {
// 	public:

// 		Item();
// 		~Item();

//         void setType(Type type) override;
// 		void setPos(Vector2<int>) override;
// 		void setSprite(sf::Sprite sp) override;
// 		void setText(sf::Text txt) override;
// 		void createSprite(std::string path, Vector2<int> pos) override;
// 		void createText(std::string text, Vector2<int> pos, int size, sf::Color color) override;
// 		Type getType() override;
// 		sf::Sprite getSprite() override;
// 		sf::Text getText() override;
//         Vector2<int> getPos() override;

// 	protected:
// 	private:
// 		Vector2<int> 	_pos;
// 		sf::Sprite 		_sprite;
// 		sf::Texture		_texture;
// 		sf::Text		_text;
// 		Type 			_type;
// };

// #endif /* !ITEM_HPP_ */
