/*
** EPITECH PROJECT, 2019
** librtype
** File description:
** MetadataMessage
*/

#ifndef METADATAMESSAGE_HPP_
#define METADATAMESSAGE_HPP_

#include <map>
#include <tuple>
#include "IMessage.hpp"


namespace rtype {

	using MetadataType = std::string;
	/**
	 * @brief Metadata type message
	 * 
	 */
	class MetadataMessage: public IMessage {
		public:
			MetadataMessage();
			~MetadataMessage();

			/**
			 * @brief Add data to MetadataMessage
			 * 
			 * @param key Key of the value
			 * @param value Value of the key
			 * @param ovveride Ovveride if is already present
			 * 
			 * throw if key is already present and ovveride is False
			 */
			void	addData(const MetadataType &key, const MetadataType &value, bool ovveride=false);

			/**
			 * @brief Remove data from MetadaMessage			 * 
			 * @param key key to remove
			 */
			void	removeData(const std::string &key);

			/**
			 * @brief Serialize metada object
			 * 
			 * @return std::pair<char *, int> 
			 */
			virtual IMessage::SerializedObject	serialize();

			/**
			 * @brief 
			 * 
			 * @param buffer 
			 * @param lenght 
			 */
			virtual void	unserialize(const char *buffer, unsigned int lenght);
			virtual void	unserialize(const IMessage::SerializedObject &serializedObject);
			/**
			 * @brief Get the Data object
			 * 
			 * @param key Key
			 * @return MetadataType* Value of the key 
			 */
			MetadataType	&getData(const MetadataType &key);

			/**
			 * @brief Access to the element
			 * 
			 * @param key Key to access
			 * @return MetadataType* Value
			 */
			MetadataType	&operator[](const MetadataType &key);


		protected:
		private:

			std::map<MetadataType, MetadataType>	_data;
	};

	MetadataMessage	&operator<<(MetadataMessage &obj, const std::pair<MetadataType, MetadataType> &toadd);
}

#endif /* !METADATAMESSAGE_HPP_ */
