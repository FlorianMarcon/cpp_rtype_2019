/*
** EPITECH PROJECT, 2019
** librtype
** File description:
** GenericMessage
*/

#include <streambuf>
#include <cstring>
#include "GenericMessage.hpp"

namespace rtype {

	GenericMessage::GenericMessage()
	{
	}

	GenericMessage::~GenericMessage()
	{
	}

	/**
	 * 		GETTER
	 */
	
	char	GenericMessage::getIdMessage() const
	{
		return (this->_idMessage);
	}

	
	IMessage	*GenericMessage::getBody() const
	{
		return (this->_body);
	}

	IMessage::SerializedObject	GenericMessage::getBodyUnserialized() const
	{
		return (std::make_pair(this->_buffer, this->_bufferLenght));
	}

	/**
	 * 		SETTER
	 */
	
	void	GenericMessage::setIdMessage(char idMessage)
	{
		this->_idMessage = idMessage;
	}

	
	void	GenericMessage::setBody(IMessage &body)
	{
		this->_body = &body;
	}

	/**
	 * 		OTHER
	 */

	
	IMessage::SerializedObject	GenericMessage::serialize()
	{
		IMessage::SerializedObject t;// = std::make_pair(nullptr, 0);
		IMessage	*body = this->getBody();

		if (body != nullptr) {
			t = body->serialize();
		}
		if (this->_buffer != nullptr)
			free(this->_buffer);
		this->_bufferLenght = (1 + 4 + std::get<1>(t));
		this->_buffer = (char *) malloc(this->_bufferLenght);
		if (this->_buffer == nullptr) {
			this->_bufferLenght = 0;
			return std::make_pair(nullptr, 0);
		}
		this->_buffer[0] = this->getIdMessage();
		std::memcpy(&this->_buffer[1], &std::get<1>(t), sizeof(this->_bufferLenght));
		std::memcpy(&this->_buffer[5], std::get<0>(t), std::get<1>(t));
		return  std::make_pair(this->_buffer, this->_bufferLenght);
	}

	
	void	GenericMessage::unserialize(const char *buffer, unsigned int lenght) 
	{
		if (lenght < 5) {
			throw std::length_error("Bad message size < 5");
		}

		this->_idMessage = buffer[0];
		std::memcpy(&this->_bufferLenght, &buffer[1], sizeof(this->_bufferLenght));	
		this->_buffer = (char *)malloc((this->_bufferLenght * sizeof(char)));
		std::memcpy(this->_buffer, &buffer[1 + sizeof(this->_bufferLenght)], this->_bufferLenght);
	}

	void	GenericMessage::unserialize(const IMessage::SerializedObject &serializedObject)
	{
		this->unserialize(std::get<0>(serializedObject), std::get<1>(serializedObject));
	}
}
