/*
** EPITECH PROJECT, 2019
** librtype
** File description:
** MetadataMessage
*/

#include <exception>
#include <iostream>
#include <sstream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/iter_find.hpp>
#include <boost/algorithm/string/finder.hpp>
#include "MetadataMessage.hpp"

namespace rtype {

	MetadataMessage::MetadataMessage()
	{
	}

	MetadataMessage::~MetadataMessage()
	{
	}

	void	MetadataMessage::addData(const MetadataType &key, const MetadataType &value, bool ovveride)
	{
		const int count = this->_data.count(key);
		
		if (count != 0 && ovveride == false) {
			throw std::logic_error("Key " + key + "is already present in metada");
		} else if (count != 0 && ovveride == true) {
			this->_data.erase(key);
		}
		this->_data.insert(std::make_pair(key, value));
	}

	void	MetadataMessage::removeData(const std::string &key)
	{
		this->_data.erase(key);
	}

	std::pair<const char *, int>	MetadataMessage::serialize()
	{
		std::stringstream buff;
		const char *data;

		for (auto it = this->_data.begin(); it != this->_data.end(); it++) {
			buff << it->first << ":" << it->second << "\r\n";
		}
		data = buff.str().c_str();
		return std::make_pair(data,((int)buff.str().size()));
	}
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
	void	MetadataMessage::unserialize(const char *buffer, unsigned int lenght)
	{
		std::cout << "MetadataMessage::unserialize" << std::endl;
		std::string sbuffer;
		std::vector<std::string> vec;
	
		if (buffer == nullptr || lenght == 0) {
			std::cout << "End anticipated MetadataMessage::unserialize" << std::endl;
			return;
		}
		sbuffer = buffer;
		(void)lenght;
		sbuffer = sbuffer.substr(0, lenght);
		try
		{
			/* code */
			std::size_t current, previous = 0;
    		current = sbuffer.find("\r\n");
    		while (current != std::string::npos) {
				vec.push_back(sbuffer.substr(previous, current - previous));
				previous = current + 2;
				current = sbuffer.find("\r\n", previous);
			}
			vec.push_back(sbuffer.substr(previous, current - previous));
			// boost::iter_split(vec, sbuffer, boost::algorithm::first_finder("\r\n"));
		}
		catch(const std::exception& e)
		{
			std::cerr << e.what() << '\n';
			return;
		}
		
		this->_data.clear();

		for (std::string substr: vec) {
			std::vector<std::string> vecline;
			std::size_t current, previous = 0;
			vecline.clear();
    		current = substr.find(":");
    		while (current != std::string::npos) {
				vecline.push_back(substr.substr(previous, current - previous));
				previous = current + 1;
				current = substr.find(":", previous);
			}
			vecline.push_back(substr.substr(previous, current - previous));

			// boost::iter_split(vecline, substr, boost::algorithm::first_finder(":"));

			if (vecline.size() == 2) {
				std::string key = vecline[0];
				std::string value = vecline[1];

				*this << std::make_pair(key, value);
			} else if ((vecline.size() == 1 && vecline[0].size() != 0)){
				throw std::logic_error("Bad buffer formating");
			}
		}
		std::cout << "End MetadataMessage::unserialize" << std::endl;


	}
	void	MetadataMessage::unserialize(const IMessage::SerializedObject &serializedObject)
	{
		this->unserialize(std::get<0>(serializedObject), std::get<1>(serializedObject));
	}
	

	MetadataType	&MetadataMessage::getData(const MetadataType &key)
	{
		try
		{
			return (this->_data.at(key));
		}
		catch(const std::exception& e)
		{
			std::stringstream ss;
			ss << "key: " << key << " didn't find in metadata";
			std::string msg = ss.str();
			throw std::range_error(msg);
		}
		
	}

	MetadataType	&MetadataMessage::operator[](const MetadataType &key)
	{
		return (this->getData(key));
	}

	MetadataMessage	&operator<<(MetadataMessage &obj, const std::pair<MetadataType , MetadataType> &toadd)
	{
		obj.addData(std::get<0>(toadd), std::get<1>(toadd), true);
		return obj;
	}

}
