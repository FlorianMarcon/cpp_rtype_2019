/*
** EPITECH PROJECT, 2019
** librtype
** File description:
** GenericMessage
*/

#ifndef GENERICMESSAGE_HPP_
#define GENERICMESSAGE_HPP_

#include <tuple>
#include "IMessage.hpp"

namespace rtype {


	/**
	 * @brief Enum representing Status code
	 * 
	 */
	enum StatusResponse: int {
		OK = 200,

		BAD_REQUEST = 400,
		UNAUTHORIZED = 401,
		NOT_FOUND = 404,
		UNPROCESSABLE_ENTITY = 422,
	};

	/**
	 * @brief Generic Message
	 * This class is the most basic message
	 * 
	 * It contain one ID and a body
	 */
	class GenericMessage: public IMessage {
		public:
			GenericMessage();
			~GenericMessage();

			/**
			 * @brief Get the Body object
			 * 
			 * @return IMessage* Body
			 */
			IMessage	*getBody() const;

			/**
			 * @brief Get the Body Unserialized object
			 * 
			 * @return IMessage::SerializedObject 
			 */
			IMessage::SerializedObject getBodyUnserialized() const;

			/**
			 * @brief Set the Body object
			 * 
			 * @param body Body to set
			 */
			void	setBody(IMessage &body);

			/**
			 * @brief Get serialized object
			 * 
			 * @return std::pair<char *, int> 	Tuple containing buffer and size of the buffer
			 */
			virtual	IMessage::SerializedObject	serialize();

			/**
			 * @brief Unserialize the object
			 * 
			 * @param buffer Buffer containing serialized object
			 * @param lenght Lenght of the buffer
			 */
			virtual void	unserialize(const char *buffer, unsigned int lenght) ;
			virtual void	unserialize(const IMessage::SerializedObject &serializedObject);

			/**
			 * @brief Get the Id Message object
			 * 
			 * @return char Get id message
			 */
			char	getIdMessage() const;

			/**
			 * @brief Set the Id Message object
			 * 
			 * @param idMessage ID Message
			 */
			void	setIdMessage(char idMessage);

		protected:
		private:

			char 	_idMessage;	/* <! Id of the message */

			IMessage	*_body = nullptr;	/* <! body of the message */

			char		*_buffer = nullptr;	/* <! Buffer containing serialzed object */
			int			_bufferLenght = 0;	/* <! Buffer Lenght */
	};
}

#endif /* !GENERICMESSAGE_HPP_ */
