/*
** EPITECH PROJECT, 2019
** librtype
** File description:
** IMessage
*/

#ifndef IMESSAGE_HPP_
#define IMESSAGE_HPP_
#include <tuple>
namespace rtype {
	/**
	 * @brief Message Interface
	 * 
	 */
	class IMessage {
		public:

			virtual ~IMessage() {};
			using SerializedObject = std::pair<const char *, int>;

			/**
			 * @brief Serialize the object
			 * 
			 * @return std::pair<char *, int> Tuple containing buffer and leght of the serialized object 
			 */
			virtual IMessage::SerializedObject	serialize() = 0;

			
			/**
			 * @brief Unserialize the buffer
			 * 
			 * Throw if bad format
			 * @param buffer Buffer containing serialized object
			 * @param lenght Buffer lenght
			 */
			virtual void	unserialize(const char *buffer, unsigned int lenght)  = 0;
			virtual void	unserialize(const SerializedObject &serializedObject) = 0;


		protected:
		private:
	};
}

#endif /* !IMESSAGE_HPP_ */
