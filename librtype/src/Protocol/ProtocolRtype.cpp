/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** ProtocolRtype
*/

#include "ProtocolRtype.hpp"

namespace rtype
{

	GenericMessage	*ProtocolRtype::generatePacketWithMetadata(ProtocolRtype::IDPacket idPacket)
	{
		GenericMessage *message;
		MetadataMessage *metada;

		if ((message = new GenericMessage()) == nullptr)
			return nullptr;
		else if ((metada = new MetadataMessage()) == nullptr) {
			delete metada;	
			return nullptr;
		}
		message->setIdMessage(idPacket);
		message->setBody(*metada);
		return (message);
	}
	GenericMessage	*ProtocolRtype::generatePacketStartingIn(unsigned int seconds, unsigned int step)
	{
		GenericMessage *message = ProtocolRtype::generatePacketWithMetadata(ProtocolRtype::IDPacket::STARTING_IN);
		MetadataMessage *metadata = (MetadataMessage *)message->getBody();

		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::SECONDS), std::to_string(seconds));
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::STEP), std::to_string(step));
		return (message);
	}
	GenericMessage	*ProtocolRtype::generatePacketPosition(std::string id, unsigned int x, unsigned int y)
	{
		GenericMessage *message = ProtocolRtype::generatePacketWithMetadata(ProtocolRtype::IDPacket::POSITIONS);
		MetadataMessage *metadata = (MetadataMessage *)message->getBody();

		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID), id);
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::X), std::to_string(x));
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::Y), std::to_string(y));
		return (message);
	}

	GenericMessage *ProtocolRtype::generatePacketMissile(unsigned int id, unsigned int power, unsigned int x, unsigned int y)
	{
		GenericMessage *message = ProtocolRtype::generatePacketWithMetadata(ProtocolRtype::IDPacket::MISSILE);
		MetadataMessage *metadata = (MetadataMessage *)message->getBody();

		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::POWER), std::to_string(power));
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::X), std::to_string(x));
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID), std::to_string(id));
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::Y), std::to_string(y));
		return (message);
	}

	GenericMessage	*ProtocolRtype::generatePacketEnemyPosition(const std::string &id, const std::string &type, unsigned int x, unsigned int y)
	{
		GenericMessage *message = ProtocolRtype::generatePacketWithMetadata(ProtocolRtype::IDPacket::ENEMY_POSITIONS);
		MetadataMessage *metadata = (MetadataMessage *)message->getBody();

		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::TYPE), type);
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID), id);
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::X), std::to_string(x));
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::Y), std::to_string(y));
		return (message);
	}
	GenericMessage	*ProtocolRtype::generatePacketDeath(const std::string &id)
	{
		GenericMessage *message = ProtocolRtype::generatePacketWithMetadata(ProtocolRtype::IDPacket::DEATH);
		MetadataMessage *metadata = (MetadataMessage *)message->getBody();

		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID), id);
		return (message);
	}
	GenericMessage	*ProtocolRtype::generateLifePacket(const std::string &id, unsigned int life)
	{
		GenericMessage *message = ProtocolRtype::generatePacketWithMetadata(ProtocolRtype::IDPacket::LIFE);
		MetadataMessage *metadata = (MetadataMessage *)message->getBody();

		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::LIFE), std::to_string(life));
		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID), id);
		return (message);
	}
	GenericMessage	*ProtocolRtype::generateDisconnectionPacket(const std::string &id)
	{
		GenericMessage *message = ProtocolRtype::generatePacketWithMetadata(ProtocolRtype::IDPacket::DISCONNECTION);
		MetadataMessage *metadata = (MetadataMessage *)message->getBody();

		metadata->addData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID), id);
		return (message);
	}
		
} // namespace rtype

