/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Protocol
*/

#ifndef PROTOCOL_HPP_
#define PROTOCOL_HPP_

#include <string>
#include <map>
#include <list>
#include <boost/preprocessor.hpp>
#include <boost/preprocessor/config/config.hpp>
#include "Messages/IMessage.hpp"
#include "Messages/GenericMessage.hpp"
#include "Messages/MetadataMessage.hpp"
#define ENUM_TO_STR(unused,data,elem) \
if (parm == elem) return BOOST_PP_STRINGIZE(elem);

namespace rtype
{

	/**
	 * @brief Static class used to handle protocol
	 * 
	 */
	class Protocol {
		public:

			/**
			 * @brief Enum representig all the idMessages
			 */
			enum IDMessage: char {
					REQUEST = 0x01,
					RESPONSE = 0x02
				};

			/**
			 * @brief Enum representig requests
			 * 
			 */
			enum IDRequest: char {
				SETUP_NAME = 0x01,
				LIST_GAMES = 0x02,
				LIST_ROOMS = 0x03,
				CREATE_ROOM = 0x04,
				CONNECT_ROOM = 0x05,
				DISCONNECT_ROOM = 0x06,
				DISCONNECT_SERVER = 0x07,
				LIST_PLAYERS_IN_ROOM = 0x08,
				LIST_PLAYERS = 0x09,
				START_GAME = 0x0A,
			};

			class MetadataFields {
				public:

				#undef METADATA_FIELDS
				#define METADATA_FIELDS (_INVALID_)\
				(SETUP_NAME)\
				(STATUS_CODE)\
				(LIST_GAMES) \
				(LIST_PLAYERS)\
				(LIST_ROOMS)\
				(GAME_NAME)\
				(ROOM_NAME)\
				(ADDRESS)\
				(PORT)\
				(MESSAGE)\
				(_MAX_)

				enum Fields
				{
					BOOST_PP_SEQ_ENUM(METADATA_FIELDS)
				};

				static const std::string toString(const Fields parm)
				{
					BOOST_PP_SEQ_FOR_EACH(ENUM_TO_STR,~,METADATA_FIELDS)
					return "_INVALID_";
				}
			};


			static GenericMessage	*generateRequest();
			static GenericMessage	*generateGenericMessage(char id);
			static GenericMessage	*generateResponse();
			static GenericMessage	*generateResponse(int status);
			static GenericMessage	*generateResponse(int status, const std::string &message);

			static GenericMessage	*generateSetNameRequest(const std::string &name);
			static GenericMessage	*generateListGamesRequest();
			static GenericMessage	*generateListPlayerRequest();
			static GenericMessage	*generateListRoomRequest(const std::string &game);
			static GenericMessage	*generateCreateRoomRequest(const std::string &game, const std::string &name);
			static GenericMessage	*generateListPlayerInRoomRequest(const std::string &game, const std::string &name);
			static GenericMessage	*generateGoInRoomRequest(const std::string &game, const std::string &name, const std::string &address, unsigned int port);
			static GenericMessage	*generateStartGameRequest(const std::string &game, const std::string &name);

			static GenericMessage	*generateListGamesResponses(std::list<std::string> &games);
			static GenericMessage	*generateListPlayerResponses(std::list<std::string> &player);
			static GenericMessage	*generateListRoomResponses(std::list<std::string> &rooms);
			static GenericMessage	*generateGoInRoomResponses(const std::string &address, unsigned int port);
		protected:
		private:
	};
} // namespace rtype


#endif /* !PROTOCOL_HPP_ */
