/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Protocol
*/

#include <string>
#include <sstream>
#include "Protocol.hpp"

namespace rtype
{

	GenericMessage	*Protocol::generateRequest()
	{
		return (Protocol::generateGenericMessage(rtype::Protocol::REQUEST));
	}
	GenericMessage	*Protocol::generateGenericMessage(char id)
	{
		GenericMessage *message = new GenericMessage();

		message->setIdMessage(id);
		return (message);
	}

	GenericMessage	*Protocol::generateResponse()
	{
		GenericMessage *message = new GenericMessage();

		message->setIdMessage(rtype::Protocol::RESPONSE);
		return (message);
	}

	/**
	 * @brief REQUESR
	 */
	GenericMessage	*Protocol::generateSetNameRequest(const std::string &name)
	{
		GenericMessage *request = Protocol::generateRequest();
		GenericMessage *requestName = new GenericMessage();
		MetadataMessage *metada = new MetadataMessage();

		request->setBody(*requestName);
		requestName->setBody(*metada);
		requestName->setIdMessage(rtype::Protocol::SETUP_NAME);
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::SETUP_NAME), name);
		return (request);
	}
	GenericMessage	*Protocol::generateListGamesRequest()
	{
		GenericMessage *request = Protocol::generateRequest();
		GenericMessage *requestName = Protocol::generateRequest();

		request->setBody(*requestName);
		requestName->setIdMessage(rtype::Protocol::LIST_GAMES);
		return (request);
	}
	GenericMessage	*Protocol::generateListPlayerRequest()
	{
		GenericMessage *request = Protocol::generateRequest();
		GenericMessage *requestName = Protocol::generateRequest();

		request->setBody(*requestName);
		requestName->setIdMessage(rtype::Protocol::LIST_PLAYERS);
		return (request);
	}
	GenericMessage	*Protocol::generateListRoomRequest(const std::string &game)
	{
		GenericMessage *request = Protocol::generateRequest();
		GenericMessage *requestRoom = Protocol::generateRequest();
		MetadataMessage *metada = new MetadataMessage();

		request->setBody(*requestRoom);
		requestRoom->setIdMessage(rtype::Protocol::LIST_ROOMS);
		requestRoom->setBody(*metada);

		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME), game);
		return (request);
	}
	GenericMessage	*Protocol::generateCreateRoomRequest(const std::string &game, const std::string &name)
	{
		GenericMessage *request = Protocol::generateRequest();
		GenericMessage *requestRoom = Protocol::generateRequest();
		MetadataMessage *metada = new MetadataMessage();

		request->setBody(*requestRoom);
		requestRoom->setIdMessage(rtype::Protocol::CREATE_ROOM);
		requestRoom->setBody(*metada);

		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME), game);
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME), name);
		return (request);
	}

	GenericMessage	*Protocol::generateListPlayerInRoomRequest(const std::string &game, const std::string &name)
	{
		GenericMessage *request = Protocol::generateRequest();
		GenericMessage *requestRoom = Protocol::generateGenericMessage(rtype::Protocol::LIST_PLAYERS_IN_ROOM);
		MetadataMessage *metada = new MetadataMessage();

		request->setBody(*requestRoom);
		requestRoom->setBody(*metada);

		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME), game);
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME), name);
		return (request);
	}
	GenericMessage	*Protocol::generateGoInRoomRequest(const std::string &game, const std::string &name, const std::string &address, unsigned int port)
	{
		GenericMessage *request = Protocol::generateRequest();
		GenericMessage *requestRoom = Protocol::generateGenericMessage(rtype::Protocol::CONNECT_ROOM);
		MetadataMessage *metada = new MetadataMessage();

		request->setBody(*requestRoom);
		requestRoom->setBody(*metada);

		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME), game);
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME), name);
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ADDRESS), address);
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::PORT), std::to_string(port));
		return (request);
	}
	GenericMessage	*Protocol::generateStartGameRequest(const std::string &game, const std::string &name)
	{
		GenericMessage *request = Protocol::generateRequest();
		GenericMessage *requestRoom = Protocol::generateGenericMessage(rtype::Protocol::START_GAME);
		MetadataMessage *metada = new MetadataMessage();

		request->setBody(*requestRoom);
		requestRoom->setBody(*metada);

		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME), game);
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME), name);
		return (request);
	}

	/**
	 * @brief RESPONSE
	 */

	GenericMessage	*Protocol::generateResponse(int status)
	{
		GenericMessage *response = Protocol::generateResponse();
		MetadataMessage *metadata = new MetadataMessage();

		metadata->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::STATUS_CODE), std::to_string(status));
		response->setBody(*metadata);

		return (response);
	}
	GenericMessage	*Protocol::generateResponse(int status, const std::string &message)
	{
		GenericMessage *response = Protocol::generateResponse(status);
		MetadataMessage *metadata = (MetadataMessage *) (response->getBody());

		metadata->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::MESSAGE), message);

		return (response);
	}


	GenericMessage	*Protocol::generateListGamesResponses(std::list<std::string> &games)
	{
		GenericMessage *response = Protocol::generateResponse(rtype::OK);
		MetadataMessage *metada = (MetadataMessage *) (response->getBody()); 
		std::stringstream ss;

		for (auto game: games)
		{
			ss << game << ',';
		}
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::LIST_GAMES), ss.str());
		return (response);
	}
	GenericMessage	*Protocol::generateListPlayerResponses(std::list<std::string> &games)
	{
		GenericMessage *response = Protocol::generateResponse(rtype::OK);
		MetadataMessage *metada = (MetadataMessage *) (response->getBody()); 
		std::stringstream ss;

		for (auto game: games)
		{
			ss << game << ',';
		}
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::LIST_PLAYERS), ss.str());
		return (response);
	}

	GenericMessage	*Protocol::generateListRoomResponses(std::list<std::string> &rooms)
	{
		GenericMessage *response = Protocol::generateResponse(rtype::OK);
		MetadataMessage *metada = (MetadataMessage *) (response->getBody()); 
		std::stringstream ss;

		for (auto room: rooms)
		{
			ss << room << ',';
		}
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::LIST_ROOMS), ss.str());
		return (response);
	}
	GenericMessage	*Protocol::generateGoInRoomResponses(const std::string &address, unsigned int port)
	{
		GenericMessage *response = Protocol::generateResponse(rtype::OK);
		MetadataMessage *metada = (MetadataMessage *) (response->getBody()); 

		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ADDRESS), address);
		metada->addData(Protocol::MetadataFields::toString(Protocol::MetadataFields::PORT), std::to_string(port));
		return (response);
	}
} // namespace rtype


