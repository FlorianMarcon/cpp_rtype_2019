/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** ProtocolRtype
*/

#ifndef PROTOCOLRTYPE_HPP_
#define PROTOCOLRTYPE_HPP_

#include <boost/preprocessor.hpp>
#include <boost/preprocessor/config/config.hpp>
#include "Messages/GenericMessage.hpp"
#include "Messages/MetadataMessage.hpp"
#define ENUM_TO_STR(unused,data,elem) \
if (parm == elem) return BOOST_PP_STRINGIZE(elem);

namespace rtype
{
	/**
	 * @brief Static class representing protocol Rtype
	 * 
	 */
	class ProtocolRtype {
		public:

			enum IDPacket: char {
				STARTING_IN = 0x00,
				POSITIONS	= 0x01,
				POINT		= 0x02,
				DEATH		= 0x03,
				MISSILE		= 0x04,
				ENEMY_POSITIONS	= 0x05,
				LIFE		= 0x06,
				DISCONNECTION	= 0x07,
			};

			class MetadataFields {
				public:

				#undef METADATA_FIELDS
				#define METADATA_FIELDS (_INVALID_)\
				(SECONDS)\
				(X)\
				(Y)\
				(ID)\
				(STEP)\
				(VX)\
				(VY)\
				(SPEED)\
				(POWER)\
				(TYPE)\
				(LIFE)\
				(_MAX_)

				enum Fields
				{
					BOOST_PP_SEQ_ENUM(METADATA_FIELDS)
				};

				static const std::string toString(const Fields parm)
				{
					BOOST_PP_SEQ_FOR_EACH(ENUM_TO_STR,~,METADATA_FIELDS)
					return "_INVALID_";
				}
			};
		
			static GenericMessage *generatePacketWithMetadata(IDPacket idPacket);
		
			/**
			 * @brief Generate Packet "Start in"
			 * 
			 * Contain SECONDS Metada
			 * @param seconds Seconds before start
			 * @return GenericMessage* 
			 */
			static GenericMessage *generatePacketStartingIn(unsigned int seconds, unsigned int step);
			
			/**
			 * @brief Player Position
			 * 
			 * SET metadata fields
			 * @param id 	ID
			 * @param x 	X
			 * @param y 	Y
			 * @return GenericMessage* Generic message 
			 */
			static GenericMessage *generatePacketPosition(std::string id, unsigned int x, unsigned int y);
			
			/**
			 * @brief Generate packet representing missile
			 * 
			 * SET metadata fields
			 * @param power	POWER 
			 * @param id 	ID
			 * @param x 	X
			 * @param y 	Y
			 * @return GenericMessage* 
			 */
			static GenericMessage *generatePacketMissile(unsigned int id, unsigned int power, unsigned int x, unsigned int y);
			
			/**
			 * @brief Generate enemy position packet
			 * 
			 * SET metadata fields
			 * @param id	ID 
			 * @param type 	TYPE
			 * @param x 	X
			 * @param y 	Y
			 * @return GenericMessage* 
			 */
			static GenericMessage *generatePacketEnemyPosition(const std::string &id, const std::string &type, unsigned int x, unsigned int y);

			/**
			 * @brief Generate packet death
			 * 
			 * SET metadata fields
			 * @param id 	ID
			 * @return GenericMessage* 
			 */
			static GenericMessage *generatePacketDeath(const std::string &id);
			
			/**
			 * @brief Generate Life packet
			 * 
			 * SET metadata fields
			 * @param id	ID 
			 * @param life 	LIFE
			 * @return GenericMessage* 
			 */
			static GenericMessage *generateLifePacket(const std::string &id, unsigned int life);
	
			/**
			 * @brief Generate disconnection packet
			 * 
			 * SET metadata fields
			 * @param id	ID 
			 * @return GenericMessage* 
			 */
			static GenericMessage *generateDisconnectionPacket(const std::string &id);
	};
	
} // namespace rtype


#endif /* !PROTOCOLRTYPE_HPP_ */
