/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** TCPServer.cpp
*/

#include "TCPServer.hpp"
#include "HandleConnection.hpp"

namespace rtype
{
  
  void TCPServer::handleAccept(Handle::pointer connection, const boost::system::error_code& err)
  {
    if (!err) {
      connection->startServer();
    }
    startServeur();
  };

  void TCPServer::startServeur()
  {
      // socket
    Handle::pointer connection = Handle::createPointer(this->__ioService);

    // asynchronous accept operation and wait for a new connection.
    this->acceptor_.async_accept(connection->createSocket(), boost::bind(&TCPServer::handleAccept, this, connection, boost::asio::placeholders::error));
    this->__ioService.run();
  };

  void	TCPServer::stopServeur()
  {
	  this->__ioService.stop();
  }

	boost::asio::ip::tcp::endpoint	TCPServer::getLocalEndPoint() const
  {
    return (this->acceptor_.local_endpoint());
  }
} // namespace rtype

