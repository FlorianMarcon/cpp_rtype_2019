/*
** EPITECH PROJECT, 2019
** librtype
** File description:
** UDP
*/

#include "UDP.hpp"

namespace rtype {

	UDP::UDP(unsigned int port) :
	_socket(this->io_service, BUDP::endpoint(BUDP::v4(), port))
	{
		boost::asio::io_service netService;
		boost::asio::ip::udp::resolver resolver(netService);
		boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(), "google.com", "");
		boost::asio::ip::udp::resolver::iterator endpoints = resolver.resolve(query);
		boost::asio::ip::udp::endpoint ep = *endpoints;
		boost::asio::ip::udp::socket socket(netService);
		socket.connect(ep);
		this->_localEndpoint = BUDP::endpoint(socket.local_endpoint().address(), this->_socket.local_endpoint().port());
		socket.close();
	}

	UDP::UDP(const UDPEndPoint &endPoint) :
	_socket(this->io_service, endPoint)
	{
	}

	UDP::~UDP()
	{
		if (this->_recvThread != nullptr) {
			this->io_service.stop();
			this->_recvThread->join();
			delete this->_recvThread;
		}
	}

	/**
	 * GETTER
	 */

	std::list<UDPEndPoint>	UDP::getReceivers() const
	{
		return (this->_receiver);
	}

	UDPEndPoint	UDP::getEndPoint() const
	{
		return (this->_localEndpoint);
	}

	/**
	 * SETTER
	 */

	void	UDP::addReceiver(const UDPEndPoint &receiver)
	{
		this->_receiver.push_back(receiver);
	}

	void	UDP::setReceiveCallback(UDP::UDPReceiveCallback	callback)
	{
		this->_receiveCallback = callback;
	}

	void	UDP::removeReceiver(const UDPEndPoint &endPoint)
	{
		this->_receiver.remove(endPoint);
	}

	/**
	 * ASYNC
	 */
	void	UDP::asyncReceive()
	{
		memset(this->_buffer, 0, BUFFER_SIZE);
		this->_socket.async_receive_from(
			boost::asio::buffer(this->_buffer, BUFFER_SIZE),
			this->_recvEndPoint,
        	boost::bind(
				&UDP::handleReceive,
				this,
          		boost::asio::placeholders::error,
          		boost::asio::placeholders::bytes_transferred
			)
		);
	}

	void	UDP::asyncSend(const char *buffer, ssize_t size)
	{
		for (const UDPEndPoint &endPoint: this->getReceivers()) {
			this->_socket.async_send_to(
				boost::asio::buffer(buffer, size),
				endPoint,
				boost::bind(
					&UDP::handleSend,
					this,
					endPoint,
					boost::asio::placeholders::error)
			);
		}
	}


	/**
	 * OTHER
	 */

	void	UDP::handleReceive(const boost::system::error_code& ec, std::size_t bytes_transferred)
	{
		(void)bytes_transferred;
		if (ec == boost::asio::error::eof || ec == boost::asio::error::connection_reset) {
			//TODO: handle connection closed

			try {
				this->_connectionClosedCallback(*this);
			} catch(const std::exception& e) {
				std::cerr << e.what() << '\n';
			}
	
			return;
		}
		try {
			this->_receiveCallback(*this, this->_buffer, bytes_transferred, this->_recvEndPoint);
		} catch(const std::exception& e) {
			std::cerr << e.what() << '\n';
		}
		this->startReceive();
	}

	void	UDP::handleSend(const UDPEndPoint &endPoint, const boost::system::error_code &ec)
	{
		(void)endPoint;
		if (ec == boost::asio::error::eof || ec == boost::asio::error::connection_reset) {
			//TODO: handle connection closed
			try {
				this->_connectionClosedCallback(*this);
			} catch(const std::exception& e) {
				std::cerr << e.what() << "at: " << __FILE__ << "(" << __LINE__ << ")" << '\n';
			}
			return;
		}
	}

	void	UDP::startReceive()
	{
		this->asyncReceive();

		if (
			this->_recvThread != NULL
			&&this->_recvThread->get_id() == boost::this_thread::get_id()
		) {
			this->io_service.run();
		} else if (
			this->_recvThread == NULL ||
			(
				this->_recvThread->timed_join(boost::posix_time::seconds(0)) == true
			)
		) {
			if (this->_recvThread != NULL) {
				delete this->_recvThread;
			}
			this->_recvThread = new boost::thread([this]() {
				this->io_service.run();
			});
		}

	}
}
