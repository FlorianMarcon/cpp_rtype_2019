// /*
// ** EPITECH PROJECT, 2019
// ** cpp_rtype_2019
// ** File description:
// ** TCPClient.cpp
// */

#include "TCPClient.hpp"

namespace rtype {
	
	TCPClient::TCPClient(const std::string &address, unsigned int port) :
	_socket(this->io_service)
	{
     	_socket.connect(tcp::endpoint( boost::asio::ip::address::from_string(address), port));

	};

	TCPClient::~TCPClient()
	{
		std::cout << "Destruction" << std::endl;
		this->_socket.close();

	};

	void TCPClient::sendData(const char *buffer, ssize_t size)
	{
		const ssize_t count = this->_socket.write_some(boost::asio::buffer(buffer, size)); 
		
		if (count != size) {
			std::stringstream ss;

			ss << "TCPClient::Send should send" << size << " but send " << count;
			throw std::length_error(ss.str());
		}
	};

	TCPClient::ReceiveData TCPClient::receiveData()
	{
		ssize_t count;

		memset(this->_bufferRcv, 0, BUFFER_RCV_SIZE);
		count = this->_socket.read_some(boost::asio::buffer(this->_bufferRcv, BUFFER_RCV_SIZE));

		return(
			std::make_pair(this->_bufferRcv, count)
		);
	}
}