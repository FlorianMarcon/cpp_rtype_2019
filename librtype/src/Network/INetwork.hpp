/*
** EPITECH PROJECT, 2019
** librtype
** File description:
** INetwork
*/

#ifndef INETWORK_HPP_
#define INETWORK_HPP_

namespace rtype {

	class INetwork {
		public:
			virtual ~INetwork() = 0;

		protected:
		private:
	};
}

#endif /* !INETWORK_HPP_ */
