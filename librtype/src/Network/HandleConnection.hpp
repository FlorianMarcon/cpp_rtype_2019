/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** HandleConnection.hpp
*/

#ifndef HANDLE_CONNECTION_
#define HANDLE_CONNECTION_

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "Messages/IMessage.hpp"
using namespace boost::asio;
using ip::tcp;
using std::cout;
using std::endl;

class Handle : public boost::enable_shared_from_this<Handle>
{
	public:
		using ReceiveCallback = boost::function<void(Handle &handle, char *buffer, ssize_t size)>;
		using ConnectionClosedCallback = boost::function<void(Handle &handle)>;


		typedef boost::shared_ptr<Handle> pointer;
  		
		Handle(boost::asio::io_service& io_service): sock(io_service){};
		virtual ~Handle(){
			this->sock.close();
		};
		
		// Creating the pointer
  		static pointer createPointer(boost::asio::io_service& io_service);
		
		// Socket creation
  		tcp::socket& createSocket();

		// Launch Asynchronus Server
  		void startServer();

		// Handle reading 
  		virtual void handleRead(const boost::system::error_code& err, size_t bytes_transferred);

		// Handle writting
		void handleWrite(const boost::system::error_code& err, size_t bytes_transferred);


		void	setReceiveCallback(const ReceiveCallback &callback);
		void	setCloseCallback(const ConnectionClosedCallback &callback);

		void	write(char *buffer, ssize_t size);
		void	write(rtype::IMessage::SerializedObject &serializedObject);
		private:



  			tcp::socket sock;
  			std::string id;
  			enum { max_length = 1024 };
  			char data[max_length];

			ReceiveCallback	_callbackReceive;
			ConnectionClosedCallback	_callbackClosed;
};

#endif