/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** TCPClient.hpp
*/

#ifndef TCP_CLIENT_HEADER_
#define TCP_CLIENT_HEADER_

#include <iostream>
#include <boost/asio.hpp>
#include <exception>
// #include <B

using namespace boost::asio;
using ip::tcp;
using std::string;
using std::cout;
using std::endl;

namespace rtype {

	#define BUFFER_RCV_SIZE	4096

	class TCPClient 
	{
		public:
			using ReceiveData = std::pair<char *, ssize_t>;
		
			TCPClient(const std::string &address, unsigned int port);
			~TCPClient();

			void sendData(const char *buffer, ssize_t size);
			ReceiveData receiveData();
	
    		boost::system::error_code __error;

			private:
				boost::asio::io_service	io_service;
				tcp::socket _socket;

				char _bufferRcv[BUFFER_RCV_SIZE];
	};

}

#endif