/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** TCPServer.hpp
*/

#ifndef TCP_SERVER_
#define TCP_SERVER_
#include <iostream>
#include <boost/asio.hpp>
#include "HandleConnection.hpp"

using namespace boost::asio;
using ip::tcp;
using std::cout;
using std::endl;

namespace rtype {

	class TCPServer 
	{
		public:

		TCPServer(boost::asio::io_service& io_service, unsigned int port = 0): acceptor_(io_service), __ioService(io_service) {
			boost::asio::io_service netService;
			boost::asio::ip::udp::resolver resolver(netService);
			boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(), "google.com", "");
			boost::asio::ip::udp::resolver::iterator endpoints = resolver.resolve(query);
			boost::asio::ip::udp::endpoint ep = *endpoints;
			boost::asio::ip::udp::socket socket(netService);
			socket.connect(ep);
			boost::asio::ip::tcp::endpoint local_endpoint(socket.local_endpoint().address(), socket.local_endpoint().port());
			socket.close();
			local_endpoint.port(port);

			this->acceptor_ = tcp::acceptor(this->__ioService, local_endpoint);
			cout << "TCP Server address	: " << this->acceptor_.local_endpoint().address().to_string() << endl;
			cout << "TCP Server port	: " << this->acceptor_.local_endpoint().port() << endl;
		};
	
		virtual void handleAccept(Handle::pointer connection, const boost::system::error_code& err);
		void startServeur();
		void stopServeur();

		boost::asio::ip::tcp::endpoint	getLocalEndPoint() const;

		tcp::acceptor acceptor_;
		boost::asio::io_service& __ioService;

		private:

	};
}

#endif