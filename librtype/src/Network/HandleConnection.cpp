/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** HandleConnection.cpp
*/

#include "HandleConnection.hpp"

Handle::pointer Handle::createPointer(boost::asio::io_service& io_service)
{
	return pointer(new Handle(io_service));
}

tcp::socket& Handle::createSocket()
{
	return this->sock;
}

void Handle::startServer()
{

	memset(this->data, 0, max_length);
	sock.async_read_some(boost::asio::buffer(data, max_length), boost::bind(&Handle::handleRead, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	// sock.async_write_some(boost::asio::buffer(message, max_length), boost::bind(&Handle::handleWrite, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void Handle::handleRead(const boost::system::error_code& err, size_t bytes_transferred)
{
		
	if (err == boost::asio::error::eof || err == boost::asio::error::connection_reset) {
		this->_callbackClosed(*this);
		return;
	}
	if (err.value() == 0) {
		this->_callbackReceive(*this, this->data, bytes_transferred);
    } else {
       	std::cerr << "error: " << err.message() << endl;
 	}
	this->startServer();
}

void Handle::handleWrite(const boost::system::error_code& err, size_t bytes_transferred)
{
	(void)bytes_transferred;
	if (err == boost::asio::error::eof || err == boost::asio::error::connection_reset) {
		this->_callbackClosed(*this);
		return;
	}
	if (err.value() != 0) {
       	std::cerr << "error: " << err.message() << endl;
 	}
}
  
void	Handle::setReceiveCallback(const Handle::ReceiveCallback &receive)
{
	this->_callbackReceive = receive;
}

void	Handle::setCloseCallback(const Handle::ConnectionClosedCallback &closed)
{
	this->_callbackClosed = closed;
}

void	Handle::write(char *buffer, ssize_t size)
{
	this->sock.write_some(
		boost::asio::buffer(buffer, size)
	);
}
void	Handle::write(rtype::IMessage::SerializedObject &serializedObject)
{
	this->sock.write_some(
		boost::asio::buffer(std::get<0>(serializedObject), std::get<1>(serializedObject))
	);
}