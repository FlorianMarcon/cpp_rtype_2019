/*
** EPITECH PROJECT, 2019
** librtype
** File description:
** UDP
*/

#ifndef UDP_HPP_
#define UDP_HPP_

#include <memory>
#include <string>
#include <tuple>
#include <boost/asio.hpp>
#include <boost/function.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ip/udp.hpp>
#include "Errors/NetworkException.hpp"

namespace rtype {

	using UDPSocket = boost::asio::ip::udp::socket;
	using UDPEndPoint = boost::asio::ip::udp::endpoint;
	using BUDP = boost::asio::ip::udp;

	using NetworkBuffer = std::pair<char *, int>;

	#define BUFFER_SIZE 4096

	class UDP {
		public:
			using UDPReceiveCallback = boost::function<void(UDP &udp, char *buffer, ssize_t size, UDPEndPoint &recvendPoint)>;
			using UDPConnectionClosedCallback = boost::function<void(UDP &udp)>;

			/**
			 * @brief Construct a new UDP object
			 * 
			 * Object is created with an available port
			 */
			UDP(unsigned int port = 0);
		
			/**
			 * @brief Construct a new UDP object
			 * 
			 * @param endPoint EndPoint of the object
			 */
			UDP(const UDPEndPoint	&endPoint);

			~UDP();

			/**
			 * @brief Add a receiver
			 * 
			 * @param receiver End point of the receiver
			 */
			void	addReceiver(const UDPEndPoint &receiver);

			/**
			 * @brief Delete a receiver
			 * 
			 * @param endPoint EndPoint to delete
			 */
			void	removeReceiver(const UDPEndPoint &endPoint);

			/**
			 * @brief Get the Receivers object
			 * 
			 * @return std::list<UDPEndPoint> Receiver list
			 */
			std::list<UDPEndPoint>	getReceivers() const;

			/**
			 * @brief Get the End Point object
			 * 
			 * @return UDPEndPoint Endpoint
			 */
			UDPEndPoint	getEndPoint() const;

			/**
			 * @brief Send message asynchrounously
			 * 
			 * @param buffer Message
			 * @param size Message size
			 */
			void	asyncSend(const char *buffer, ssize_t size);
			
			/**
			 * @brief Start thread receiver
			 */
			void	startReceive();

			/**
			 * @brief Set the Receive Callback object
			 * 
			 * @param callback Callback
			 */
			void	setReceiveCallback(UDPReceiveCallback callback);

		protected:
			/**
			 * @brief Handle async receive
			 * 
			 * @param error Error code
			 * @param bytes_transferred Bytes quantity 
			 */
			void	handleReceive(const boost::system::error_code& error, std::size_t bytes_transferred);
			void	handleSend(const UDPEndPoint &endPoint, const boost::system::error_code &ec);
			
		private:
			/**
			 * @brief Receive data asynchronously
			 * 
			 */
			void	asyncReceive();

			boost::asio::io_service	io_service;	/* <! Service */

			UDPSocket	_socket;	/* <! Socket */

			std::list<UDPEndPoint>	_receiver;	/* <! List of receiver */

			char _buffer[BUFFER_SIZE] = {0};
			// boost::array<char, 4096>	_recvBuffer;	/* <! use to receive data */
			UDPEndPoint					_recvEndPoint;	/* <! use to receive data */
			boost::thread				*_recvThread = NULL;

			UDPReceiveCallback			_receiveCallback;	/* <! Callback when a data is received */
			UDPConnectionClosedCallback	_connectionClosedCallback;
			
			UDPEndPoint					_localEndpoint;

	};

}

#endif /* !UDP_HPP_ */
