/*
** EPITECH PROJECT, 2019
** libbabel
** File description:
** NetworkException
*/

#ifndef NETWORKEXCEPTION_HPP_
#define NETWORKEXCEPTION_HPP_

#include <exception>
#include <string>
#include "BasicException.hpp"

namespace rtype {

	class NetworkException : public BasicException {

		public:
			NetworkException(const std::string &errorMessage, const std::string &whereMessage) :
			BasicException("NetworkException: " + errorMessage, whereMessage) 
			{}

			~NetworkException() {};

		protected:
		private:

	};
	
} // namespace rtype


#endif /* !NETWORKEXCEPTION_HPP_ */
