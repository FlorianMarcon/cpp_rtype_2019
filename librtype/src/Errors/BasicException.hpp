/*
** EPITECH PROJECT, 2019
** libbabel
** File description:
** BasicException
*/

#ifndef BASICEXCEPTION_HPP_
#define BASICEXCEPTION_HPP_

#include <exception>
#include <string>

namespace rtype {
	#define POSITION std::string(__FILE__ ":") + std::to_string(__LINE__)

	class BasicException : public std::exception {

		public:
			BasicException(const std::string &errorMessage = "Unknown", const std::string &whereMessage = "Unknown") :
			_errorMessage(errorMessage),
			_whereMessage(whereMessage) {}
			~BasicException() {};

			virtual const char* what() const throw () {
				return _errorMessage.c_str();
			}
			virtual const char* where() const throw () {
				return _whereMessage.c_str();
			}
		protected:
		private:
			std::string _errorMessage;
			std::string _whereMessage;

	};
	
} // namespace rtype


#endif /* !BasicException_HPP_ */
