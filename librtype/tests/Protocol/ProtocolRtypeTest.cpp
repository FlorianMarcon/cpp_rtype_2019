#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include "Protocol/ProtocolRtype.hpp"
#include "Messages/GenericMessage.hpp"
#include "Messages/IMessage.hpp"

namespace rtype {

	BOOST_AUTO_TEST_CASE(ProtocolRtypeStartingInPacket)
	{
		GenericMessage *obj = ProtocolRtype::generatePacketStartingIn(0x42, 1);
		IMessage::SerializedObject serializedObject;
		GenericMessage msg;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(msg.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(msg.getIdMessage(), ProtocolRtype::IDPacket::STARTING_IN);
		serializedObject = msg.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::SECONDS)),
			std::to_string(0x42)
		);
		delete obj;
	}

	BOOST_AUTO_TEST_CASE(ProtocolRtypePositionPacket)
	{
		GenericMessage *obj = ProtocolRtype::generatePacketPosition("idid", 3, 4);
		IMessage::SerializedObject serializedObject;
		GenericMessage msg;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(msg.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(msg.getIdMessage(), ProtocolRtype::IDPacket::POSITIONS);
		serializedObject = msg.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::X)),
			std::to_string(3)
		);
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::Y)),
			std::to_string(4)
		);
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID)),
			"idid"
		);
		delete obj;
	}
	BOOST_AUTO_TEST_CASE(ProtocolRtypeMissilePacket)
	{
		GenericMessage *obj = ProtocolRtype::generatePacketMissile(42, 1, 0, 0);
		IMessage::SerializedObject serializedObject;
		GenericMessage msg;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(msg.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(msg.getIdMessage(), ProtocolRtype::IDPacket::MISSILE);
		serializedObject = msg.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::X)),
			std::to_string(0)
		);
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::Y)),
			std::to_string(0)
		);
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID)),
			std::to_string(42)
		);
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::POWER)),
			std::to_string(1)
		);
		delete obj;
	}

	BOOST_AUTO_TEST_CASE(ProtocolRtypeEnemyPositionPacket)
	{
		GenericMessage *obj = ProtocolRtype::generatePacketEnemyPosition("idid", "type", 3, 4);
		IMessage::SerializedObject serializedObject;
		GenericMessage msg;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(msg.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(msg.getIdMessage(), ProtocolRtype::IDPacket::ENEMY_POSITIONS);
		serializedObject = msg.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::X)),
			std::to_string(3)
		);
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::Y)),
			std::to_string(4)
		);
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID)),
			"idid"
		);

		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::TYPE)),
			"type"
		);
		delete obj;
	}

	BOOST_AUTO_TEST_CASE(ProtocolRtypeDeathPacket)
	{
		GenericMessage *obj = ProtocolRtype::generatePacketDeath("idid");
		IMessage::SerializedObject serializedObject;
		GenericMessage msg;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(msg.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(msg.getIdMessage(), ProtocolRtype::IDPacket::DEATH);
		serializedObject = msg.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID)),
			"idid"
		);
		delete obj;
	}
	BOOST_AUTO_TEST_CASE(generateLifePacket)
	{
		GenericMessage *obj = ProtocolRtype::generateLifePacket("idid", 100);
		IMessage::SerializedObject serializedObject;
		GenericMessage msg;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(msg.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(msg.getIdMessage(), ProtocolRtype::IDPacket::LIFE);
		serializedObject = msg.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID)),
			"idid"
		);
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::LIFE)),
			"100"
		);
		delete obj;
	}

	BOOST_AUTO_TEST_CASE(generateDisconnectionPacket)
	{
		GenericMessage *obj = ProtocolRtype::generateDisconnectionPacket("idid");
		IMessage::SerializedObject serializedObject;
		GenericMessage msg;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(msg.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(msg.getIdMessage(), ProtocolRtype::IDPacket::DISCONNECTION);
		serializedObject = msg.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(ProtocolRtype::MetadataFields::toString(ProtocolRtype::MetadataFields::ID)),
			"idid"
		);
		delete obj;
	}
}