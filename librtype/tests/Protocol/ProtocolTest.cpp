#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include "Protocol/Protocol.hpp"
#include "Messages/GenericMessage.hpp"
#include "Messages/IMessage.hpp"

namespace rtype {
	BOOST_AUTO_TEST_CASE(ProtocolFieldEnum) {
		BOOST_CHECK_EQUAL(
			Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::SETUP_NAME),
			"SETUP_NAME"
		);

		BOOST_CHECK_EQUAL(
			Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::STATUS_CODE),
			"STATUS_CODE"
		);
	};

	BOOST_AUTO_TEST_CASE(ProtocolGenerateRequest)
	{
		GenericMessage *request = Protocol::generateRequest();

		BOOST_CHECK_EQUAL(request->getIdMessage(), Protocol::IDMessage::REQUEST);
		BOOST_CHECK_NO_THROW(delete request);
	}

	BOOST_AUTO_TEST_CASE(ProtocolGenerateResponse)
	{
		GenericMessage *obj = Protocol::generateResponse(OK, "un petit message");
		IMessage::SerializedObject serializedObject;

		GenericMessage response;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(response.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(response.getIdMessage(), Protocol::IDMessage::RESPONSE);
		serializedObject = response.getBodyUnserialized();
		BOOST_CHECK_NO_THROW(metadata.unserialize(response.getBodyUnserialized()));
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::STATUS_CODE)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::STATUS_CODE)), "200");
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::MESSAGE)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::MESSAGE)), "un petit message");
	}

	BOOST_AUTO_TEST_CASE(ProtocolGenerateSetupName)
	{
		GenericMessage *obj = Protocol::generateSetNameRequest("Kirikou");
		IMessage::SerializedObject serializedObject;
		GenericMessage request;
		GenericMessage requestName;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(request.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(request.getIdMessage(), Protocol::IDMessage::REQUEST);
		serializedObject = request.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(requestName.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(requestName.getIdMessage(), Protocol::IDRequest::SETUP_NAME);
		serializedObject = requestName.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::SETUP_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::SETUP_NAME)), "Kirikou");
	}

	BOOST_AUTO_TEST_CASE(ProtocolGenerateListGamesRequest)
	{
		GenericMessage *obj = Protocol::generateListGamesRequest();
		IMessage::SerializedObject serializedObject;
		GenericMessage request;
		GenericMessage requestListGames;

		BOOST_CHECK_NO_THROW(request.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(request.getIdMessage(), Protocol::IDMessage::REQUEST);
		serializedObject = request.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(requestListGames.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(requestListGames.getIdMessage(), Protocol::IDRequest::LIST_GAMES);
		delete obj;
	}
	BOOST_AUTO_TEST_CASE(ProtocolGenerateListGamesresponse)
	{
		std::list<std::string> games = {"rtype", "mario"};
		GenericMessage *obj = Protocol::generateListGamesResponses(games);
		IMessage::SerializedObject serializedObject;
		GenericMessage response;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(response.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(response.getIdMessage(), Protocol::IDMessage::RESPONSE);
		serializedObject = response.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::LIST_GAMES)),
			"rtype,mario,"
		);
		delete obj;
	}
	BOOST_AUTO_TEST_CASE(ProtocolGenerateListPlayersRequest)
	{
		GenericMessage *obj = Protocol::generateListPlayerRequest();
		IMessage::SerializedObject serializedObject;
		GenericMessage request;
		GenericMessage requestListGames;

		BOOST_CHECK_NO_THROW(request.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(request.getIdMessage(), Protocol::IDMessage::REQUEST);
		serializedObject = request.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(requestListGames.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(requestListGames.getIdMessage(), Protocol::IDRequest::LIST_PLAYERS);
		delete obj;
	}
	BOOST_AUTO_TEST_CASE(ProtocolGenerateListPlayersresponse)
	{
		std::list<std::string> players = {"paul", "marc", "mathieu", "florian"};
		GenericMessage *obj = Protocol::generateListPlayerResponses(players);
		IMessage::SerializedObject serializedObject;
		GenericMessage response;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(response.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(response.getIdMessage(), Protocol::IDMessage::RESPONSE);
		serializedObject = response.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::LIST_PLAYERS)),
			"paul,marc,mathieu,florian,"
		);
		delete obj;
	}

	BOOST_AUTO_TEST_CASE(ProtocolListRoomRequest)
	{
		GenericMessage *obj = Protocol::generateListRoomRequest("Rtype");
		IMessage::SerializedObject serializedObject;
		GenericMessage request;
		GenericMessage requestName;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(request.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(request.getIdMessage(), Protocol::IDMessage::REQUEST);
		serializedObject = request.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(requestName.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(requestName.getIdMessage(), Protocol::IDRequest::LIST_ROOMS);
		serializedObject = requestName.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::GAME_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::GAME_NAME)), "Rtype");
	}
	BOOST_AUTO_TEST_CASE(ProtocolCreateRoomRequest)
	{
		GenericMessage *obj = Protocol::generateCreateRoomRequest("Rtype", "red-room");
		IMessage::SerializedObject serializedObject;
		GenericMessage request;
		GenericMessage requestName;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(request.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(request.getIdMessage(), Protocol::IDMessage::REQUEST);
		serializedObject = request.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(requestName.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(requestName.getIdMessage(), Protocol::IDRequest::CREATE_ROOM);
		serializedObject = requestName.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::GAME_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::GAME_NAME)), "Rtype");
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::ROOM_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::Fields::ROOM_NAME)), "red-room");
	}
	BOOST_AUTO_TEST_CASE(ProtocolGenerateListRoomsresponse)
	{
		std::list<std::string> rooms = {"red-room", "epitech", "citycity"};
		GenericMessage *obj = Protocol::generateListRoomResponses(rooms);
		IMessage::SerializedObject serializedObject;
		GenericMessage response;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(response.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(response.getIdMessage(), Protocol::IDMessage::RESPONSE);
		serializedObject = response.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(metadata.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::LIST_ROOMS)),
			"red-room,epitech,citycity,"
		);
		delete obj;
	}
	BOOST_AUTO_TEST_CASE(ProtocolGenerateListPlayersInRoomRequest)
	{
		GenericMessage *obj = Protocol::generateListPlayerInRoomRequest("rtype", "red-room");
		IMessage::SerializedObject serializedObject;
		GenericMessage request;
		GenericMessage requestListPlayersInRoom;
		MetadataMessage metadata;


		BOOST_CHECK_NO_THROW(request.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(request.getIdMessage(), Protocol::IDMessage::REQUEST);
		serializedObject = request.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(requestListPlayersInRoom.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(requestListPlayersInRoom.getIdMessage(), Protocol::IDRequest::LIST_PLAYERS_IN_ROOM);
		
		BOOST_CHECK_NO_THROW(metadata.unserialize(requestListPlayersInRoom.getBodyUnserialized()));
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME)), 
		"rtype");
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME)), 
		"red-room");

		delete obj;
	}
	BOOST_AUTO_TEST_CASE(ProtocolGoInRoomRequest)
	{
		GenericMessage *obj = Protocol::generateGoInRoomRequest("rtype", "red-room", "localhost", 42);
		IMessage::SerializedObject serializedObject;
		GenericMessage request;
		GenericMessage requestListPlayersInRoom;
		MetadataMessage metadata;


		BOOST_CHECK_NO_THROW(request.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(request.getIdMessage(), Protocol::IDMessage::REQUEST);
		serializedObject = request.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(requestListPlayersInRoom.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(requestListPlayersInRoom.getIdMessage(), Protocol::IDRequest::CONNECT_ROOM);

		BOOST_CHECK_NO_THROW(metadata.unserialize(requestListPlayersInRoom.getBodyUnserialized()));
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME)), 
		"rtype");
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME)), 
		"red-room");
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ADDRESS)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ADDRESS)), 
		"localhost");
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::PORT)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::PORT)), 
		"42");

		delete obj;
	}

	BOOST_AUTO_TEST_CASE(ProtocolGoInRoomResponse)
	{
		GenericMessage *obj = Protocol::generateGoInRoomResponses("127.0.0.1", 4242);
		IMessage::SerializedObject serializedObject;
		GenericMessage response;
		MetadataMessage metadata;

		BOOST_CHECK_NO_THROW(response.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(response.getIdMessage(), Protocol::IDMessage::RESPONSE);

		BOOST_CHECK_NO_THROW(metadata.unserialize(response.getBodyUnserialized()));
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ADDRESS)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ADDRESS)), 
			"127.0.0.1"
		);
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::PORT)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::PORT)), 
			"4242"
		);

		delete obj;
	}

	BOOST_AUTO_TEST_CASE(ProtocolStartGameRequest)
	{
		GenericMessage *obj = Protocol::generateStartGameRequest("rtype", "red-room");
		IMessage::SerializedObject serializedObject;
		GenericMessage request;
		GenericMessage requestListPlayersInRoom;
		MetadataMessage metadata;


		BOOST_CHECK_NO_THROW(request.unserialize(obj->serialize()));
		BOOST_CHECK_EQUAL(request.getIdMessage(), Protocol::IDMessage::REQUEST);
		serializedObject = request.getBodyUnserialized();

		BOOST_CHECK_NO_THROW(requestListPlayersInRoom.unserialize(serializedObject));
		BOOST_CHECK_EQUAL(requestListPlayersInRoom.getIdMessage(), Protocol::IDRequest::START_GAME);
		
		BOOST_CHECK_NO_THROW(metadata.unserialize(requestListPlayersInRoom.getBodyUnserialized()));
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::GAME_NAME)), 
		"rtype");
		BOOST_CHECK_NO_THROW(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME)));
		BOOST_CHECK_EQUAL(metadata.getData(Protocol::MetadataFields::toString(Protocol::MetadataFields::ROOM_NAME)), 
		"red-room");
		delete obj;
	}
}