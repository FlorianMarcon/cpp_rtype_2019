#define BOOST_TEST_MODULE librtype      // Nom du module de test
#include <iostream>
#include <boost/test/unit_test.hpp>
#include "Protocol/Protocol.hpp"
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include "Messages/GenericMessage.hpp"
#include "Messages/IMessage.hpp"

namespace rtype {
	BOOST_AUTO_TEST_CASE(GenericMessageConstruction) {
		GenericMessage msg;
		GenericMessage msg2;

		msg.setIdMessage(Protocol::IDMessage::REQUEST);
		msg.setBody(msg2);

		BOOST_CHECK_EQUAL(msg.getIdMessage(), Protocol::IDMessage::REQUEST);
		BOOST_CHECK_EQUAL(msg.getBody(), &msg2);
	}

	BOOST_AUTO_TEST_CASE(GenericMessageSerialize) {
		GenericMessage msg;
		GenericMessage msg2;
		char msg2b[] = {Protocol::IDMessage::REQUEST, 0x00, 0x00, 0x00, 0x00};
		char msg1b[] = {
			Protocol::IDMessage::REQUEST,
			0x05, 0x00, 0x00, 0x00,
			Protocol::IDMessage::REQUEST,
			0x00, 0x00, 0x00, 0x00
		};
		
		msg.setIdMessage(Protocol::IDMessage::REQUEST);
		msg2.setIdMessage(Protocol::IDMessage::REQUEST);
		msg.setBody(msg2);
		
		IMessage::SerializedObject t = msg2.serialize();
		BOOST_CHECK_EQUAL(std::get<1>(t), 5);
		for (unsigned int i = 0; i < 5; i++)
			BOOST_CHECK_EQUAL(std::get<0>(t)[i], msg2b[i]);

		t = msg.serialize();
		BOOST_CHECK_EQUAL(std::get<1>(t), 10);
		for (unsigned int i = 0; i < 10; i++) {
			BOOST_CHECK_EQUAL(std::get<0>(t)[i], msg1b[i]);
		}
		
	}

	BOOST_AUTO_TEST_CASE(GenericMessageUnserialize) {
		char msgb[] = {Protocol::IDMessage::REQUEST, 0x00, 0x00, 0x00, 0x00};
		GenericMessage msg;

		msg.unserialize(msgb, 5);

		BOOST_CHECK_EQUAL(msg.getIdMessage(), Protocol::IDMessage::REQUEST);
		BOOST_CHECK_EQUAL(msg.getBody(), nullptr);

		BOOST_CHECK_THROW(msg.unserialize(msgb, 1), std::length_error);
	}

	BOOST_AUTO_TEST_CASE(RequestID) {
		BOOST_CHECK_EQUAL(Protocol::IDRequest::SETUP_NAME, 0x01);
		BOOST_CHECK_EQUAL(Protocol::IDRequest::LIST_GAMES, 0x02);
		BOOST_CHECK_EQUAL(Protocol::IDRequest::LIST_ROOMS, 0x03);
		BOOST_CHECK_EQUAL(Protocol::IDRequest::CREATE_ROOM, 0x04);
		BOOST_CHECK_EQUAL(Protocol::IDRequest::CONNECT_ROOM, 0x05);
		BOOST_CHECK_EQUAL(Protocol::IDRequest::DISCONNECT_ROOM, 0x06);
		BOOST_CHECK_EQUAL(Protocol::IDRequest::DISCONNECT_SERVER, 0x07);
		BOOST_CHECK_EQUAL(Protocol::IDRequest::LIST_PLAYERS_IN_ROOM, 0x08);
		BOOST_CHECK_EQUAL(Protocol::IDRequest::LIST_PLAYERS, 0x09);
	}

	BOOST_AUTO_TEST_CASE(GenericMessageId) {
		BOOST_CHECK_EQUAL(Protocol::IDMessage::REQUEST, 0x01);
		BOOST_CHECK_EQUAL(Protocol::IDMessage::RESPONSE, 0x02);
		
	}
	
}