#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include "Messages/MetadataMessage.hpp"
#include "Messages/IMessage.hpp"

namespace rtype {
	
	BOOST_AUTO_TEST_CASE(MetadataMessageTest)
	{
		MetadataMessage m;
		std::string key = "key";
		std::string value = "value";
		std::string value2 = "value2";

		m << std::make_pair(key, value);

		BOOST_CHECK_EQUAL(m[key], value);
		BOOST_CHECK_EQUAL(m.getData(key), value);

		BOOST_CHECK_THROW(m.addData(key, value, false), std::logic_error);

		BOOST_CHECK_NO_THROW(m << std::make_pair(key, value2));
		BOOST_CHECK_EQUAL(m[key], value2);

		BOOST_CHECK_NO_THROW(m.removeData(key));
	}

	BOOST_AUTO_TEST_CASE(MetadataSerialize)
	{
		MetadataMessage m;
		IMessage::SerializedObject t;
		const char serial[] = {
			'g', 'a', 'm', 'e', ':', 'r', 't', 'y', 'p', 'e', '\r', '\n',
			'n', 'a', 'm', 'e', ':', 'F', 'l', 'o', 'r', 'i', 'a', 'n', '\r', '\n',
		};

		m << std::make_pair("game", "rtype");
		m << std::make_pair("name", "Florian");

		t = m.serialize();

		BOOST_CHECK_NE(std::get<0>(t), nullptr);
		BOOST_CHECK_NE(std::get<1>(t), 0);

		BOOST_CHECK_EQUAL(std::get<1>(t), 26);
		for (int i = 0; i < std::get<1>(t); i++)
			BOOST_CHECK_EQUAL(std::get<0>(t)[i], serial[i]);

	}

	BOOST_AUTO_TEST_CASE(MetadataUnserialize)
	{
		MetadataMessage m;
		const char serial[] = {
			'g', 'a', 'm', 'e', ':', 'r', 't', 'y', 'p', 'e', '\r', '\n',
			'n', 'a', 'm', 'e', ':', 'F', 'l', 'o', 'r', 'i', 'a', 'n', '\r', '\n', '\0'
		};
		unsigned int size = 26;
		BOOST_CHECK_NO_THROW(m.unserialize(serial, size));

		BOOST_CHECK_EQUAL(m["game"], "rtype");

		BOOST_CHECK_EQUAL(m["name"], "Florian");

		BOOST_CHECK_THROW(m.unserialize("adzadad", 5), std::logic_error);

	}
}