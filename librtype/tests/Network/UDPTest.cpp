#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include "Network/UDP.hpp"

namespace rtype {
	

	BOOST_AUTO_TEST_CASE(UDPTestReceiver)
	{
		UDP	udp;
		const UDPEndPoint e1(boost::asio::ip::address::from_string("127.0.0.1"), 42);
		const UDPEndPoint e2(boost::asio::ip::address::from_string("127.0.0.2"), 43);
		std::list<UDPEndPoint>	l;

		udp.addReceiver(e1);
		udp.addReceiver(e2);

		l = udp.getReceivers();

		BOOST_CHECK(l.size() == 2);
		BOOST_CHECK(l.front().port() == 42);
		BOOST_CHECK_EQUAL(l.front().address().to_string(), "127.0.0.1");
		BOOST_CHECK(l.back().port() == 43);
		BOOST_CHECK_EQUAL(l.back().address().to_string(), "127.0.0.2");
	}

	BOOST_AUTO_TEST_CASE(UDPTest)
	{
		UDP	receiver;
		UDP sender;
		UDP sender2;
		bool isReceived1 = false;
		bool isReceived2 = false;

		receiver.setReceiveCallback([&isReceived1, &isReceived2, &sender, &sender2](UDP &udp, char *buffer, ssize_t size, UDPEndPoint &recvEndPoint) {
			(void)udp;

			BOOST_CHECK_EQUAL(size, 5);
			BOOST_CHECK_EQUAL(buffer, "hello");
			BOOST_CHECK_EQUAL(sender.getEndPoint().port(), recvEndPoint.port());
			isReceived1 = true;
			udp.setReceiveCallback([&isReceived2, &sender2](UDP &udp, char *buffer, ssize_t size, UDPEndPoint &recvEndPoint){

				(void)udp;
				BOOST_CHECK_EQUAL(size, 14);
				BOOST_CHECK_EQUAL(buffer, "second message");
				BOOST_CHECK_EQUAL(sender2.getEndPoint().port(), recvEndPoint.port());
				isReceived2 = true;
			});
			udp.startReceive();
		});

		sender.addReceiver(receiver.getEndPoint());
		sender2.addReceiver(receiver.getEndPoint());
		sender2.removeReceiver(receiver.getEndPoint());
		sender2.addReceiver(receiver.getEndPoint()); 

		receiver.startReceive();

		sender.asyncSend("hello", 5);
		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
		BOOST_CHECK_EQUAL(isReceived1, true);

		//check no problem
		receiver.startReceive();

		sender2.asyncSend("second message", 14);
		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
		BOOST_CHECK_EQUAL(isReceived2, true);
	}




}