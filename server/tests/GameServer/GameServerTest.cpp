#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "Protocol/Protocol.hpp"
#include "Messages/GenericMessage.hpp"
#include "Messages/MetadataMessage.hpp"
#include "Network/TCPClient.hpp"
#include "GameServer/GameServer.hpp"
#include "GameInitializer/RtypeInitializer.hpp"
#include "Game/Rtype/RtypeRoom.hpp"

namespace server {
	BOOST_AUTO_TEST_CASE(GameServerTestSetupName) {
		boost::asio::io_service service;
		GameServer server(service);

		boost::thread *t = new boost::thread([&server, &service](){
			//Connect client
			rtype::TCPClient::ReceiveData rcvData;
			rtype::GenericMessage response;
			rtype::MetadataMessage responseMeta;
			rtype::GenericMessage *request;
			rtype::IMessage::SerializedObject serializedObject;

			//Connect client
			rtype::TCPClient *client = new rtype::TCPClient(server.getLocalEndPoint().address().to_string(), server.getLocalEndPoint().port());
			std::this_thread::sleep_for(std::chrono::milliseconds(500));

			//create request
			request = rtype::Protocol::generateSetNameRequest("Florian");
			BOOST_CHECK_NE(request, nullptr);
			serializedObject = request->serialize();

			//Send request ask name
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));
			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));

			//Disconnect client
			delete client;

			server.stopServeur();
			service.stop();
		

		});

		server.startServeur();
		t->join();
	}

	BOOST_AUTO_TEST_CASE(GameServerTestListGames) {
		boost::asio::io_service service;
		GameServer server(service);

		boost::thread *t = new boost::thread([&server, &service](){
			//Connect client
			rtype::TCPClient::ReceiveData rcvData;
			rtype::GenericMessage response;
			rtype::MetadataMessage responseMeta;
			rtype::GenericMessage *request;
			rtype::IMessage::SerializedObject serializedObject;
			
			//Connect client
			rtype::TCPClient *client = new rtype::TCPClient(server.getLocalEndPoint().address().to_string(), server.getLocalEndPoint().port());
			std::this_thread::sleep_for(std::chrono::milliseconds(500));

			//create request
			request = rtype::Protocol::generateListGamesRequest();
			serializedObject = request->serialize();

			//Send request ask name
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;

			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));
			const std::string games = RtypeRoom::getName() + std::string(",");
			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_GAMES)), games);

			//Disconnect client
			delete client;

			server.stopServeur();
			service.stop();
		

		});

		server.startServeur();
		t->join();
	}

	BOOST_AUTO_TEST_CASE(GameServerTestListPlayers) {
		boost::asio::io_service service;
		GameServer server(service);

		boost::thread *t = new boost::thread([&server, &service](){
			//Connect client
			rtype::TCPClient::ReceiveData rcvData;
			rtype::GenericMessage response;
			rtype::MetadataMessage responseMeta;
			rtype::GenericMessage *request;
			rtype::IMessage::SerializedObject serializedObject;
			
			//Connect client
			rtype::TCPClient *client = new rtype::TCPClient(server.getLocalEndPoint().address().to_string(), server.getLocalEndPoint().port());
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
			rtype::TCPClient *other = new rtype::TCPClient(server.getLocalEndPoint().address().to_string(), server.getLocalEndPoint().port());
			std::this_thread::sleep_for(std::chrono::milliseconds(500));

			//set other's name
			request = rtype::Protocol::generateSetNameRequest("OtHeRPlayer");
			serializedObject = request->serialize();
			other->sendData(serializedObject.first, serializedObject.second);
			delete request;

			//create request
			request = rtype::Protocol::generateListPlayerRequest();
			serializedObject = request->serialize();

			//Send request ask list players
			client->sendData(serializedObject.first, serializedObject.second);
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));
			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_PLAYERS)), 
				"OtHeRPlayer,"
			);

			//Disconnect other client
			delete other;

			//Send request ask list players
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));
			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_PLAYERS)), 
			""
			);

			//Disconnect client
			delete client;

			server.stopServeur();
			service.stop();
		

		});

		server.startServeur();
		t->join();
	}
	
	BOOST_AUTO_TEST_CASE(GameServerTestListRooms) {
		boost::asio::io_service service;
		GameServer server(service);

		boost::thread *t = new boost::thread([&server, &service](){
			//Connect client
			rtype::TCPClient::ReceiveData rcvData;
			rtype::GenericMessage response;
			rtype::MetadataMessage responseMeta;
			rtype::GenericMessage *request;
			rtype::IMessage::SerializedObject serializedObject;
			
			//Connect client
			rtype::TCPClient *client = new rtype::TCPClient(server.getLocalEndPoint().address().to_string(), server.getLocalEndPoint().port());
			std::this_thread::sleep_for(std::chrono::milliseconds(500));

			//create request
			request = rtype::Protocol::generateCreateRoomRequest(RtypeRoom::getName(), "red-room");
			serializedObject = request->serialize();

			//Send request create room
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));
			
		
			//create request
			request = rtype::Protocol::generateCreateRoomRequest("NO exist", "red-room");
			serializedObject = request->serialize();

			//Send request create room
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::NOT_FOUND));
			
			//create request
			request = rtype::Protocol::generateListRoomRequest(RtypeRoom::getName());
			serializedObject = request->serialize();

			//Send request create room
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));
						
			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_ROOMS)), 
				"red-room,"
			);

			//Disconnect client
			delete client;

			server.stopServeur();
			service.stop();
		

		});

		server.startServeur();
		t->join();
	}
	
	BOOST_AUTO_TEST_CASE(GameServerTestListPlayersInRoom) {
		boost::asio::io_service service;
		GameServer server(service);

		boost::thread *t = new boost::thread([&server, &service](){
			//Connect client
			rtype::TCPClient::ReceiveData rcvData;
			rtype::GenericMessage response;
			rtype::MetadataMessage responseMeta;
			rtype::GenericMessage *request;
			rtype::IMessage::SerializedObject serializedObject;
			
			//Connect client
			rtype::TCPClient *client = new rtype::TCPClient(server.getLocalEndPoint().address().to_string(), server.getLocalEndPoint().port());
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
			rtype::TCPClient *other = new rtype::TCPClient(server.getLocalEndPoint().address().to_string(), server.getLocalEndPoint().port());
			std::this_thread::sleep_for(std::chrono::milliseconds(500));

			//set other's name
			request = rtype::Protocol::generateSetNameRequest("Karibou");
			serializedObject = request->serialize();
			other->sendData(serializedObject.first, serializedObject.second);
			other->receiveData();
			delete request;


			//create request
			request = rtype::Protocol::generateCreateRoomRequest(RtypeRoom::getName(), "red-room");
			serializedObject = request->serialize();

			//Send request ask to create a room
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));

	
			//create request
			request = rtype::Protocol::generateStartGameRequest(RtypeRoom::getName(), "red-room");
			serializedObject = request->serialize();

			//Send request ask to create a room
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::UNAUTHORIZED));


			//create request
			request = rtype::Protocol::generateGoInRoomRequest(RtypeRoom::getName(), "red-room", "127.0.0.1", 42);
			serializedObject = request->serialize();

			//Send request ask to go in a room
			other->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = other->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));
			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::ADDRESS)));
			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::PORT)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));


			//create request
			request = rtype::Protocol::generateListPlayerInRoomRequest(RtypeRoom::getName(), "red-room");
			serializedObject = request->serialize();

			//Send request ask list players in room
			client->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = client->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));
			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::LIST_PLAYERS)), 
				"Karibou,"
			);

			//create request
			request = rtype::Protocol::generateStartGameRequest(RtypeRoom::getName(), "red-room");
			serializedObject = request->serialize();

			//Send request ask to create a room
			other->sendData(serializedObject.first, serializedObject.second);
			delete request;
			
			//Wait to receive data
			rcvData = other->receiveData();
			BOOST_CHECK_NO_THROW(response.unserialize(std::get<0>(rcvData), std::get<1>(rcvData)));
			BOOST_CHECK_EQUAL(response.getIdMessage(), rtype::Protocol::RESPONSE);
			BOOST_CHECK_NO_THROW(responseMeta.unserialize(response.getBodyUnserialized()));

			BOOST_CHECK_NO_THROW(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)));

			BOOST_CHECK_EQUAL(responseMeta.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::STATUS_CODE)), std::to_string(rtype::OK));


			//Disconnect client
			delete client;
			delete other;

			server.stopServeur();
			service.stop();
		

		});

		server.startServeur();
		t->join();
	}
}