#define BOOST_TEST_MODULE ServerTest      // Nom du module de test
#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>

#include "GameInitializer/RtypeInitializer.hpp"
#include "Game/Rtype/RtypeRoom.hpp"

namespace server {
	BOOST_AUTO_TEST_CASE(Server) {
		RtypeInitializer rtypeInitializer;
		IRoom	*room = nullptr;

		BOOST_CHECK_EQUAL(rtypeInitializer.getInformation("name"), "RTYPE");
		BOOST_CHECK_EQUAL(rtypeInitializer.getName(), "RTYPE");
		room = rtypeInitializer.createGame();
		BOOST_CHECK_NE(room, nullptr);
		BOOST_CHECK_NO_THROW(room->getIpAddress());
		BOOST_CHECK_NO_THROW(room->getPortAddress());
	}
}