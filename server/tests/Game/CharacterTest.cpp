#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include "Game/Rtype/Character.hpp"

namespace server {
	BOOST_AUTO_TEST_CASE(CharacterTest)
	{
		Character c(Character::PLAYER, 100);

		BOOST_CHECK_EQUAL(c.getType(), Character::PLAYER);
		BOOST_CHECK_EQUAL(c.getLifePoint(), (unsigned int)100);
	}
	
}