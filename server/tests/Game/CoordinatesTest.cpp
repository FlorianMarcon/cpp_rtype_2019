#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include "Game/Rtype/Coordinates.hpp"

namespace server {
	BOOST_AUTO_TEST_CASE(CoordinatesTest)
	{
		Coordinates c(1, 2);

		BOOST_CHECK_EQUAL(c.getX(), 1);
		BOOST_CHECK_EQUAL(c.getY(), 2);
		c.setX(42);
		c.setY(43);
		BOOST_CHECK_EQUAL(c.getX(), 42);
		BOOST_CHECK_EQUAL(c.getY(), 43);
	}
	
}