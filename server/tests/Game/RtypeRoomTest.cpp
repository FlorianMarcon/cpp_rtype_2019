#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include "Protocol/ProtocolRtype.hpp"
#include "Messages/GenericMessage.hpp"
#include "Network/UDP.hpp"

#include "GameInitializer/RtypeInitializer.hpp"
#include "Game/Rtype/RtypeRoom.hpp"

namespace server {
	BOOST_AUTO_TEST_CASE(RtypeRoomTest)
	{
		RtypeRoom *room;

		BOOST_CHECK_NO_THROW(room = new RtypeRoom());

		BOOST_CHECK_EQUAL(room->getGameName(), "RTYPE");
		BOOST_CHECK_EQUAL(room->getName(), "RTYPE");

		delete room;
	}
	BOOST_AUTO_TEST_CASE(RtypeRoomAddPlayerTest)
	{
		RtypeRoom *room;
		boost::asio::io_service io;
		Handle::pointer p = Handle::createPointer(io);
		Gamer gamer(p);
		rtype::UDP udp;
		
		gamer.setUDPEndPoint(udp.getEndPoint().address().to_string(), udp.getEndPoint().port());
		
		BOOST_CHECK_NO_THROW(room = new RtypeRoom());

		room->addPlayer(&gamer);
		BOOST_CHECK_EQUAL((int)room->getPlayers().size(), 1);
		delete room;
	}

	BOOST_AUTO_TEST_CASE(RtypeRoomGameTest)
	{
		RtypeRoom *room;
		boost::asio::io_service io;
		Handle::pointer p = Handle::createPointer(io);
		Gamer gamer(p);
		rtype::UDP udp(rtype::UDPEndPoint(
			boost::asio::ip::address::from_string("127.0.0.1")
			, 0
		));
		int i = 20;
		
		udp.setReceiveCallback([&i](rtype::UDP &u, char *buffer, ssize_t size, rtype::UDPEndPoint &recvendPoint) {
			rtype::GenericMessage message;
			rtype::MetadataMessage metadata;
			std::string time;
			BOOST_CHECK_NO_THROW(message.unserialize(buffer, size));
			BOOST_CHECK_EQUAL(message.getIdMessage(), rtype::ProtocolRtype::IDPacket::STARTING_IN);
			BOOST_CHECK_NO_THROW(metadata.unserialize(message.getBodyUnserialized()));
			BOOST_CHECK_NO_THROW(time = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::SECONDS)));
			BOOST_CHECK_EQUAL(std::to_string(i--), time);
			(void)size;
			(void)u;
			(void)recvendPoint;
			(void)buffer;
		});
		udp.startReceive();
		gamer.setUDPEndPoint(udp.getEndPoint().address().to_string(), udp.getEndPoint().port());
		BOOST_CHECK_NO_THROW(room = new RtypeRoom());
		room->addPlayer(&gamer);

		room->start();
		boost::this_thread::sleep_for(boost::chrono::seconds(2));
		delete room;
	}
}