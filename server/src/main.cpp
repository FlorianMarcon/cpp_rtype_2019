#include <iostream>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "GameServer/GameServer.hpp"

using namespace boost::asio;
using ip::tcp;
using std::cout;
using std::endl;

int main(int ac, char *av[])
{
	boost::program_options::options_description desc("Allowed options");
	boost::program_options::variables_map vm;
	unsigned int port = 0;

	desc.add_options()
    	("help", "Produce help message")
    	("port", boost::program_options::value<unsigned int>(),"Server port");

	boost::program_options::store(boost::program_options::parse_command_line(ac, av, desc), vm);

	if (vm.count("help")) {
		cout << desc << "\n";
		return 0;
	}
	if (vm.count("port")) {
		port = vm["port"].as<unsigned int>();
	}
	boost::program_options::notify(vm);

	try
	{
		vm.notify();
	}
	catch(const std::exception& e)
	{
		std::cerr << "Use --help" << std::endl;
		return (84);
	}

	try
	{
		boost::asio::io_service io_service;
		server::GameServer server(io_service, port);
		server.startServeur();
	}
	catch(std::exception& e)
	{
	std::cerr << e.what() << endl;
	}
	return 0;
}