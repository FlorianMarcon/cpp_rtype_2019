/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Character
*/

#include <random>
#include <chrono>
#include "Character.hpp"

namespace server
{
	
	Character::Character(Character::Type type, unsigned int lifePoint)
	{
		this->_type = type;
		this->_lifePoint = lifePoint;

		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

		std::minstd_rand0 generator (seed);  // minstd_rand0 is a standard linear_congruential_engine
		this->_id = generator();
	}

	Character::~Character()
	{
	}

	unsigned int	Character::getLifePoint() const
	{
		return (this->_lifePoint);
	}
	unsigned int	Character::getId() const
	{
		return (this->_id);
	}
	Character::Type			Character::getType() const
	{
		return (this->_type);
	}

	void	Character::decreaseLifePoint(unsigned int quantity)
	{
		if (this->_lifePoint < quantity)
			this->_lifePoint = 0;
		else
			this->_lifePoint -= quantity;
	}

	
} // namespace server

