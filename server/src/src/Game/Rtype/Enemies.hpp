/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Enemies
*/

#ifndef ENEMIES_HPP_
#define ENEMIES_HPP_

#include <chrono>
#include "Character.hpp"

namespace server
{
	class Enemies: public Character {
		public:
			Enemies();
			~Enemies();

			std::time_t	getLastTimeShoot() const;

			void	shoot();
		protected:
		private:

			std::time_t	_lastShoot = 0;
	};
} // namespace server


#endif /* !ENEMIES_HPP_ */
