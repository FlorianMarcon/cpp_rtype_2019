/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Coordinates
*/

#ifndef COORDINATES_HPP_
#define COORDINATES_HPP_

namespace server
{
	/**
	 * @brief Represent coordinates
	 * 
	 */
	class Coordinates {
		public:

			Coordinates(int x = 0, int y = 0);
			~Coordinates();


			/**
			 * @brief Set x coordinates
			 * 
			 * @param x 
			 */
			void	setX(int x);

			/**
			 * @brief Set y coordinates
			 * 
			 * @param y 
			 */
			void	setY(int y);

			/**
			 * @brief Get X coordinates
			 * 
			 * @return int 
			 */
			int		getX() const;

			/**
			 * @brief get Y coordinates
			 * 
			 * @return int 
			 */
			int		getY() const;
		protected:
		private:

			int _x;
			int _y;
	};

} // namespace server


#endif /* !COORDINATES_HPP_ */
