/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** RtypeRoom
*/

#ifndef RTYPEROOM_HPP_
#define RTYPEROOM_HPP_

#include "Game/IRoom.hpp"
#include "Gamer/Gamer.hpp"
#include "Network/UDP.hpp"
#include "Messages/IMessage.hpp"
#include "Lua/Lua.hpp"
#include <string>
#include <boost/thread.hpp>
#include <list>
#include <utility>
#include "Game/Rtype/Character.hpp"
#include "Game/Rtype/Enemies.hpp"
#include "Game/Rtype/Missile.hpp"

namespace server
{
	class RtypeRoom: public IRoom, public rtype::UDP {
		public:
			RtypeRoom();
			~RtypeRoom();

			/**
			 * @brief Get the Name object
			 * 
			 * @return std::string Name
			 */
			static std::string	getName();

			/**
			 * @brief Get the Game Name object
			 * 
			 * @return std::string Game Name
			 */
			std::string getGameName() const;

			/**
			 * @brief Get the Ip Address object of the room
			 * 
			 * @return std::string Ip address
			 */
			std::string	getIpAddress() const;

			/**
			 * @brief Get the Port Address object of the room
			 * 
			 * @return unsigned int Port
			 */
			unsigned int getPortAddress() const;

			/**
			 * @brief Get the Room Name object
			 * 
			 * @return std::string Room name
			 */
			std::string	getRoomName() const;

			/**
			 * @brief Set the Room Name object
			 * 
			 * @param name Name of the room
			 */
			void setRoomName(const std::string &name);

			/**
			 * @brief Add a player to the room
			 * 
			 * @param gamer Gamer to add
			 */
			void addPlayer(Gamer *gamer);

			/**
			 * @brief Get the Players list
			 * 
			 * @return std::list<Gamer *> List of the player
			 */
			std::list<Gamer *>	getPlayers() const;

			/**
			 * @brief Start the game
			 */
			void	start();

			/**
			 * @brief Run Game
			 * 
			 */
			void	runGame();

			/**
			 * @brief Loop sending counter before start
			 * 
			 */
			void	runWaitingStep(unsigned int step);

			/**
			 * @brief Function called to say that player is disconnected
			 * 
			 * @param gamer Gamer disconnected
			 */
			void	playerDisconnection(const Gamer &gamer);
		protected:

			Character	*findCharacter(Gamer *gamer) const;

			/**
			 * @brief Handle receive data
			 * 
			 * @param udp Udp receiving data
			 * @param buffer Buffer containing data
			 * @param size Size of the buffer
			 * @param recvendPoint Endpoint received data
			 */
			void	receive(UDP &udp, char *buffer, ssize_t size, rtype::UDPEndPoint &recvendPoint);

			void	handlePosition(Gamer *gamer, rtype::IMessage::SerializedObject serializedObject);
			void	handleMissile(Gamer *gamer, rtype::IMessage::SerializedObject serializedObject);
		

			void	updateEnemies();
			void	updateMissiles();
		private:
			std::string	_roomName = "";

			std::list<std::pair<Gamer *, Character*>> _gamers;

			boost::thread	*threadGame = nullptr;

			unsigned int	_step;

			std::list<Enemies *>	_enemies;

			std::list<Missile *>	_missiles;

			bool		_run = false;

			
			server::Lua		_lua;
			

	};
} // namespace server


#endif /* !RTYPEROOM_HPP_ */
