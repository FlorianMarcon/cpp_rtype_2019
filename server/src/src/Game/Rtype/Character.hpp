/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Character
*/

#ifndef CHARACTER_HPP_
#define CHARACTER_HPP_

#include "Coordinates.hpp"

namespace server
{
	/**
	 * @brief Reprensent all Character. Enemies and players
	 * 
	 */
	class Character: public Coordinates {
		public:

			/**
			 * @brief Represent type of character
			 * 
			 */
			enum Type {
				PLAYER,
				ENEMY
			};

			Character(Type type, unsigned int lifePoint = 0);
			~Character();

			/**
			 * @brief Get the Life Point object
			 * 
			 * @return unsigned int 
			 */
			unsigned int	getLifePoint() const;
			
			/**
			 * @brief Get the Type object
			 * 
			 * @return Type 
			 */
			Type			getType() const;

			/**
			 * @brief Get the Id object
			 * 
			 * @return unsigned int 
			 */
			unsigned int	getId() const;

			/**
			 * @brief Decrease character life point
			 * 
			 * @param quantity Quantity to remove
			 */
			void	decreaseLifePoint(unsigned int quantity);

		protected:
		private:

			unsigned int	_lifePoint;
			Type			_type;
			unsigned int	_id;
	};
	
} // namespace server


#endif /* !CHARACTER_HPP_ */
