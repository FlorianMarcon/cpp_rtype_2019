/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Missile
*/

#include <chrono>
#include <random>
#include <iostream>
#include "Missile.hpp"

namespace server
{
	Missile::Missile(unsigned int power, unsigned int x, unsigned int y, int vx, int vy):
	Coordinates(x, y),
	_power(power),
	_vx(vx),
	_vy(vy)
	{
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

		std::minstd_rand0 generator (seed);  // minstd_rand0 is a standard linear_congruential_engine
		this->_id = generator();
	}

	Missile::~Missile()
	{
	}


	unsigned int	Missile::getId() const
	{
		return (this->_id);
	}
	unsigned int	Missile::getPower() const
	{
		return (this->_power);
	}
	int	Missile::getVx() const
	{
		return (this->_vx);
	}
	int	Missile::getVy() const
	{
		return (this->_vy);
	}

	void	Missile::updatePosition()
	{
		this->setX(this->getX() + (this->_vx * this->_power * 2));
		this->setY(this->getY() + (this->_vy * this->_power));
		// std::cout << "New missile position [" << this->getId() << "] (" << this->getX() << ";" << this->getY() << ")" << std::endl;
	}
	
} // namespace server

