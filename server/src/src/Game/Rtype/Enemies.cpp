/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Enemies
*/

#include "Enemies.hpp"

namespace server
{
	Enemies::Enemies() :
	Character(ENEMY, 10)
	{

	}

	Enemies::~Enemies()
	{
	}

	void	Enemies::shoot()
	{
		this->_lastShoot = std::time(nullptr);
	}

	std::time_t		Enemies::getLastTimeShoot() const
	{
		return (std::time(nullptr) - this->_lastShoot);
	}
	
} // namespace server

