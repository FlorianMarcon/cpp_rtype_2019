/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Missile
*/

#ifndef MISSILE_HPP_
#define MISSILE_HPP_

#include "Game/Rtype/Coordinates.hpp"

namespace server
{
	class Missile: public Coordinates {
		public:
			Missile(unsigned int power, unsigned int x, unsigned int y, int vx, int vy);
			~Missile();

			unsigned int	getPower() const;

			unsigned int	getId() const;
			int	getVx() const;
			int	getVy() const;

			void	updatePosition();
		protected:
		private:

			unsigned int _power;
			int				_vx;
			int				_vy;
			unsigned int	_id;
	};
	
} // namespace server



#endif /* !MISSILE_HPP_ */
