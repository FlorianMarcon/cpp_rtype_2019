/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Coordinates
*/

#include "Coordinates.hpp"

namespace server
{
	Coordinates::Coordinates(int x, int y) :
	_x(x),
	_y(y)
	{
	}

	Coordinates::~Coordinates()
	{
	}

	void	Coordinates::setX(int x)
	{
		this->_x = x;
	}
	void	Coordinates::setY(int y)
	{
		this->_y = y;
	}
	int		Coordinates::getX() const
	{
		return (this->_x);
	}
	int		Coordinates::getY() const
	{
		return (this->_y);
	}

} // namespace server

