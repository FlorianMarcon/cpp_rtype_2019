/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** RtypeRoom
*/

#include "Protocol/ProtocolRtype.hpp"
#include "RtypeRoom.hpp"
#include <chrono>
#include <thread>

namespace server
{
	RtypeRoom::RtypeRoom() :
	_lua("../move.lua")
	{
		this->setReceiveCallback(boost::bind(&RtypeRoom::receive, this, _1, _2, _3, _4));
		std::srand(std::time(nullptr));
	}

	RtypeRoom::~RtypeRoom()
	{
		if (this->threadGame != nullptr) {
			this->threadGame->interrupt();
		}
	}

	std::string RtypeRoom::getName()
	{
		return ("RTYPE");
	}
	std::string	RtypeRoom::getGameName() const
	{
		return (this->getName());
	}

	std::string	RtypeRoom::getRoomName() const
	{
		return (this->_roomName);
	}

	void	RtypeRoom::setRoomName(const std::string &name)
	{
		this->_roomName = name;
	}

	std::string	RtypeRoom::getIpAddress() const
	{
		return (this->getEndPoint().address().to_string());
	}

	unsigned int	RtypeRoom::getPortAddress() const
	{
		return (this->getEndPoint().port());
	}

	void	RtypeRoom::addPlayer(Gamer *gamer)
	{
		if (gamer != NULL) {
			this->_gamers.push_back(std::make_pair(gamer, new Character(Character::PLAYER, 100)));
			this->addReceiver(gamer->getUDPEndPoint());
		}
	}


	std::list<Gamer *>	RtypeRoom::getPlayers() const
	{
		std::list<Gamer *> l;

		for(auto obj: this->_gamers) {
			l.push_back(obj.first);
		}
		return (l);
	}

	void	RtypeRoom::start()
	{
		if (this->threadGame != nullptr && this->threadGame->timed_join(boost::posix_time::seconds(0)) == true) {
			delete this->threadGame;
			this->threadGame = nullptr;
		}
		if (this->threadGame == nullptr) {
			this->threadGame = new boost::thread([this]() {
				try
				{
					this->_run = true;
					this->_step = 0;
					this->runGame();
				}
				catch(const boost::thread_interrupted &e)
				{
					return;
				}	
			});
			this->startReceive();
		}
	}

	void	RtypeRoom::runWaitingStep(unsigned int step)
	{
		rtype::GenericMessage *message;
		rtype::IMessage::SerializedObject serial;

		for (int i = 5; i >= 0 && this->_run; i--) {
			message = rtype::ProtocolRtype::generatePacketStartingIn(i, step);
			if (message != nullptr) {
				//std::cout << "Room "<< this->getName() << " start in " << i << std::endl;
				serial = message->serialize();
				this->asyncSend(serial.first, serial.second);
				delete message;
			}
			boost::this_thread::sleep_for(boost::chrono::milliseconds(1000));	
		}
	}

	void	RtypeRoom::playerDisconnection(const Gamer &gamer)
	{
		for (auto it = this->_gamers.begin(); it != this->_gamers.end();) {
			std::pair<Gamer *, Character *> p = *it;

			if (p.first == &gamer) {
				delete p.second;
				it = this->_gamers.erase(it);
			} else {
				it++;
			}
		}

		if (this->_gamers.size() <= 0)
			this->_run = false;
	}

	void	RtypeRoom::runGame()
	{
		while (this->_step != 1 && this->_run)
		{
			this->runWaitingStep(this->_step);
			for (Enemies *e : this->_enemies)
				delete e;
			for (Missile *e : this->_missiles)
				delete e;
			
			this->_enemies.clear();
			this->_missiles.clear();



			std::time_t start = std::time(nullptr);
			while ((std::time(nullptr) - start) < 60 && this->_run) {
				// std::cout << "sleep" << std::endl;
				std::this_thread::sleep_for(std::chrono::milliseconds(100));

				// std::cout << "updateEnemies" << std::endl;
				updateEnemies();
				// std::cout << "updateMissiles" << std::endl;
				updateMissiles();
			}
			this->_step++;
		}

		rtype::GenericMessage *message = rtype::ProtocolRtype::generateDisconnectionPacket("");
		if (message != nullptr) {
			rtype::IMessage::SerializedObject s = message->serialize();
			this->asyncSend(s.first, s.second);
			delete message;
		}
		std::cout << "End runGame" << std::endl;
	}

	void	RtypeRoom::receive(UDP &udp, char *buffer, ssize_t size, rtype::UDPEndPoint &rcvEndPoint)
	{
		//std::cout << "RtypeRoom receive a new UDP Packet from " << rcvEndPoint.address().to_string() << std::endl;
		rtype::GenericMessage message;
		std::list<Gamer *>::iterator p;
		(void)udp;

		p = std::find_if(this->getPlayers().begin(), this->getPlayers().end(), [rcvEndPoint](Gamer *gamer) {
			if (gamer == nullptr)
				return false;
			return (
				rcvEndPoint == gamer->getUDPEndPoint()
			);
		});

		if (p == this->getPlayers().end()) {
			std::cerr << "Receive a packet from unknow user : " << rcvEndPoint.address().to_string() << std::endl;
			return;
		}

		try
		{
			message.unserialize(buffer, size);
			// std::cout << "UDP Server receive message  from : " << rcvEndPoint.address().to_string() << std::endl;
			switch (message.getIdMessage())
			{
				case rtype::ProtocolRtype::IDPacket::POSITIONS:
					// std::cout << "POSITION" << std::endl;
					this->handlePosition(*p, message.getBodyUnserialized());
					break;
				case rtype::ProtocolRtype::IDPacket::MISSILE:
					// std::cout << "MISSILE" << std::endl;
					this->handleMissile(*p, message.getBodyUnserialized());
					break;
				default:
					std::cout << "UNKNOWN" << std::endl;
					break;
			}
		}
		catch(const std::exception& e)
		{
			std::cerr << e.what() << '\n';
			return;
		}		
	}

	Character	*RtypeRoom::findCharacter(Gamer *gamer) const
	{
		for (auto obj: this->_gamers) {
			if (obj.first == gamer)
				return (obj.second);
		}
		return (nullptr);
	}
	void	RtypeRoom::handlePosition(Gamer *gamer, rtype::IMessage::SerializedObject serializedObject)
	{
		rtype::GenericMessage *response;
		Character	*character = this->findCharacter(gamer);
		rtype::MetadataMessage metadata;
		unsigned int x;
		unsigned int y;

		if (character == nullptr || character->getLifePoint() <= 0)
			return;

		try
		{
			metadata.unserialize(serializedObject);
			x = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::X)));
			y = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::Y)));
			// const std::string &name = metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::ID));
			// std::cout << name << " " << x << " " << y << std::endl;
		}
		catch(const std::exception& e)
		{
			std::cerr << e.what() << '\n';
			return;
		}
		character->setX(x);
		character->setY(y);

		response = rtype::ProtocolRtype::generatePacketPosition(gamer->getName(), x, y);
		if (response == nullptr)
			return;
		
		//  << "New Position for " << gamer->getName() << " x: " << x << " y: " << y << std::endl;
		serializedObject = response->serialize();
		this->asyncSend(serializedObject.first, serializedObject.second);
		delete response;
	}
	void	RtypeRoom::handleMissile(Gamer *gamer, rtype::IMessage::SerializedObject serializedObject)
	{
		rtype::GenericMessage *response;
		rtype::MetadataMessage metadata;
		Missile *missile;
		unsigned int x;
		unsigned int y;
		// unsigned int vx;
		// unsigned int vy;
		unsigned int power;
		(void)gamer;

		try
		{
			metadata.unserialize(serializedObject);
			x = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::X)));
			y = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::Y)));
			// vx = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::VX)));
			// vy = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::VY)));
			power = std::stoul(metadata.getData(rtype::ProtocolRtype::MetadataFields::toString(rtype::ProtocolRtype::MetadataFields::POWER)));
			// std::cout << "x: " << x;
			// std::cout << "y: " << y;
			// std::cout << "vx: " << vx;
			// std::cout << "vy: " << vy;
			// std::cout << "power: " << power << std::endl;;
		}
		catch(const std::exception& e)
		{
			std::cerr << e.what() << '\n';
			return;
		}
		if (this->_missiles.size() <= (this->_step + 1) * 4 * (this->_step + 1)) {	
			missile = new Missile(10 * (this->_step + 1), x + 100, y, (this->_step + 1), 0);
			if (missile == nullptr)
				return;
			this->_missiles.push_back(missile);
		} else  {
			return;
		}

		response = rtype::ProtocolRtype::generatePacketMissile(missile->getId(), power, x, y);
		if (response == nullptr)
			return;
		serializedObject = response->serialize();
		this->asyncSend(serializedObject.first, serializedObject.second);
		delete response;
	}

	void	RtypeRoom::updateEnemies()
	{
		rtype::GenericMessage *message;

		//std::cout << "Update enemies" << std::endl;
		for (std::list<Enemies *>::iterator it = this->_enemies.begin(); it != this->_enemies.end(); ) {
			Enemies *enemy = *it;

			if (enemy->getX() <= 3 || enemy->getLifePoint() <= 0) {
				it = this->_enemies.erase(it);
				delete enemy;
			} else {
				
				// if (this->_missiles.size() <= (this->_step + 1) * 10) {
				// 	enemy->shoot();
				// 	Missile *missile = new Missile(10 * (this->_step + 1), enemy->getX(), enemy->getY(), -(this->_step + 1), 0);
				// 	message = rtype::ProtocolRtype::generatePacketMissile(missile->getId(), missile->getPower(), missile->getX(), missile->getY());

				// 	if (message != nullptr) {
				// 		rtype::IMessage::SerializedObject s = message->serialize();
				// 		this->asyncSend(s.first, s.second);
				// 		delete message;
				// 	}
				// 	this->_missiles.push_back(missile);
				// }
				it++;
			}
		}


		if (this->_enemies.size() < (this->_step + 1) * 10) {
			Enemies *enemy = new Enemies();

			if (enemy != nullptr) {
				enemy->setX(1500);
				enemy->setY(std::rand() % 600);
				this->_enemies.push_back(enemy);
			}
		}


		for (Enemies *enemies: this->_enemies) {
			// change enemy position
			this->_lua.getNewCoordinates(enemies);
			// enemies->setX(enemies->getX() - (2));
			message = rtype::ProtocolRtype::generatePacketEnemyPosition(
				std::to_string(enemies->getId()),
				"no-type",
				enemies->getX(),
				enemies->getY()
			);
			if (message != nullptr) {
				rtype::IMessage::SerializedObject s = message->serialize();
				this->asyncSend(s.first, s.second);
				delete message;
			}
		}

		// std::vector<Enemies *> temp(this->_enemies.begin(), this->_enemies.end());
		// std::random_shuffle(temp.begin(), temp.end());

		// // copy the (shuffled) vector back into the list
		// std::copy(temp.begin(), temp.end(), this->_enemies.begin());

		// std::random_shuffle(this->_enemies.begin(), this->_enemies.end());
		//std::cout << "Update enemies	end" << std::endl;

	}

	void	RtypeRoom::updateMissiles()
	{
		//std::cout << "Update updateMissiles " << this->_missiles.size() << std::endl;

		std::list<Missile *>::iterator itend = this->_missiles.end();
		for (std::list<Missile *>::iterator it = this->_missiles.begin(); it != itend; ) {
			Missile *missile = *it;
			missile->updatePosition();

			if (missile->getX() > 1900 || missile->getX() < 0) {
				it = this->_missiles.erase(it);
				delete missile;
				// std::cout << "delete missile" << std::endl;
			} else {
				rtype::GenericMessage *message = rtype::ProtocolRtype::generatePacketMissile(missile->getId(), missile->getPower(), missile->getX(), missile->getY());
				
				if (message != nullptr) {
					rtype::IMessage::SerializedObject s = message->serialize();
					this->asyncSend(s.first, s.second);
					delete message;
				}
				for (auto p: this->_gamers) {
					Character *character = p.second;
					Gamer *gamer = p.first;

					if (
						(character->getX() > missile->getX() && character->getX() < missile->getX() + 30)
						&& (character->getY() > missile->getY() && character->getY() < missile->getY() + 30)
						) {
							character->decreaseLifePoint(missile->getPower());
							std::cout << character->getId() << " lose " << missile->getPower() << "points: " << character->getLifePoint() << std::endl;
							rtype::GenericMessage *message = rtype::ProtocolRtype::generateLifePacket(gamer->getName(), character->getLifePoint());
							rtype::IMessage::SerializedObject serial;
							if (message != nullptr) {
								serial = message->serialize();
								this->asyncSend(serial.first, serial.second);
								delete message;
							}

							if (character->getLifePoint() <= 1 && (message = rtype::ProtocolRtype::generatePacketDeath(gamer->getName()))) {
								serial = message->serialize();
								this->asyncSend(serial.first, serial.second);
								delete message;
							}

							if ((message = rtype::ProtocolRtype::generatePacketDeath(std::to_string(missile->getId()))))
							{
								serial = message->serialize();
								this->asyncSend(serial.first, serial.second);
								delete message;
							}

							it = this->_missiles.erase(it);
							delete missile;
							break;
						}
				}

				if (it != this->_missiles.end()) {
					for (Enemies *enemy: this->_enemies) {	

						if (
							(missile->getX() > enemy->getX() && missile->getX() < enemy->getX() + 30)
							&& (missile->getY() > enemy->getY() && missile->getY() < enemy->getY() + 30)
						) {
							enemy->decreaseLifePoint(missile->getPower());
							// std::cout << "Enemy " << enemy->getId() << " lose " << missile->getPower() << "points: " << enemy->getLifePoint() << std::endl;
							rtype::GenericMessage *message = rtype::ProtocolRtype::generateLifePacket(std::to_string(enemy->getId()), enemy->getLifePoint());
							rtype::IMessage::SerializedObject serial;
							if (message != nullptr) {
								serial = message->serialize();
								this->asyncSend(serial.first, serial.second);
								delete message;
							}

							if (enemy->getLifePoint() <= 1 && (message = rtype::ProtocolRtype::generatePacketDeath(std::to_string(enemy->getId())))) {
								serial = message->serialize();
								this->asyncSend(serial.first, serial.second);
								delete message;
							}

							if ((message = rtype::ProtocolRtype::generatePacketDeath(std::to_string(missile->getId()))))
							{
								serial = message->serialize();
								this->asyncSend(serial.first, serial.second);
								delete message;
							}

							it = this->_missiles.erase(it);
							delete missile;
							break;
						}
					}
				}

				it++;
			}
		}
		//std::cout << "Update updateMissiles end" << std::endl;


	}
}

