/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** IGame
*/

#ifndef IGAME_HPP_
#define IGAME_HPP_
#include <string>
#include <list>
#include "Gamer/Gamer.hpp"
namespace server
{
	class IRoom {
		public:
			virtual ~IRoom() {};

			/**
			 * @brief Get the Game Name object
			 * 
			 * @return std::string Game name
			 */
			virtual std::string	getGameName() const = 0;
			
			/**
			 * @brief Get the Room Name object
			 * 
			 * @return std::string Room name
			 */
			virtual std::string	getRoomName() const = 0;
			
			/**
			 * @brief Set the Room Name object
			 * 
			 * @param name Name to set
			 */
			virtual void	setRoomName(const std::string &name) = 0;

			/**
			 * @brief Get the Ip Address object
			 * 
			 * @return std::string Address ip
			 */
			virtual std::string	getIpAddress() const = 0;

			/**
			 * @brief Get the Port Address object
			 * 
			 * @return unsigned int Port
			 */
			virtual unsigned int	getPortAddress() const = 0;

			/**
			 * @brief Add a player in the room
			 * 
			 * @param gamer Gamer to add
			 */
			virtual void	addPlayer(Gamer *gamer)= 0;

			/**
			 * @brief Get the Gamers object
			 * 
			 * @return std::list<Gamer &> gamers
			 */
			virtual std::list<Gamer *> getPlayers() const = 0;

			/**
			 * @brief Start the game
			 */
			virtual void	start() = 0;

			/**
			 * @brief Function called to say that player is disconnected
			 * 
			 * @param gamer Gamer disconnected
			 */
			virtual void	playerDisconnection(const Gamer &gamer) = 0;
		protected:
		private:
	};
	
} // namespace server

#endif /* !IGAME_HPP_ */
