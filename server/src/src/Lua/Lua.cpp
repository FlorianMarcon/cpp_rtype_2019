/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Lua
*/

#include <iostream>
#include "Lua.hpp"
// #include <unistd.h>
// #include <stdio.h>
// #include <limits.h>
namespace server
{


	/**
	 * @brief Construct a new Lua:: Lua object
	 * 
	 * C function to lua library
	 * 
	 * @param file 
	 */

	static int	l_add(lua_State *L)
	{
		double a = lua_tonumber(L, 1);  /* get argument */
		double b = lua_tonumber(L, 2);  /* get argument */
      	lua_pushnumber(L, (a + b) );  /* push result */
      	return 1;
	}

	/**
	 * ==================================================
	 */

	Lua::Lua(const std::string &file)
	{
		 /* initialize Lua */
        luaState = luaL_newstate();

        /* load Lua base libraries */
		luaopen_base(this->luaState);             /* opens the basic library */
		luaopen_table(this->luaState);            /* opens the table library */
		luaopen_io(this->luaState);               /* opens the I/O library */
		luaopen_string(this->luaState);           /* opens the string lib. */
		luaopen_math(this->luaState);             /* opens the math lib. */

        // /* load the script */
        luaL_dofile(luaState, file.c_str());

		lua_pushcfunction(this->luaState, l_add);
		lua_setglobal(this->luaState, "myadd");
	}

	Lua::~Lua()
	{
		lua_close(this->luaState);
	}
	

	void	Lua::getNewCoordinates(Coordinates *coords)
	{
		
		lua_getglobal(this->luaState, "getNewCoordinates");
		lua_pushnumber(this->luaState, coords->getX());
		lua_pushnumber(this->luaState, coords->getY());
		if (lua_pcall(this->luaState, 2, 2, 0) != 0) {
			coords->setX(coords->getX() - 2);
			coords->setY(coords->getY());
		} else {
			if (!lua_isnumber(this->luaState, -1) || !lua_isnumber(this->luaState, -2)) {
				std::cerr << "Lua: getNewCoordinates must return two integer" << std::endl;
			}
			const unsigned int x = lua_tointeger(this->luaState, -2);
			const unsigned int y = lua_tointeger(this->luaState, -1);

			coords->setX(x);
			coords->setY(y);
			lua_pop(this->luaState, 1);
		}
	
	}
} // namespace server

