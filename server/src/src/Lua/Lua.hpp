/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Lua
*/

#ifndef LUA_HPP_
#define LUA_HPP_

#include <string>
#include "Game/Rtype/Coordinates.hpp"
#include "lua5.3/lua.h"
#include "lua5.3/lualib.h"
#include "lua5.3/lauxlib.h"

namespace server
{
	/**
	 * @brief 
	 * 
	 */
	class Lua {
		public:
			Lua(const std::string &file);
			~Lua();


			void getNewCoordinates(Coordinates *coords);
		protected:
		private:

		lua_State *luaState;
	};
	
} // namespace server


#endif /* !LUA_HPP_ */
