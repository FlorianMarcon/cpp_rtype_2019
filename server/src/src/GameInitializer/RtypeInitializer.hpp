/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** RtypeInitializer
*/

#ifndef RTYPEINITIALIZER_HPP_
#define RTYPEINITIALIZER_HPP_

#include "IGameInitializer.hpp"
namespace server
{
	/**
	 * @brief Rtype game initializer
	 * 
	 */
	class RtypeInitializer: public IGameInitializer {
		public:
			RtypeInitializer();
			~RtypeInitializer();

			/**
			 * @brief Get the Name object
			 * 
			 * @return std::string Name
			 */
			std::string	getName() const;

			/**
			 * @brief Get the Information object
			 * 
			 * @param key Information asked
			 * @return std::string Information
			 */
			std::string	getInformation(const std::string &key) const;

			/**
			 * @brief Create a Game object
			 * 
			 * @return IRoom* Game
			 */
			IRoom	*createGame() const;
		protected:
		private:
	};
	
} // namespace server


#endif /* !RTYPEINITIALIZER_HPP_ */
