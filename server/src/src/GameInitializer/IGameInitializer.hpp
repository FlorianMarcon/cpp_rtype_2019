/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** IGameInitializer
*/

#ifndef IGAMEINITIALIZER_HPP_
#define IGAMEINITIALIZER_HPP_

#include <string>
#include "Game/IRoom.hpp"

namespace server
{
	class IGameInitializer {
		public:

			/**
			 * @brief Get the Name object
			 * 
			 * @return std::string Game name
			 */
			virtual std::string	getName() const = 0;

			/**
			 * @brief Create a Game object
			 * 
			 * @return IRoom* Game object
			 */
			virtual IRoom	*createGame() const = 0;

			/**
			 * @brief Get the Information object
			 * 
			 * @param key Information asked
			 * @return std::string Value of the informations
			 */
			virtual std::string	getInformation(const std::string &key) const = 0;
		protected:
		private:
	};
	
} // namespace server


#endif /* !IGAMEINITIALIZER_HPP_ */
