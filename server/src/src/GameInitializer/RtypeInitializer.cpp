/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** RtypeInitializer
*/

#include "RtypeInitializer.hpp"
#include "Game/Rtype/RtypeRoom.hpp"

namespace server
{
	RtypeInitializer::RtypeInitializer()
	{
	}

	RtypeInitializer::~RtypeInitializer()
	{
	}

	std::string	RtypeInitializer::getName() const {
		return RtypeRoom::getName();
	}

	std::string	RtypeInitializer::getInformation(const std::string &key) const {
		if (key == "name")
			return this->getName();
		return ("");
	}

	IRoom	*RtypeInitializer::createGame() const
	{
		return (new RtypeRoom());
	}
	
} // namespace server

