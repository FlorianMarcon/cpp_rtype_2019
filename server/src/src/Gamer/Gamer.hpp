/*
** Gamer.hpp for cpp_rtype_2019 in /home/marc/Bureau/EPITECH_CURSUS_TROISIEME_ANNEE/R-TYPE/cpp_rtype_2019/server/src/src/Gamer
**
** Made by marc.frezal@epitech.eu
** Login   <>
**
** Started on  Tue Nov 5 16:34:19 2019 marc.frezal@epitech.eu
** Last update Wed Nov 5 16:35:06 2019 marc.frezal@epitech.eu
*/

#ifndef GAMER_HPP_
# define GAMER_HPP_

#include <string>
#include "Network/HandleConnection.hpp"
#include "Network/UDP.hpp"

namespace server {
	class Gamer {
		public:
			using ReceiveCallback = boost::function<void(Gamer &gamer, char *buffer, ssize_t size)>;
			using ConnectionClosedCallback = boost::function<void(Gamer &gamer)>;

			Gamer(Handle::pointer handle);
			~Gamer();


			/**
			 * @brief Get the Handle object
			 * 
			 * @return Handle::pointer 
			 */
			Handle::pointer	getHandle() const;


			void	setReceiveCallback(const ReceiveCallback &callback);
			void	setCloseCallback(const ConnectionClosedCallback &callback);
		
			void	setName(const std::string &name);
			std::string	getName() const;

			/**
			 * @brief Get the udp endpoint
			 * 
			 * @return rtype::UDPEndPoint Udp endpoint
			 */
			rtype::UDPEndPoint	getUDPEndPoint() const;

			/**
			 * @brief Set UDP endpoint
			 * 
			 * @param address Address
			 * @param port Port
			 */
			void	setUDPEndPoint(const std::string &address, unsigned int port);
		protected:
		private:
			void	handleReceive(Handle &handle, char *buffer, ssize_t size);
			void	handleClosed(Handle	&handle);
			
			Handle::pointer	_handle;
			ReceiveCallback	_callbackReceive;
			ConnectionClosedCallback	_callbackClosed;

			std::string	_name;

			rtype::UDPEndPoint	_udpEndPoint;
	};
}
#endif /* !GAMER_HPP_ */
