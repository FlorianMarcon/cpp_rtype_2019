/*
** Gamer.cpp for cpp_rtype_2019 in /home/marc/Bureau/EPITECH_CURSUS_TROISIEME_ANNEE/R-TYPE/cpp_rtype_2019/server/src/src/Gamer
**
** Made by marc.frezal@epitech.eu
** Login   <>
**
** Started on  Tue Nov 5 16:34:56 2019 marc.frezal@epitech.eu
** Last update Wed Nov 5 16:36:23 2019 marc.frezal@epitech.eu
*/

#include "Gamer.hpp"
#include <boost/asio.hpp>
#include <exception>

namespace server {
	Gamer::Gamer(Handle::pointer handle) :
	_handle(handle)
	{
		if (this->_handle == nullptr)
			throw std::logic_error("Handle::pointer can't be null");

		this->_handle->setCloseCallback(boost::bind(&Gamer::handleClosed, this, _1));
		this->_handle->setReceiveCallback(boost::bind(&Gamer::handleReceive, this, _1, _2, _3));
	}

	Gamer::~Gamer()
	{
	}

	Handle::pointer	Gamer::getHandle() const {
		return (this->_handle);
	}


	void	Gamer::setReceiveCallback(const Gamer::ReceiveCallback &receive)
	{
		this->_callbackReceive = receive;
	}

	void	Gamer::setCloseCallback(const Gamer::ConnectionClosedCallback &closed)
	{
		this->_callbackClosed = closed;
	}

	void	Gamer::handleReceive(Handle &handle, char *buffer, ssize_t size)
	{
		(void)handle;
		try
		{
			this->_callbackReceive(*this, buffer, size);
		}
		catch(const std::exception& e)
		{
		}
		
	}

	void	Gamer::handleClosed(Handle &handle)
	{
		(void)handle;

		try
		{
			this->_callbackClosed(*this);
		}
		catch(const std::exception& e)
		{
		}
		
	}


	void	Gamer::setName(const std::string &name)
	{
		this->_name = name;
	}

	std::string	Gamer::getName() const
	{
		return (this->_name);
	}

	void	Gamer::setUDPEndPoint(const std::string &address, unsigned int port)
	{
		this->_udpEndPoint = rtype::UDPEndPoint(
			boost::asio::ip::address::from_string(address)
			,port
			);
	}

	rtype::UDPEndPoint	Gamer::getUDPEndPoint() const
	{
		return (this->_udpEndPoint);
	}
}
