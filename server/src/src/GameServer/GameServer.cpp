/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Server
*/

#include	"GameServer.hpp"
#include "GameInitializer/RtypeInitializer.hpp"
#include "Protocol/Protocol.hpp"

namespace server
{
	GameServer::GameServer(boost::asio::io_service& io_service, unsigned int port) : TCPServer(io_service, port)
	{
	}

	GameServer::~GameServer()
	{
	}
	
	IRoom	*GameServer::getRoom(const std::string &game, const std::string &name)
	{
		std::list<IRoom *>::iterator it = std::find_if(this->_rooms.begin(), this->_rooms.end(), [game, name](IRoom *room) {
			return (room->getRoomName() == name && room->getGameName() == game);
		});

		if (it == this->_rooms.end())
			return nullptr;
		return (*it);
	}
	void GameServer::handleAccept(Handle::pointer connection, const boost::system::error_code& err)
	{
		Gamer	*gamer;

		if (err.value() != 0)
			return;
		
		gamer = new Gamer(connection);
		this->_gamers.push_back(gamer);
		gamer->setCloseCallback(boost::bind(&GameServer::handleClosing, this, _1));
		gamer->setReceiveCallback(boost::bind(&GameServer::handleReceive, this, _1, _2, _3));
		gamer->getHandle()->startServer();
		this->startServeur();
	}

	void	GameServer::handleClosing(Gamer &gamer)
	{
		this->_gamers.remove(&gamer);

		std::cout << "Gamer " << gamer.getName() << " is disconnected" << std::endl;
		for (IRoom *room: this->_rooms) {
			room->playerDisconnection(gamer);
		}
		delete &gamer;
	}

	void	GameServer::handleReceive(Gamer &gamer, char *buffer, ssize_t size)
	{
		rtype::GenericMessage message;

		try
		{
			message.unserialize(buffer, size);
			std::cout << "Server receive message ";
			switch (message.getIdMessage())
			{
				case rtype::Protocol::IDMessage::REQUEST :
					std::cout << "REQUEST" << std::endl;
					this->handleRequest(gamer, message);
					break;
				default:
					std::cout << "UNKNOWN" << std::endl;
					break;
			}
		}
		catch(const std::exception& e)
		{
			std::cerr << e.what() << '\n';
			return;
		}
		

	}

	void	GameServer::handleRequest(Gamer &gamer, rtype::GenericMessage &message)
	{
		rtype::GenericMessage request;
		rtype::IMessage::SerializedObject serializedObject = message.getBodyUnserialized();
		rtype::GenericMessage *response;
		
		try
		{
			request.unserialize(serializedObject);
			std::cout << "Server receive request ";
			switch (request.getIdMessage())
			{
			case rtype::Protocol::SETUP_NAME:
				std::cout << "SETUP_NAME"  << std::endl;
				this->handleSetupName(gamer, request);
				break;
			case rtype::Protocol::LIST_GAMES:
				std::cout << "LIST_GAMES"  << std::endl;
				this->handleListGames(gamer, request);
				break;
			case rtype::Protocol::LIST_PLAYERS:
				std::cout << "LIST_PLAYERS"  << std::endl;
				this->handleListPlayers(gamer, request);
				break;
			case rtype::Protocol::LIST_PLAYERS_IN_ROOM:
				std::cout << "LIST_PLAYERS_IN_ROOM"  << std::endl;
				this->handleListPlayersInRoom(gamer, request);
				break;
			case rtype::Protocol::LIST_ROOMS:
				std::cout << "LIST_ROOMS"  << std::endl;
				this->handleListRooms(gamer, request);
				break;
			case rtype::Protocol::CREATE_ROOM:
				std::cout << "CREATE_ROOM"  << std::endl;
				this->handleCreateRoom(gamer, request);
				break;
			case rtype::Protocol::CONNECT_ROOM:
				std::cout << "CONNECT_ROOM"  << std::endl;
				this->handleGoInRoom(gamer, request);
				break;
			case rtype::Protocol::START_GAME:
				std::cout << "START_GAME"  << std::endl;
				this->handleStartGame(gamer, request);
				break;

			default:
				std::cout << "UNKNOWN REQUEST"  << std::endl;;
				response = rtype::Protocol::generateResponse(rtype::BAD_REQUEST, "Unknown request");
				serializedObject = response->serialize();
				gamer.getHandle()->write(serializedObject);
				delete response;
				break;
			}
		}
		catch(const std::range_error& e)
		{
			std::cerr << e.what() << '\n';
			response = rtype::Protocol::generateResponse(rtype::UNPROCESSABLE_ENTITY, e.what());
			serializedObject = response->serialize();
			gamer.getHandle()->write(serializedObject);
			delete response;
			return;
		}
		catch(const std::exception& e)
		{
			std::cerr << e.what() << '\n';
			response = rtype::Protocol::generateResponse(rtype::BAD_REQUEST, e.what());
			serializedObject = response->serialize();
			gamer.getHandle()->write(serializedObject);
			delete response;
			return;
		}
		
	

	}

	void	GameServer::handleSetupName(Gamer &gamer, rtype::GenericMessage &request)
	{
		rtype::GenericMessage *response;
		rtype::IMessage::SerializedObject serializedObject = request.getBodyUnserialized();
		rtype::MetadataMessage metadata;
		std::string name;
		try
		{
			metadata.unserialize(serializedObject);
			name = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::SETUP_NAME));
		}
		catch(const std::exception& e)
		{
			std::cerr << e.what() << '\n';
			response = rtype::Protocol::generateResponse(rtype::BAD_REQUEST, e.what());
			serializedObject = response->serialize();
			gamer.getHandle()->write(serializedObject);
			delete response;
			return;
		}
		response = rtype::Protocol::generateResponse(rtype::OK);
		serializedObject = response->serialize();
		gamer.setName(name);
		std::cout << "Name set: " << name << std::endl;
		gamer.getHandle()->write(serializedObject);
		delete response;
	}
	void	GameServer::handleListGames(Gamer &gamer, rtype::GenericMessage &request)
	{
		rtype::GenericMessage *response;
		rtype::IMessage::SerializedObject serializedObject;
		std::list<std::string> games;
		(void)request;

		for (IGameInitializer *game: this->_gameInitializers)
		{
			games.push_back(game->getName());
		}
		response = rtype::Protocol::generateListGamesResponses(games);
		serializedObject = response->serialize();
		gamer.getHandle()->write(serializedObject);
		delete response;
	}

	void	GameServer::handleListPlayers(Gamer &gamer, rtype::GenericMessage &request)
	{
		rtype::GenericMessage *response;
		rtype::IMessage::SerializedObject serializedObject;
		std::list<std::string> players;
		(void)request;

		for (Gamer *other: this->_gamers)
		{
			if (other != &gamer)
				players.push_back(other->getName());
		}
		response = rtype::Protocol::generateListPlayerResponses(players);
		serializedObject = response->serialize();
		gamer.getHandle()->write(serializedObject);
		delete response;
	}
	void	GameServer::handleListRooms(Gamer &gamer, rtype::GenericMessage &request)
	{
		rtype::GenericMessage *response;
		rtype::IMessage::SerializedObject serializedObject = request.getBodyUnserialized();
		std::list<std::string> rooms;
		rtype::MetadataMessage metadata;
		std::string game;
		(void)request;

		metadata.unserialize(serializedObject);
		game = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::GAME_NAME));

		for (IRoom *obj: this->_rooms)
		{
			if (obj->getGameName() == game)
				rooms.push_back(obj->getRoomName());
		}
		response = rtype::Protocol::generateListRoomResponses(rooms);
		serializedObject = response->serialize();
		gamer.getHandle()->write(serializedObject);
		delete response;
	}
	void	GameServer::handleCreateRoom(Gamer &gamer, rtype::GenericMessage &request)
	{
		rtype::GenericMessage *response;
		rtype::IMessage::SerializedObject serializedObject = request.getBodyUnserialized();
		rtype::MetadataMessage metadata;
		std::string game;
		std::string name;

		// Get data
		metadata.unserialize(serializedObject);
		name = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::ROOM_NAME));
		game = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::GAME_NAME));


		//Search GameInitializer
		std::list<IGameInitializer *>::iterator it = std::find_if(this->_gameInitializers.begin(), this->_gameInitializers.end(), [game](IGameInitializer *obj) {
			return (obj->getName() == game);
		});

		if (it == this->_gameInitializers.end()) {
			response = rtype::Protocol::generateResponse(rtype::NOT_FOUND);
			serializedObject = response->serialize();
		} else {
			IRoom *room = (*it)->createGame();
			room->setRoomName(name);
			this->_rooms.push_back(room);
			response = rtype::Protocol::generateResponse(rtype::OK);
			serializedObject = response->serialize();
		}
		gamer.getHandle()->write(serializedObject);
		delete response;
	}

	void	GameServer::handleListPlayersInRoom(Gamer &gamer, rtype::GenericMessage &request)
	{
		rtype::GenericMessage *response;
		rtype::IMessage::SerializedObject serializedObject = request.getBodyUnserialized();
		std::list<std::string> gamers;
		rtype::MetadataMessage metadata;
		std::string game;
		std::string name;
		IRoom *room;
		(void)request;

		metadata.unserialize(serializedObject);
		game = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::GAME_NAME));
		name = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::ROOM_NAME));

		room = this->getRoom(game, name);

		if (room == nullptr) {
			response = rtype::Protocol::generateResponse(rtype::NOT_FOUND, "This games doesn't exist");
		} else {
			for (Gamer *obj: room->getPlayers())
			{
				gamers.push_back(obj->getName());
			}
			response = rtype::Protocol::generateListPlayerResponses(gamers);
		}
		serializedObject = response->serialize();
		gamer.getHandle()->write(serializedObject);
		delete response;
	}

	void	GameServer::handleGoInRoom(Gamer &gamer, rtype::GenericMessage &request)
	{
		rtype::GenericMessage *response;
		rtype::IMessage::SerializedObject serializedObject = request.getBodyUnserialized();
		std::list<std::string> rooms;
		rtype::MetadataMessage metadata;
		std::string game;
		std::string name;
		std::string address;
		std::string port;
		IRoom *room;

		metadata.unserialize(serializedObject);
		game = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::GAME_NAME));
		name = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::ROOM_NAME));
		address = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::ADDRESS));
		port = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::PORT));


		room = this->getRoom(game, name);
		if (room == nullptr) {
			response = rtype::Protocol::generateResponse(rtype::NOT_FOUND, "This rooms doesnt exist");
		} else {
			gamer.setUDPEndPoint(address, std::atoi(port.c_str()));
			room->addPlayer(&gamer);
			response = rtype::Protocol::generateGoInRoomResponses(room->getIpAddress(), room->getPortAddress());
		}

		serializedObject = response->serialize();
		gamer.getHandle()->write(serializedObject);
		delete response;
	}


	void	GameServer::handleStartGame(Gamer &gamer, rtype::GenericMessage &request)
	{
		rtype::GenericMessage *response;
		rtype::IMessage::SerializedObject serializedObject = request.getBodyUnserialized();
		rtype::MetadataMessage metadata;
		std::string game;
		std::string name;
		IRoom *room;

		metadata.unserialize(serializedObject);
		game = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::GAME_NAME));
		name = metadata.getData(rtype::Protocol::MetadataFields::toString(rtype::Protocol::MetadataFields::Fields::ROOM_NAME));

		room = this->getRoom(game, name);
		if (room == nullptr) {
			response = rtype::Protocol::generateResponse(rtype::NOT_FOUND, "This rooms doesnt exist");
		} else {
			std::list<Gamer *> gamers = room->getPlayers();

			for (Gamer *g: gamers)
			{
				std::cerr << g->getName() << std::endl;
			}
			std::list<Gamer *>::iterator it = std::find(gamers.begin(), gamers.end(), &gamer);
			if (it == gamers.end()) {
				response = rtype::Protocol::generateResponse(rtype::UNAUTHORIZED);
			} else {
				room->start();
				response = rtype::Protocol::generateResponse(rtype::OK);
			}
		}

		serializedObject = response->serialize();
		gamer.getHandle()->write(serializedObject);
		delete response;
	}	
} // namespace rtype

