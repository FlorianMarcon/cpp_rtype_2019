/*
** EPITECH PROJECT, 2019
** cpp_rtype_2019
** File description:
** Server
*/

#ifndef SERVER_HPP_
#define SERVER_HPP_

#include "GameInitializer/RtypeInitializer.hpp"
#include "Network/TCPServer.hpp"
#include "Gamer/Gamer.hpp"
#include "GameInitializer/IGameInitializer.hpp"
#include "Messages/IMessage.hpp"
#include "Messages/GenericMessage.hpp"
#include "Game/IRoom.hpp"
#include <list>

namespace server
{
	/**
	 * @brief Game Server implementing TCPServer
	 * 
	 */
	class GameServer : public rtype::TCPServer {
		public:
		
			GameServer(boost::asio::io_service& io_service, unsigned int port = 0);
			~GameServer();

			/**
			 * @brief Function called when new connection appears
			 * 
			 * @param connection Connection
			 * @param err Error code
			 */
	  		void handleAccept(Handle::pointer connection, const boost::system::error_code& err) override;
		
			/**
			 * @brief Function called when a socket receive data
			 * 
			 * @param gamer Gamer sending a data
			 * @param buffer Buffer containing data
			 * @param size Buffer size
			 */
			void handleReceive(Gamer &gamer, char *buffer, ssize_t size);
			/**
			 * @brief Function called when a session is finished
			 * 
			 * @param gamer Gamer
			 */
			void handleClosing(Gamer &gamer);

		protected:

			void handleRequest(Gamer &gamer, rtype::GenericMessage &message);
		
			void handleSetupName(Gamer &gamer, rtype::GenericMessage &request);
			void handleListGames(Gamer &gamer, rtype::GenericMessage &request);
			void handleListPlayers(Gamer &gamer, rtype::GenericMessage &request);
			void handleListPlayersInRoom(Gamer &gamer, rtype::GenericMessage &request);
			void handleListRooms(Gamer &gamer, rtype::GenericMessage &request);
			void handleCreateRoom(Gamer &gamer, rtype::GenericMessage &request);
			void handleGoInRoom(Gamer &gamer, rtype::GenericMessage &request);
			void handleStartGame(Gamer &gamer, rtype::GenericMessage &request);

			IRoom	*getRoom(const std::string &game, const std::string &name);
		private:

			std::list<server::Gamer *>	_gamers;
			std::list<IGameInitializer *> _gameInitializers = {
				new RtypeInitializer()
			};
			std::list<IRoom *> _rooms;
	};
	
} // namespace rtype


#endif /* !SERVER_HPP_ */
